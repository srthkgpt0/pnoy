/** @type {import('tailwindcss').Config} */
const withMT = require("@material-tailwind/react/utils/withMT");
export default withMT({
  mode: "jit",
  darkMode: "class",
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontSize: {
      text_xs: ["10px", "14px"],
      xs: ["12px", "16px"],
      sm: ["14px", "20px"],
      base: ["16px", "19.5px"],
      lg: ["18px", "21.94px"],
      xl: ["20px", "24.38px"],
      "2xl": ["24px", "29.26px"],
      "3xl": ["28px", "50px"],
      "3.5xl": ["35px", "50px"],
      "4xl": ["48px", "58px"],
      "8xl": ["96px", "106px"],
    },
    extend: {
      fontFamily: {
        inter: ["Inter", "sans-serif"],
        goBold: ["Gobold", "sans-serif"],
        aurinda: ["Aurinda", "Helvetica"],
        bebas: ["Bebas", "sans-serif"],
      },
      colors: {
        primary: "#346BAE",
        "nav-link": "#E8F3FF",
        "card-bg": "#f3f9ff",
        secondary: "#262973",
        yellow: "#F5A93F",
        "carousal-white": "#d9d9d9",
        "white-400": "rgba(255, 255, 255, 0.80)",
        "light-text": "#a2a2a2",
        "whatsapp-green": '#60D669'
      },
      boxShadow: {
        "3xl": "0 10px 40px rgba(0, 0, 0, 0.1)",
      },
      backgroundImage: {
        hero: "url('assets/images/collection-background.svg')",
        card: "url('assets/images/thumbnail-background.svg')",
      },
      screens: {
        wide: "1440px",
        '3xl': "1536px"
      },
      animation: {
        marquee: 'marquee 15s linear infinite',
        marquee2: 'marquee2 15s linear infinite'
      },
      keyframes: {
        marquee: {
          '0%': { transform: 'translateX(0%)' },
          '100%': { transform: 'translateX(-100%)' },
        },
        marquee2: {
          '0%': { transform: 'translateX(100%)' },
          '100%': { transform: 'translateX(0%)' },
        },
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
});
