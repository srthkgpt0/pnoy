import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const PrivacyPolicy = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Privacy Policy</SecondaryHeader>
      <section className='w-full padding-x padding-y flex flex-col justify-between'>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Effective Date
          </h2>
          <p>
            Thank you for visiting the Mobile App / Website of PNOY Water
            Ionisers. At PNOY, we value your privacy and are committed to
            protecting the information you share with us. This PrivacyPolicy
            outlines how we collect, use, disclose, and protect your personal
            information when you use our Mobile App / Website and services. By
            accessing or using our Mobile App / Website , you consent to the
            practices described in this Privacy Policy..
          </p>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Information We Collect
          </h2>
          <ul className='list-disc px-5'>
            <li>
              Personal Information: When you interact with our Mobile App /
              Website or purchase our products, we may collect certain personal
              information such as your name, address, email address, phone
              number, and payment details. We only collect this information when
              you voluntarily provide it to us.
            </li>
            <li>
              Non-Personal Information: We may also collect non-personal
              information automatically, such as your IP address, browser type,
              device information, and usage data, which helps us enhance your
              experience on our website/Mobile App / Website and improve our
              services.
            </li>
          </ul>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            How We Use Your Information
          </h2>
          <ul className='list-disc px-5'>
            <li>
              Provide Services: We use your personal information to process
              orders, deliver products, and respond to your inquiries or
              customer service requests.
            </li>
            <li>
              Communication: We may use your contact information to send you
              updates, promotional materials, and newsletters about our products
              and services. You can opt-out of these communications at any time.
            </li>
            <li>
              Improve User Experience: Non-personal information helps us analyze
              website usage trends and improve our website's performance,
              functionality, and content.
            </li>
            <li>
              Legal Compliance: We may use and disclose your information to
              comply with applicable laws, regulations, legal processes, or
              government requests.
            </li>
          </ul>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Sharing Your Information
          </h2>
          <ul className='list-disc px-5'>
            <li>
              Third-Party Service Providers: We may share your personal
              information with trusted third- party service providers who assist
              us in delivering our products and services, such as shipping
              companies and payment processors. These providers are
              contractually obligated to protect your information and can only
              use it for the specified purpose.
            </li>
            <li>
              Business Transfers: In the event of a merger, acquisition, or
              other corporate transaction involving PNOY, your personal
              information may be transferred as part of the transaction. We will
              notify you of any such event and your choices regarding your
              information.
            </li>
            <li>
              Legal Requirements: We may disclose your information when required
              by law or in response to valid legal requests, such as subpoenas
              or court orders.
            </li>
          </ul>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Cookies and Tracking Technologies
          </h2>
          <p>
            We may use cookies and similar tracking technologies to enhance your
            browsing experience, store your preferences, and collect information
            about how you use our website. You can adjust your browser settings
            to disable cookies, but some features of the website may not
            function properly.
          </p>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Data Security
          </h2>
          <p>
            We implement reasonable security measures to protect your personal
            information from unauthorized access, disclosure, alteration, or
            destruction. However, no method of data transmission over the
            internet or electronic storage is entirely secure, so we cannot
            guarantee absolute security.
          </p>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Changes to the Privacy Policy
          </h2>
          <p>
            We may update this Privacy Policy from time to time to reflect
            changes in our practices or for other operational, legal, or
            regulatory reasons. The updated policy will be posted on our
            website/Mobile App / Website with a revised effective date.
          </p>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Contact Us
          </h2>
          <p>
            If you have any questions, concerns, or requests regarding this
            Privacy Policy or your personal information, please contact us at
          </p>
        </div>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Email: sales@pnoy.in
          </h2>
          <p>
            Thank you for trusting PNOY Water Ionisers with your personal
            information.
          </p>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default PrivacyPolicy;

// const Card = ({ heading, content, type }) => (
//   <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
//     <h2 className='text-xl font-bold text-yellow uppercase mb-10'>{heading}</h2>
//     {type === 'list' ? (
//       <ul className='list-disc px-5'>
//         {content.map((cont, index) => {
//           return <li key={index}>{cont}</li>;
//         })}
//       </ul>
//     ) : (
//       <>
//         {content.map((cont, index) => (
//           <p key={index}>{cont}</p>
//         ))}
//       </>
//     )}
//   </div>
// );
