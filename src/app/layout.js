import Header from "@/components/Header";
import "./globals.css";
import Footer from "@/components/Footer";
import Head from "next/head";

const metadata = {
  title: "Pnoy Electronic",
  description: "India's No. 1 Alkaline Water Solutions",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <Head><title>{metadata.title}</title></Head>
      <body>
        <Header />
        <main className="max-container">{children}</main>
        <Footer />
      </body>
    </html>
  );
}
