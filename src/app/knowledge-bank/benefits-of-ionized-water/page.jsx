'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { motion } from 'framer-motion';
import KnowledgeBankWrapper from '../KnowledgeBankWrapper';
import TabComponent from './TabComponent';
import { benefitsOfIonizedWater } from '@/constants';

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delayChildren: 0.3,
      staggerChildren: 0.2
    }
  }
};

const BenefitsOfIonizedWater = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Knowledge bank</SecondaryHeader>
      <section className='padding-x padding-b w-full'>
        <KnowledgeBankWrapper imgSrc='/PNOY/benefits-of-alkaline-water.png'>
          <h2 className='text-3xl self-center font-bold text-yellow uppercase text-center lg:text-left'>
            Benefits of ionized water
          </h2>
          <motion.div
            className='flex flex-wrap'
            variants={container}
            initial='hidden'
            animate='visible'>
            {benefitsOfIonizedWater.map((element, index) => (
              <TabComponent content={element} key={index} />
            ))}
          </motion.div>
        </KnowledgeBankWrapper>
      </section>
    </LayoutWrapper>
  );
};

export default BenefitsOfIonizedWater;
