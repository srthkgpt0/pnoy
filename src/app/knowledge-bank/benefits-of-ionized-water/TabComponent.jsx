'use client';
import Image from 'next/image';
import { motion } from 'framer-motion';

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1
  }
};

const TabComponent = ({ content }) => {
  const { imgSrc, title, text } = content;
  return (
    <motion.div
      className='flex w-[85%] lg:max-w-[45%] h-[180px] lg:h-[150px] bg-white rounded-xl px-4 py-4 items-center justify-center mx-6 my-6'
      variants={item}>
      <div className='rounded-xl mr-4 w-[10rem] h-[6rem] antialiased overflow-hidden'>
        <div className='relative h-full w-full'>
          <Image src={imgSrc} alt={title} fill className='object-contain' />
        </div>
      </div>
      <div className='flex flex-col py-3 gap-2 self-start'>
        <h3 className='uppercase text-primary text-xl font-semibold font-goBold'>
          {title}
        </h3>
        <div className='text-xs text-gray-600'>{text}</div>
      </div>
    </motion.div>
  );
};

export default TabComponent;
