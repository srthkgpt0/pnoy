const KnowledgeBankWrapper = ({ children, imgSrc }) => {
  return (
    <div className='relative padding-x padding-y'>
      <div className='z-10 relative bg-white bg-opacity-60 rounded-3xl flex flex-col py-10 gap-10 font-inter'>
        {children}
      </div>
      <div className='absolute rounded-b-2xl overflow-hidden top-0 left-0 right-0 bottom-0'>
        <img src={imgSrc} alt='gdfg' className='object-cover h-full w-full' />
      </div>
    </div>
  );
};

export default KnowledgeBankWrapper;
