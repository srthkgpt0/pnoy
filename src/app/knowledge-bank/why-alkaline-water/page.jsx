import { FadeInWhenVisible } from '@/components';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const WhyAlkalineWater = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Knowledge bank</SecondaryHeader>
      <section className='padding-x padding-y flex flex-col'>
        <h2 className='text-3xl self-center font-bold text-yellow uppercase  mb-10'>
          Why <span className='italic'>Ionised</span> water
        </h2>
        <div className='relative'>
          <div className='bg-nav-link p-10 rounded-2xl'>
            <FadeInWhenVisible>
              <h2 className='text-lg font-bold text-yellow uppercase mb-5'>
                Water and our bodies
              </h2>
              <p className='text-sm'>
                The quality of water we drink determines the quality of our
                health. To keep us healthy, our bodies maintain just the right
                internal conditions, such as the concentration of electrolytes,
                body temperature, and pH. Our body contains a lot of water —
                around 60% of an adult's body weight is body fluid. The water we
                drink is absorbed by the intestine and circulated throughout the
                body in the form of body fluid, such as blood. A total of about
                7200L of blood per day circulates around the body, carrying
                oxygen, carbon dioxide, nutrients, and metabolic waste. Blood
                also eliminates bacteria and other foreign materials from our
                body, so it plays a very important role in keeping us alive.
                Body fluid contains electrolytes which have a vital role. If
                electrolyte levels are too low or too high, cell and organ
                functions will decline, which could lead to life-threatening
                conditions. These electrolytes include Sodium (Na), Potassium
                (K), Calcium (Ca), Magnesium (Mg), and Chlorides, and they are
                absorbed by the water we drink, maintaining the function of
                muscles and nerve cells. Therefore, an adequate supply of clean
                and mineral-rich high-quality drinking water is the key to a
                healthy body.
              </p>
            </FadeInWhenVisible>
          </div>
          <div className='relative flex padding-x padding-y'>
            <img
              src='/PNOY/why-alkaline-water-02.png'
              alt='why-alkaline-water-02'
              className='w-1/2'
            />
            <img
              src='/PNOY/why-alkaline-water-01.png'
              alt='why-alkaline-water-01'
              className='w-1/2'
            />
          </div>
        </div>
        <div className='bg-yellow rounded-full lg:padding-x py-10 relative'>
          <FadeInWhenVisible>
            <h2 className='text-lg font-bold text-primary uppercase mb-5 text-center lg:text-left'>
              Alkaline Ionized Water
            </h2>
            <p className='text-white text-sm font-inter px-16 lg:px-0'>
              Akaline Ionized water is smooth, energizing water which has great
              taste and possesses excellent absorption and hydrating abilities
              to aid in buffering the negative effects of hyper acidity in your
              body. Alkaline water also contains alkaline minerals and negative
              Oxidation Reduction Potential (ORP). The more negative the ORP
              value, the more anti-oxidizing it is. Alkaline hydrogen rich water
              boosts our Immunity.
            </p>
            <div className='absolute h-full w-full top-0 left-0 right-0 bottom-0 overflow-hidden'>
              <img
                src='/PNOY/texture_rounded_rec.png'
                alt='texture_rounded_rec'
                className='w-full h-full'
              />
            </div>
          </FadeInWhenVisible>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default WhyAlkalineWater;
