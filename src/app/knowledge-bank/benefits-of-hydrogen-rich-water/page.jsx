'use client';

import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { motion } from 'framer-motion';
import KnowledgeBankWrapper from '../KnowledgeBankWrapper';
const list = [
  'Powerful anti-inflammatory properties.',
  'Reduces pain and swelling.',
  'Offers anti-aging properties.',
  'Has clinically proven benefits in the management of diabetes, asthma, high cholesterol, and all forms of cancers.',
  "Improves brain functions, thus relieving stress, anxiety, depression, insomnia, and Alzheimer's.",
  "Proven clinical benefits in Parkinson's disease.",
  'Offers a quick energy boost.',
  'Ensures quick recovery after exercise.',
  'Treats chronic fatigue syndrome.',
  'Found to be beneficial in treating ADHD.',
  'Improves nerve functions.'
];

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delayChildren: 0.3,
      staggerChildren: 0.2
    }
  }
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1
  }
};

const BenefitsOfHydrogenRichWater = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Knowledge bank</SecondaryHeader>
      <section className='padding-x padding-b w-full'>
        <KnowledgeBankWrapper imgSrc='/PNOY/benefits-hydrogen-rich-antioxidant.png'>
          <h2 className='text-3xl text-center font-bold text-yellow uppercase'>
            Benefits of hydrogen rich antioxidant
          </h2>
          <motion.ul
            className='list-disc padding-x uppercase'
            variants={container}
            initial='hidden'
            animate='visible'>
            {list.map((l, index) => (
              <motion.li key={index} variants={item}>
                {l}
              </motion.li>
            ))}
          </motion.ul>
        </KnowledgeBankWrapper>
      </section>
    </LayoutWrapper>
  );
};

export default BenefitsOfHydrogenRichWater;
