'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { motion } from 'framer-motion';
import KnowledgeBankWrapper from '../KnowledgeBankWrapper';

const list = [
  'Helps in restoring the pH balance of the body.',
  'Reduces the excessive workload of internal organs.',
  'Helps in weight loss.',
  'Helps reduce visceral fat.',
  'Improves digestion.',
  'Cures acidity.',
  'Treats constipation.',
  'Helps maintain good GI flora.',
  'Helps improve oxygen levels in the body.',
  "Meets the requirement of the body's alkaline minerals.",
  'Helps improve bone strength, nerve function, cardiac health, and muscle function.'
];
const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delayChildren: 0.3,
      staggerChildren: 0.2
    }
  }
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1
  }
};
const BenefitsOfAlkalineWater = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Knowledge bank</SecondaryHeader>
      <section className='padding-x padding-b w-full'>
        <KnowledgeBankWrapper imgSrc='/PNOY/why-alkaline-water.png'>
          <h2 className='text-3xl text-center font-bold text-yellow uppercase'>
            Benefits of Alkaline water
          </h2>
          <motion.ul
            className='list-disc padding-x uppercase'
            variants={container}
            initial='hidden'
            animate='visible'>
            {list.map((l, index) => (
              <motion.li key={index} variants={item}>
                {l}
              </motion.li>
            ))}
          </motion.ul>
        </KnowledgeBankWrapper>
      </section>
    </LayoutWrapper>
  );
};

export default BenefitsOfAlkalineWater;
