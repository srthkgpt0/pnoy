'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import './ph-level.css';

const PhLevel = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Knowledge bank</SecondaryHeader>
      <section className='padding-x padding-y w-full'>
        <div className='relative'>
          {/* <ReactSlider
            className='bg-gray-400 w-full h-10 '
            thumbClassName=''
            trackClassName='example-track'
            renderThumb={(props, state) => (
              <div {...props} className='bg-black p-2 text-white absolute'>
                {state.valueNow}
              </div>
            )}
            renderTrack={(props, state) => {
              // console.log({ props, state });
              return (
                <div className='flex h-full'>
                  <div className='w-[20%]  bg-orange-700 rounded-lg'></div>
                  <div className='w-[20%]  bg-blue-700 rounded-lg'></div>
                  <div className='w-[20%]  bg-green-700 rounded-lg'></div>
                  <div className='w-[20%]  bg-yellow rounded-lg'></div>
                  <div className='w-[20%]  bg-red-700 rounded-lg'></div>
                </div>
              );
            }}
            marks={5}
            renderMark={(props) => {
              console.log('marks', props);
              return <div className='flex'>{props.key}</div>;
            }}
            step={10}
          /> */}
          <input
            type='range'
            min={2.5}
            max={12}
            className='w-full z-10 relative bg-transparent'
            step={0.5}
          />
          <div className='flex absolute left-0 right-0 top-0'>
            <div className='bg-blue-gray-900 py-2 w-1/5 rounded-full' />
            <div className='bg-green-400 py-2 w-1/5 rounded-full' />
            <div className='bg-red-400 py-2 w-1/5 rounded-full' />
            <div className='bg-orange-400 py-2 w-1/5 rounded-full' />
            <div className='bg-yellow py-2 w-1/5 rounded-full' />
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default PhLevel;
