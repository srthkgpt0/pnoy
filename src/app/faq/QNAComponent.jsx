const QNAComponent = ({ children }) => {
  return (
    <div className='py-8 flex flex-col gap-8'>
      <div className='text-base text-primary flex gap-1'>
        <p>Q.</p> {children[0]}
      </div>
      <div className='pl-5 text-sm text-gray-500'>{children[1]}</div>
    </div>
  );
};

export default QNAComponent;
