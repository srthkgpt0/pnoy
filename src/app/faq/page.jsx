import { SecondaryHeader } from '@/utilities';
import ContentSwitcher from './ContentSwitcher';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const Faq = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>FREQUENTLY ASKED QUESTIONS</SecondaryHeader>
      <section className='padding-x padding-y w-full'>
        <div className='flex flex-col items-center gap-5'>
          <div className='text-center'>
            <h2 className='text-3.5xl uppercase text-yellow font-semibold'>
              You have questions, we have answers!
            </h2>
            <p className='font-inter uppercase'>
              Let's discuss some regularly asked questions and clarify some of
              our doubts.
            </p>
          </div>
          <p className='py-4'>
            <img src='/PNOY/underline.svg' alt='underline' />
          </p>
        </div>
      </section>
      <section className='padding-x padding-b w-full'>
        <ContentSwitcher />
      </section>
    </LayoutWrapper>
  );
};

export default Faq;
