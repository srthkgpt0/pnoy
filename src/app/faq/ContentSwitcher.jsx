'use client';
import { useCallback, useEffect, useState } from 'react';
import { useSearchParams, useRouter, usePathname } from 'next/navigation';
import { faq } from '@/constants';
import QNAComponent from './QNAComponent';

const ContentSwitcher = () => {
  const { push } = useRouter();
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const lang = searchParams.get('lang');
  const search = searchParams.get('selected');
  const [contentToDisplay, setContentToDisplay] = useState([]);

  useEffect(() => {
    if (lang && search) {
      setContentToDisplay(faq[lang][search]);
    }
  }, [lang, search]);

  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);

      return params.toString();
    },
    [searchParams]
  );

  useEffect(() => {
    if (!searchParams.size) {
      push(
        pathname +
          '?' +
          createQueryString('selected', 'gq') +
          '&' +
          createQueryString('lang', 'en'),
        {
          scroll: false
        }
      );
    }
  }, [searchParams]);

  const contentSwitcher1 = (selectedTab) => {
    push(pathname + '?' + createQueryString('selected', selectedTab), {
      scroll: false
    });
  };
  const handleChangeLanguage = (lang) => {
    push(pathname + '?' + createQueryString('lang', lang), { scroll: false });
  };
  return (
    <div className='flex flex-col gap-10 font-inter'>
      <div className='flex justify-center gap-10 text-sm'>
        <button
          className={`uppercase text-secondary ${
            lang === 'en'
              ? 'bg-secondary rounded-full px-4 text-white py-1'
              : ''
          }`}
          onClick={() => handleChangeLanguage('en')}>
          English
        </button>
        <button
          className={`uppercase text-secondary ${
            lang === 'hi'
              ? 'bg-secondary rounded-full px-4 text-white py-1'
              : ''
          }`}
          onClick={() => handleChangeLanguage('hi')}>
          Hindi
        </button>
      </div>
      <div className='w-full bg-nav-link rounded-2xl p-6 lg:p-8 divide-y divide-primary'>
        <div className='text-lg text-gray-500 flex divide-x divide-primary pb-5'>
          <button
            className={`flex-1 py-2 ${search === 'gq' ? 'text-primary' : ''}`}
            onClick={() => contentSwitcher1('gq')}>
            GENERAL QUESTIONS
          </button>
          <button
            className={`flex-1 py-2 ${
              search === 'ionizedWater' ? 'text-primary' : ''
            }`}
            onClick={() => contentSwitcher1('ionizedWater')}>
            ABOUT THE IONIZED WATER
          </button>
          <button
            className={`flex-1 py-2 ${
              search === 'machine' ? 'text-primary' : ''
            }`}
            onClick={() => contentSwitcher1('machine')}>
            ABOUT THE MACHINE
          </button>
        </div>
        <div className='pr-10 py-5 divide-y-2 divide-gray-300'>
          {contentToDisplay.map((qna, index) => {
            return (
              <QNAComponent key={index}>
                <div>{qna.ques}</div>
                <div>
                  {qna.ans.map((a, index) => (
                    <p key={index}>{a.content}</p>
                  ))}
                </div>
              </QNAComponent>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ContentSwitcher;
