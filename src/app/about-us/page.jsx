import {
  AboutPnoy,
  Certifications,
  ExperienceComponent,
  MottoComponent
} from '@/components';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import VideoAboutUs from './VideoAboutUs';

function AboutUs() {
  return (
    <LayoutWrapper>
      <SecondaryHeader>About Us</SecondaryHeader>
      <section className='padding-y padding-x'>
        <AboutPnoy />
      </section>
      <section className='w-full padding-y lg:padding-x'>
        <ExperienceComponent />
      </section>
      <section className='w-full padding-y lg:padding-x'>
        <VideoAboutUs />
      </section>
      <section className='w-full padding-y'>
        <MottoComponent />
      </section>
      <section className='padding-y'>
        <div className='flex justify-center lg:padding-x flex-col lg:flex-row gap-10 lg:gap-0'>
          <div className='flex flex-col flex-1 gap-8 font-inter'>
            <h3 className='uppercase font-semibold text-2xl'>Our Leadership</h3>
            <div className='text-xs flex flex-col gap-4 justify-start'>
              <p>
                Dr. Sunita Nagori, a Ph.D. degree holder in the field of
                Environmental Science, has wide experience of over 20 years. A
                dynamic leader to the core, she has been working tirelessly to
                integrate this company, keeping a firm belief in the philosophy
                of 'karma', which is enshrined in the very roots of the
                organization. She has mobilized a team of like-minded,
                passionate people.
              </p>
              <p>
                Dr. Nagori has the vision to redefine health by providing
                technologically advanced ionizers. She plays an instrumental
                role in guiding all the team members and leads them from the
                front.
              </p>
              <p>
                With the upcoming generation, she wants to set an example by
                contributing to the environment. She feels it is the duty of
                every individual to do something for the upliftment of society
                as well as the environment.
              </p>
            </div>
          </div>
          <div className='flex-1 flex flex-col justify-center items-center gap-5'>
            <div
              className={`relative w-[150px] h-[150px] rounded-full overflow-hidden border-primary border-[3px]`}>
              <img
                src='/PNOY/Sunitas picture.jpeg'
                alt='testimonial'
                className='object-cover w-full h-full'
              />
            </div>

            <p className='uppercase text-primary font-semibold font-inter'>
              Dr. Sunita Nagori, MD
            </p>
          </div>
        </div>
      </section>
      <section className='pb-10 padding-x padding-y w-full'>
        <Certifications />
      </section>
    </LayoutWrapper>
  );
}

export default AboutUs;
