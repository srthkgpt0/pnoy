'use client';
import YouTube from 'react-youtube';

const VideoAboutUs = () => {
  return (
    <YouTube
      videoId='9fXJsojQoHQ'
      className='flex justify-center'
      iframeClassName='h-[300px] w-3/4'
    />
  );
};

export default VideoAboutUs;
