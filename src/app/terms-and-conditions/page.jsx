import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const TermsAndConditions = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Terms and Conditions</SecondaryHeader>
      <section className='w-full padding-x padding-y flex flex-col justify-between'>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Introduction
          </h2>
          <p>
            Greetings and welcome to PNOY Electronic India Pvt Ltd! These Terms
            and Conditions ("Terms") regulate your access to and utilization of
            our website and any associated services (collectively, the "Site").
            By accessing or using the site, you consent to abide by these Terms.
            We strongly advise you to carefully review them before proceeding.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Use of the Site
          </h2>
          <p>
            You agree to use the site solely for lawful purposes and in
            compliance with these Terms. You agree not to use the Site:
            <ul className='list-disc'>
              <li>
                In any way that violates any applicable law or regulation.
              </li>
              <li>For any illegal or unauthorized objectives.</li>
              <li>
                To encroach upon the rights of others, including their
                intellectual property rights.
              </li>
              <li>
                To transmit any harmful or malicious content, such as viruses or
                spyware.
              </li>
              <li>
                To disrupt the functioning of the site or any server or network
                linked to it.
              </li>
              <li>
                To attempt to gain unauthorized access to the site or any server
                or network linked to it.
              </li>
            </ul>
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            User Content
          </h2>
          <p>
            You assume responsibility for any content you contribute to the
            site. You affirm that you possess ownership or requisite rights to
            the user content and that it does not infringe upon the rights of
            any third party. You consent to indemnify us against any claims
            arising from your user content.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Intellectual Property
          </h2>
          <p>
            The Site and its contents, including text, graphics, logos, images,
            and software, are our property or that of our licensors and are
            protected by copyright and other intellectual property statutes. You
            are prohibited from utilizing any of the Site's content without our
            explicit written consent.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Disclaimer of Warranties
          </h2>
          <p>
            The site is provided "as is" and devoid of any warranties, express
            or implied. We disclaim all warranties, including but not confined
            to, warranties of merchantability, fitness for a particular purpose,
            and non-infringement. We do not guarantee that the site will be
            uninterrupted, error-free, or secure.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Limitation of Liability
          </h2>
          <p>
            To the fullest extent permitted by law, we will not be liable for
            any damage arising out of or in connection with your use of the
            site. This includes, but is not restricted to, direct, indirect,
            incidental, consequential, and punitive damages.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Indemnification
          </h2>
          <p>
            You agree to indemnify and safeguard us against any claims, losses,
            damages, liabilities, and expenses (including attorney's fees)
            arising from or in connection with your use of the site or your
            breach of these Terms.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Termination
          </h2>
          <p>
            We reserve the right to terminate your access to the site for any
            reason, at any time, without prior notice.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Governing Law and Jurisdiction
          </h2>
          <p>
            These terms and conditions shall be governed by and interpreted in
            accordance with the laws of Gurugram, India, without regard to its
            conflict of laws principles. Any dispute arising from or relating to
            these terms and conditions shall be subject to the exclusive
            jurisdiction of the courts of Gurugram, India.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Entire Agreement
          </h2>
          <p>
            These Terms and Conditions constitute the entire agreement between
            you and us regarding your use of the site.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Amendments
          </h2>
          <p>
            We may revise these Terms and Conditions periodically. Any
            alterations will be posted on the site, and your continued use of
            the site after such changes are posted will signify your acceptance
            of those revisions.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Contact Us
          </h2>
          <p>
            For any inquiries about these Terms and Conditions, please reach out
            to us at at sales@pnoy.in
          </p>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default TermsAndConditions;
