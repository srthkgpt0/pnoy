'use client';
import { medicalResearch } from '@/constants';
import { AccordionComponent } from '@/utilities';
import ResearchComponent from './ResearchComponent';
import ResearchInfoDetail from '../ionizedWaterBooks/ResearchInfoDetail';

const ResearchAccordion = () => {
  return (
    <>
      {medicalResearch.map((accordionDetail, index) => (
        <AccordionComponent key={index}>
          <AccordionComponent.Header>
            {accordionDetail.title}
          </AccordionComponent.Header>
          <AccordionComponent.Body>
            {accordionDetail.isScientificResearch ? (
              <section className='flex flex-wrap'>
                {accordionDetail.content.map((content, index) => (
                  <ResearchInfoDetail key={index} details={content} />
                ))}
              </section>
            ) : (
              <section>
                {accordionDetail.content.map((content, index) => (
                  <ResearchComponent key={index} {...content} />
                ))}
              </section>
            )}
          </AccordionComponent.Body>
        </AccordionComponent>
      ))}
    </>
  );
};

export default ResearchAccordion;
