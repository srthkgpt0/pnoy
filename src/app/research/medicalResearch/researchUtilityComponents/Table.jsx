import { Text } from '.';
import React from 'react';
import Link from 'next/link';

const Table = (props) => {
  const { content } = props;
  const TABLE_HEAD = [
    'Year',
    'TOPIC/SUBJECT',
    'TITLE/DESCRIPTION OF SCIENTIFIC ARTICLE'
  ];
  return (
    <div className='w-full h-full'>
      <table className='w-3/4 table-auto text-left mx-auto'>
        <thead>
          <tr>
            {TABLE_HEAD.map((head) => (
              <th
                key={head}
                className='border-b border-blue-gray-100 bg-primary p-4'>
                <p className='font-normal leading-none opacity-70 text-white'>
                  {head}
                </p>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {content.map(({ year, subject, description }, index) => (
            <tr key={subject} className='even:bg-card-bg'>
              <td className='p-5 w-1/4 font-semibold'>
                <TableData content={year} />
              </td>
              <td className='p-5'>
                <TableData content={subject} />
              </td>
              <td>
                <TableData content={description} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;

const TableData = ({ content }) => {
  return Array.isArray(content) ? (
    <div className='p-5'>
      {content.map((cont, index) => (
        <div className='mb-5' key={index}>
          <p className='text-sm inline'>{cont.content}</p>
          {cont.linkToText && (
            <Link
              href={cont.linkToText}
              rel='noopener noreferrer'
              target='_blank'
              className='bg-primary px-2 text-xs ml-3 text-white py-1 rounded-full inline-block'>
              Read More
            </Link>
          )}
        </div>
      ))}
    </div>
  ) : (
    <Text content={content} />
  );
};
