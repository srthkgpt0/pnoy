import Image from 'next/image';

const ImageComponent = (props) => {
  const { imgSrc, alt } = props;
  return (
    <div className='h-[250px] flex justify-center relative flex-shrink-0'>
      <Image src={imgSrc} alt={alt} fill className='object-contain' />
    </div>
  );
};

export default ImageComponent;
