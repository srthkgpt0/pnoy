const Heading = ({ content }) => {
  return <h3 className='font-semibold text-lg'>{content}</h3>;
};

export default Heading;
