const Text = ({ content }) => {
  return <p className='text-sm'>{content}</p>;
};

export default Text;
