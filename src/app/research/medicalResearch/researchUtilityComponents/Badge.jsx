const Badge = ({ content = [] }) => {
  return (
    <div className='flex flex-wrap justify-center'>
      {content?.map((badge, index) => (
        <div
          key={index}
          className='mx-1 my-1 bg-nav-link px-3 py-1 rounded-full'>
          {badge}
        </div>
      ))}
    </div>
  );
};

export default Badge;
