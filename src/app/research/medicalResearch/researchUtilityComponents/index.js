import Badge from './Badge';
import Heading from './Heading';
import Image from './Image';
import Table from './Table';
import Text from './Text';
import Video from './Video';

export { Text, Heading, Image, Video, Badge, Table }