import YouTube from 'react-youtube';

const Video = ({ videoId }) => {
  return (
    <YouTube
      videoId={videoId}
      className='flex justify-center'
      iframeClassName='h-[300px] w-3/4'
    />
  );
};

export default Video;
