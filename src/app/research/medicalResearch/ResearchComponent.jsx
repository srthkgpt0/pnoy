import { format } from '@/constants';
import {
  Heading,
  Image,
  Text,
  Video,
  Badge,
  Table
} from './researchUtilityComponents';

const displayElements = {
  [format.TEXT]: Text,
  [format.HEADING]: Heading,
  [format.IMAGE]: Image,
  [format.VIDEO]: Video,
  [format.BADGE]: Badge,
  [format.TABLE]: Table
};
const ResearchComponent = (props) => {
  const { format } = props;
  const Component = displayElements[format];
  return (
    <div className='mb-6 font-inter text-black antialiased'>
      <Component {...props} />
    </div>
  );
};

export default ResearchComponent;
