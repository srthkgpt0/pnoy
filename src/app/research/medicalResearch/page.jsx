import { SecondaryHeader } from '@/utilities';
import ResearchAccordion from './ResearchAccordion';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const page = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Medical Research</SecondaryHeader>
      <section className='padding-x padding-y w-full'>
        <ResearchAccordion />
      </section>
    </LayoutWrapper>
  );
};

export default page;
