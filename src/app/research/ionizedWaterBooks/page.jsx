import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import ResearchComponent from './ResearchComponent';

const ScientificResearch = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Books on ionized water</SecondaryHeader>
      <section className='lg:padding-x padding-y flex justify-center items-center'>
        <ResearchComponent />
      </section>
    </LayoutWrapper>
  );
};

export default ScientificResearch;
