import Image from 'next/image';

const ResearchInfoDetail = ({ details }) => {
  const { image, name, title, text } = details;
  return (
    <div className='flex lg:max-w-[45%] lg:h-[300px] 3xl:h-[250px] bg-card-bg rounded-xl px-4 py-4 items-center justify-center mx-6 my-6'>
      <div className='bg-nav-link rounded-xl mr-4 min-w-[200px] max-w-[200px] w-[28rem] h-[11rem] antialiased overflow-hidden'>
        <div className='relative h-full w-full'>
          <Image
            src={image.imgSrc}
            alt={image.alt}
            fill
            className='object-contain'
          />
        </div>
      </div>
      <div className='flex flex-col py-3 gap-2'>
        <h3 className='uppercase text-primary text-lg font-semibold font-goBold'>
          {title}
        </h3>
        <p className='text-black text-sm font-semibold'>{name}</p>
        <p className='border w-1/3 border-yellow'></p>
        <p>Overview</p>
        <div className='text-xs text-gray-600'>
          {text.map((t, index) => (
            <p key={index}>{t}</p>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ResearchInfoDetail;
