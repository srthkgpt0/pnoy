import { scientificResearchOnIonizedWater } from '@/constants';
import ResearchInfoDetail from './ResearchInfoDetail';

const ResearchComponent = () => {
  return (
    <div className='flex flex-wrap'>
      {scientificResearchOnIonizedWater.map((card, index) => (
        <ResearchInfoDetail details={card} key={index} />
      ))}
    </div>
  );
};

export default ResearchComponent;
