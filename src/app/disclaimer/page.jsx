import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const Disclaimer = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Disclaimer</SecondaryHeader>
      <section className='w-full padding-x padding-y  '>
        <div className='flex flex-col bg-nav-link rounded-2xl shadow-md p-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Important points to note
          </h2>
          <ul className='list-disc font-inter mb-10 px-5'>
            <li>
              All information contained in this website is correct at the time
              of development, and is subject to change without prior notice.
            </li>
            <li> Images are for illustration purposes only.</li>
            <li>
              Due to printing considerations, the actual color may vary slightly
              from those shown.
            </li>
            <li>
              PNOY reserves the right to revise any content in this website as
              it deems fit without prior notice.
            </li>
          </ul>
          <div className='font-inter'>
            <p>
              The information contained in this website is for general
              information purposes only. The information is provided by PNOY and
              while we endeavor to keep the information up to date and correct,
              we make no representations or warranties of any kind, express or
              implied, about the completeness, accuracy, reliability,
              suitability or availability with respect to the website or the
              information, products, services, or related graphics contained on
              the website for any purpose. Any reliance you place on such
              information is therefore strictly at your own risk.
            </p>
            <p>
              In no event will we be liable for any loss or damage including
              without limitation, indirect or consequential loss or damage, or
              any loss or damage whatsoever arising from loss of data or profits
              arising out of, or in connection with, the use of this website.
            </p>
            <p>
              Every effort is made to keep the website up and running smoothly.
              However, PNOY takes no responsibility for, and will not be liable
              for, the website being temporarily unavailable due to technical
              issues beyond our control.
            </p>
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default Disclaimer;
