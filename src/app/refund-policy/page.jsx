'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { motion } from 'framer-motion';

const RefundPolicy = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Refund Policy</SecondaryHeader>
      <section className='w-full padding-x padding-y flex flex-col justify-between'>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Introduction
          </h2>
          <p>
            Greetings from PNOY Electronic India Pvt Ltd! Our aim is your
            complete satisfaction with your purchase. This refund policy
            clarifies the circumstances under which you qualify for a refund and
            the process involved.
          </p>
        </motion.div>

        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Refundable Situations
          </h2>
          <p>
            We will reimburse the full purchase price of your item(s) under the
            following circumstances:
          </p>
          <ul className='list-disc'>
            <li>
              <strong>Incorrect or Incomplete Order:</strong> If you receive
              item(s) different from your order or an incomplete order.
            </li>
            <li>
              <strong>Damaged or Defective Items:</strong> If you receive
              item(s) that are damaged or defective upon arrival.
            </li>
            <li>
              <strong>Item Not as Described:</strong> If the item(s) received do
              not match the description or specifications listed on our website.
            </li>
            <li>
              <strong>Cancellation within 7 Days:</strong> You cancel your order
              within 7 days of placement.
            </li>
          </ul>
        </motion.div>

        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Non-Refundable Situations
          </h2>
          <p>Refunds cannot be provided for:</p>
          <ul className='list-disc'>
            <li>
              <strong>Used or Opened Items:</strong> Items that have been opened
              or used.
            </li>
            <li>
              <strong>Personalized or Customized Items:</strong> Custom-made or
              personalized items.
            </li>
            <li>
              <strong>Final Sale Items:</strong> Items marked as "final sale" or
              "clearance."
            </li>
            <li>
              <strong>Damage from Misuse or Neglect:</strong> Damage caused by
              misuse, neglect, or improper care.
            </li>
          </ul>
        </motion.div>

        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Refund Process
          </h2>
          <ol className='list-decimal'>
            <li>
              Contact us within 7 days of receiving your order at{' '}
              <a href='mailto:sales@pnoy.in'>sales@pnoy.in</a> or 9563309563.
            </li>
            <li>
              Briefly explain the reason for your refund request and provide
              your order number.
            </li>
            <li>
              Depending on the circumstance, we may request additional
              information or photos of the item(s).
            </li>
            <li>
              Upon approval, you will receive instructions for returning the
              item(s) (if applicable).
            </li>
            <li>
              After receiving and inspecting the returned item(s), we will
              process your refund within 14 business days.
            </li>
            <li>
              Refunds will be issued to the original payment method used for the
              purchase.
            </li>
          </ol>
        </motion.div>

        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Important Details
          </h2>
          <ul className='list-disc'>
            <li>
              Original shipping costs are non-refundable, except for cases of
              incorrect or incomplete orders.
            </li>
            <li>
              Return shipping costs may be applicable, depending on the reason
              for return.
            </li>
            <li>
              Refund processing time may vary depending on your bank or
              financial institution.
            </li>
          </ul>
        </motion.div>

        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Contact Us
          </h2>
          <p>
            For any queries regarding our refund policy, please feel free to
            reach out to us at <a href='mailto:sales@pnoy.in'>sales@pnoy.in</a>{' '}
            or 9563309563.
          </p>
        </motion.div>
      </section>
    </LayoutWrapper>
  );
};

export default RefundPolicy;
