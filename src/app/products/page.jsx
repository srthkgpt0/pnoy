'use client';
import { products } from '@/constants';
import { ProductDisplay, SecondaryHeader } from '@/utilities';
import { motion } from 'framer-motion';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import React from 'react';

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delayChildren: 0.3,
      staggerChildren: 0.2
    }
  }
};

const Products = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Our Products</SecondaryHeader>
      <section className='px-2 3xl:padding-x padding-y flex justify-center'>
        <motion.div
          className='flex flex-wrap'
          variants={container}
          initial='hidden'
          animate='visible'>
          {products.map((prod, index) => (
            <ProductDisplay key={index} product={prod} />
          ))}
        </motion.div>
      </section>
    </LayoutWrapper>
  );
};

export default Products;
