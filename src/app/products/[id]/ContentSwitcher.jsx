'use client';
import { useCallback, useEffect } from 'react';
import { displayScreens, kindsOfWater } from '@/constants';
import { useSearchParams, useRouter, usePathname } from 'next/navigation';
import ProductDetailInfo from './ProductDetailInfo';

const ContentSwitcher = ({ matchedProduct }) => {
  const { isAlkalinePurifier, productDetails, productBenefits, productUsage } =
    matchedProduct;
  const { replace, push } = useRouter();
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const search = searchParams.get('selected');

  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);

      return params.toString();
    },
    [searchParams]
  );

  useEffect(() => {
    if (!searchParams.size) {
      replace(pathname + '?' + createQueryString('selected', 'description'), {
        scroll: false
      });
    }
  }, []);

  const contentSwitcher1 = (selectedTab) => {
    push(pathname + '?' + createQueryString('selected', selectedTab), {
      scroll: false
    });
  };

  return (
    <>
      <div className='border border-yellow rounded-full uppercase text-secondary flex px-2 py-2 text-sm justify-between items-center w-fit self-center lg:self-start'>
        {(isAlkalinePurifier || productBenefits?.length) && (
          <div
            className={`${
              search === 'description'
                ? 'bg-secondary border border-secondary text-white'
                : ''
            } rounded-full px-3 py-1 cursor-pointer`}
            onClick={() => contentSwitcher1('description')}>
            Description
          </div>
        )}
        {!isAlkalinePurifier && !productBenefits?.length && (
          <h2 className='text-yellow px-3 font-inter'>Description</h2>
        )}

        {productBenefits && (
          <div
            className={`${
              search === 'benefits'
                ? 'bg-secondary border border-secondary text-white'
                : ''
            } rounded-full px-3 py-1 cursor-pointer`}
            onClick={() => contentSwitcher1('benefits')}>
            Benefits
          </div>
        )}
        {productUsage && (
          <div
            className={`${
              search === 'usage'
                ? 'bg-secondary border border-secondary text-white'
                : ''
            } rounded-full px-3 py-1 cursor-pointer`}
            onClick={() => contentSwitcher1('usage')}>
            Usage
          </div>
        )}

        {isAlkalinePurifier && (
          <div
            className={`${
              search === 'waterTypes'
                ? 'bg-secondary border border-secondary text-white'
                : ''
            } rounded-full px-3 py-1 cursor-pointer whitespace-nowrap`}
            onClick={() => contentSwitcher1('waterTypes')}>
            Kinds of water
          </div>
        )}
        {isAlkalinePurifier && (
          <div
            className={`${
              search === 'colors'
                ? 'bg-secondary border border-secondary text-white'
                : ''
            } rounded-full px-3 py-1 cursor-pointer whitespace-nowrap`}
            onClick={() => contentSwitcher1('colors')}>
            Color of the screen
          </div>
        )}
      </div>

      {search === 'description' && (
        <div className='pl-10'>
          <ul className='list-disc'>
            {productDetails?.map((detail, index) => (
              <li key={index} className='font-inter text-sm leading-6'>
                {detail}
              </li>
            ))}
          </ul>
        </div>
      )}
      {search === 'waterTypes' && (
        <div className='font-inter md:px-20 lg:px-0'>
          <h3 className='font-inter text-center lg:text-left uppercase'>
            Generates 7 kinds of water
          </h3>
          <div className='pt-5 flex justify-center lg:justify-start flex-wrap'>
            {kindsOfWater?.map((kind, index) => (
              <ProductDetailInfo key={index} detail={kind} />
            ))}
          </div>
        </div>
      )}
      {search === 'colors' && (
        <div className='md:px-20 lg:px-0'>
          <h3 className='font-inter uppercase text-center lg:text-left'>
            Different colors apprear for different ph levels
          </h3>
          <div className='pt-5 flex justify-center lg:justify-start flex-wrap'>
            {displayScreens?.map((kind, index) => (
              <ProductDetailInfo key={index} detail={kind} />
            ))}
          </div>
        </div>
      )}
      {search === 'benefits' && (
        <div className='pl-10'>
          <ul className='list-disc '>
            {productBenefits?.map((benefits, index) => (
              <li key={index} className='font-inter text-sm leading-6'>
                {benefits}
              </li>
            ))}
          </ul>
        </div>
      )}
      {search === 'usage' && (
        <div className='pl-10'>
          <ul className='list-disc '>
            {productUsage?.map((usage, index) => (
              <li key={index} className='font-inter text-sm leading-6'>
                {usage}
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};

export default ContentSwitcher;
