'use client';
import TextInput from '@/components/TextInput';
import api from '@/utilities/api';
import { useState } from 'react';

const BookYourDemo = () => {
  const [items, setItems] = useState({});
  const [toaster, setToaster] = useState(false);

  const textInputHandler = (e) => {
    const { name, value } = e.target;
    if (name === 'number') {
      const newValue = value.replace(/[^0-9]/g, '');
      setItems({ ...items, [name]: newValue });
    } else setItems({ ...items, [name]: value });
  };
  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await api.postFormData('/bookDemo', { ...items });
      setItems({});
      // setToaster(true);
    } catch (error) {
      console.error('Error submitting form:', error);
      // Handle errors
    }
  };

  return (
    <>
      <div className='w-full text-center uppercase font-bold'>
        <h1 className='text-3.5xl'>BOOK A FREE DEMO</h1>
        {/* <p className='text-sm'>& Get a Free Pre-Filter of Your RO</p> */}
      </div>
      <form>
        <div className='flex flex-col gap-5 font-inter py-10'>
          <div className='flex flex-col lg:flex-row justify-between gap-5 lg:gap-0'>
            <div>
              <TextInput
                id='name'
                label='Name'
                placeholder='Enter your name'
                type='text'
                items={items}
                onChange={textInputHandler}
              />
            </div>
            <div>
              <TextInput
                id='email'
                label='Email'
                placeholder='Enter your email id'
                type='email'
                items={items}
                onChange={textInputHandler}
              />
            </div>
            <div>
              <TextInput
                id='number'
                label='Mobile No.'
                placeholder='Enter your mobile no.'
                type='text'
                items={items}
                onChange={textInputHandler}
              />
            </div>
          </div>
          <div className='w-full'>
            <label
              htmlFor='enquiry'
              className='block text-primary text-sm font-semibold'>
              Message
            </label>
            <textarea
              id='enquiry'
              name='enquiry'
              value={items?.enquiry}
              onChange={textInputHandler}
              placeholder='Type your message'
              className='px-2 py-3 rounded-md border-gray-300 w-full placeholder:text-gray-400 placeholder:text-sm'
            />
          </div>
          <button
            onClick={handleFormSubmit}
            className='self-start bg-secondary rounded-full px-5 py-2 text-white uppercase text-sm'>
            Book my demo
          </button>
        </div>
      </form>
    </>
  );
};

export default BookYourDemo;
