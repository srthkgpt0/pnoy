import { kindsOfWaterColorVariants } from '@/constants';
import Image from 'next/image';

const ProductDetailInfo = ({ detail }) => {
  const { imgSrc, title, subHeading, ph } = detail;
  return (
    <div className='flex flex-col gap-4 mx-4 3xl:mx-5 my-3 w-28'>
      <div className='relative h-10 w-14'>
        <Image src={imgSrc} fill className='object-contain' />
      </div>
      <div className='flex flex-col gap-2 font-inter text-xs'>
        <p
          className={`uppercase font-semibold ${
            title.color ? kindsOfWaterColorVariants[title.color] : ''
          }`}>
          {title.name ? title.name : title}
        </p>
        {subHeading && (
          <p className='uppercase text-[#979797] font-semibold'>{subHeading}</p>
        )}
        <p className='uppercase text-[#979797] font-semibold'>{ph}</p>
      </div>
    </div>
  );
};

export default ProductDetailInfo;
