'use client';
import { SecondaryHeader } from '@/utilities';
import Image from 'next/image';
import React, { useCallback, useEffect, useState } from 'react';
import ContentSwitcher from './ContentSwitcher';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { notFound, useParams } from 'next/navigation';
import { products } from '@/constants';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import BookYourDemo from './BookYourDemo';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 1
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

const ProductDetail = () => {
  const { id } = useParams();
  const [matchedProduct, setMatchedProduct] = useState({});
  const findMatchedProduct = useCallback(
    () => products.find((prod) => prod.productId === id),
    [id]
  );
  useEffect(() => {
    const matchedProductFound = findMatchedProduct();
    if (matchedProductFound) {
      setMatchedProduct(matchedProductFound);
    } else {
      notFound();
    }
  }, []);

  const {
    productName,
    productPaths,
    productCode,
    productDescription,
    isAlkalinePurifier
  } = matchedProduct;

  const CustomDot = ({ onClick, ...rest }) => {
    const { active } = rest;
    return (
      <button
        className={`mx-1 mb-16 transition-all duration-500
         ${
           active
             ? 'bg-gray-800 w-4 h-2 rounded-full'
             : 'bg-gray-500 w-2 h-2 rounded-full'
         }`}
        onClick={() => onClick()}
      />
    );
  };

  return (
    <LayoutWrapper>
      <SecondaryHeader>{productName}</SecondaryHeader>
      <section className='lg:padding-x padding-y w-full'>
        <div className='flex flex-col lg:flex-row gap-20'>
          <div className='lg:w-2/5 lg:h-[600px] relative'>
            <div className='rounded-full bg-yellow aspect-square'>
              {productPaths?.length && (
                <Carousel
                  containerClass='carousel-container h-full overflow-y-visible'
                  itemClass='carousel-item h-full flex justify-center items-end'
                  sliderClass='h-full'
                  showDots
                  renderDotsOutside
                  ssr
                  customDot={<CustomDot />}
                  autoPlay
                  autoPlaySpeed={4000}
                  keyBoardControl={true}
                  arrows={false}
                  infinite
                  pauseOnHover={true}
                  responsive={responsive}>
                  {productPaths?.map((te, index) => {
                    if (te.type === 'image') {
                      return (
                        <img
                          key={index}
                          src={te.path}
                          fill
                          className='object-contain h-4/6 w-4/6'
                          alt={productName}
                        />
                      );
                    } else {
                      return (
                        <video controls width='350'>
                          <source src={te.path} type='video/mp4' />
                        </video>
                      );
                    }
                  })}
                </Carousel>
              )}
            </div>
          </div>
          <div className='flex flex-col gap-8 lg:w-3/5'>
            <div className='flex flex-col lg:flex-row lg:divide-x-2 items-center'>
              <div className='uppercase padding-r'>
                <p className='text-primary font-semibold text-3xl whitespace-nowrap'>
                  {productName}
                </p>
                <p className='font-inter text-lg leading-8'>{productCode}</p>
                <p className='font-inter text-lg leading-8'>
                  {productDescription}
                </p>
              </div>
              <div className='padding-l flex-grow hidden lg:block'>
                <div className='h-[100px] w-[200px] relative'>
                  <Image
                    src='/PNOY/availableAtAmazon.jpeg'
                    alt='availableAtAmazon'
                    fill
                    className='object-contain'
                  />
                </div>
              </div>
            </div>
            <ContentSwitcher matchedProduct={matchedProduct} />
          </div>
        </div>
      </section>
      {isAlkalinePurifier && (
        <section className='w-full padding-x'>
          <div className='bg-nav-link text-[#ee0b0b] py-5 text-xs text-center rounded-2xl font-inter'>
            <p>
              *Value of pH depends on the supplied water quality as well as the
              flow of water
            </p>
            <p>
              **The colours of pH value shown above may differ from the actual
              test results
            </p>
          </div>
        </section>
      )}
      <section className='w-full padding-x padding-y'>
        <BookYourDemo />
      </section>
    </LayoutWrapper>
  );
};

export default ProductDetail;
