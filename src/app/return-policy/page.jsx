'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { motion } from 'framer-motion';

const RefundPolicy = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Return Policy</SecondaryHeader>
      <section className='w-full padding-x padding-y flex flex-col justify-between'>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Introduction
          </h2>
          <p>
            Thank you for choosing PNOY Electronic India Pvt Ltd for your
            shopping needs! We sincerely hope that you are delighted with your
            purchase. However, if for any reason you find yourself less than
            completely satisfied, we are pleased to provide options for returns
            or exchanges on most items. We kindly ask you to review this policy
            thoroughly before proceeding with a return.
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Eligible Items for Return
          </h2>
          <p>
            You may return most items purchased from PNOY Electronic India Pvt
            Ltd within 7 days from the delivery date. However, the following
            items are not eligible for return:
            <ul className='list-disc'>
              <li>Items labeled as "final sale"or "clearance"</li>
              <li>Personalized or customized items.</li>
            </ul>
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Return Criteria
          </h2>
          <p>
            For an item to qualify for a return, it must meet the following
            conditions: You may return most items purchased from PNOY Electronic
            India Pvt Ltd within 7 days from the delivery date. However, the
            following items are not eligible for return:
            <ul className='list-disc'>
              <li>All tags and accessories must be attached</li>
              <li>Personalized or customized items.</li>
              <li>It must be in the same condition as when received</li>
            </ul>
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            How to Initiate a Return
          </h2>
          <p>
            To start a return process, kindly follow these steps:
            <ul className='list-disc'>
              <li>Contact us at sales@pnoy.in or 9563309563.</li>
              <li>
                Provide your order number and specify the item(s) you intend to
                return.
              </li>
              <li>We will furnish you with a return shipping label.</li>
              <li>
                Securely pack the item in its original packaging and affix the
                provided return shipping label.
              </li>
              <li>Send the item back to us.</li>
            </ul>
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Refunds and Exchanges
          </h2>
          <p>
            Upon receipt of your returned item, we will assess it to ensure it
            meets the return criteria. If approved, you will receive:
            <ul className='list-disc'>
              <li>
                A full refund for the purchase price of the item(s), minus any
                initial shipping costs.
              </li>
              <li>
                Alternatively, store credit equivalent to the full purchase
                price of the item(s) if preferred.
              </li>
              <li>
                An exchange for another item of equal or lesser value, upon
                request.
              </li>
            </ul>
            Please note that it may take up to 14 business days for your refund
            or exchange to be processed. You will receive a confirmation email
            once your return has been handled.
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Shipping Expenses
          </h2>
          <p>
            Return shipping costs are as follows:
            <ul className='list-disc'>
              <li>Free for orders exceeding 2,00,000.</li>
              <li>300 for orders above 3000.</li>
            </ul>
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            International Return Policy
          </h2>
          <p>
            Returns from international destinations are subject to distinct
            terms and conditions. Please contact us at{' '}
            <a href='mailto:sales@pnoy.in'>sales@pnoy.in</a> for further details
            regarding international returns.
          </p>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: 50 }}
          animate={{ opacity: 1, translateY: 0 }}
          transition={{ duration: 0.5 }}
          className='bg-nav-link p-10  w-full flex flex-col justify-between rounded-2xl shadow-md  font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Contact Information
          </h2>
          <p>
            If you have any inquiries regarding our return policy, please do not
            hesitate to reach out to us at{' '}
            <a href='mailto:sales@pnoy.in'>sales@pnoy.in</a>.
          </p>
        </motion.div>
      </section>
    </LayoutWrapper>
  );
};

export default RefundPolicy;
