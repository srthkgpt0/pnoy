import { FadeInWhenVisible, TabComponent } from '@/components';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const Technology = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Technology</SecondaryHeader>
      <section className='w-full padding-x padding-y'>
        <div className='justify-between flex flex-col-reverse lg:flex-row mx-auto gap-10 lg:gap-0'>
          <div className='flex flex-col font-inter lg:w-[48%] gap-8'>
            <h2 className='uppercase  font-semibold text-2xl'>
              Technology used in PNOY's Ionizers
            </h2>
            <div className='text-xs flex flex-col gap-4 justify-center items-center'>
              <p>
                The basic principle of electrolysis of water has been used in
                the technology of PNOY's Ionizers. In general, water cannot
                decompose into O2 and H2 in its natural state. However, by
                applying Faraday's Law of Electrolysis, which involves adding a
                cathode and anode into the water and passing a current across
                it, the water is electrolyzed to separate the hydrogen (H+) ion
                and the hydroxide (OH-) ion. This electrolysis process is
                performed in an electrolysis chamber equipped with
                platinum-coated titanium electrodes and a semi-permeable ion
                exchange membrane. This membrane prevents the catholyte (with
                the alkaline OH ion) and analyte (with the acidic H+ ions)
                compartments from getting mixed. Thus, alkaline (mild or strong)
                and acidic (mild or strong) water is produced at the cathode and
                anode, respectively. Oxidation-reduction reactions occur at the
                cathode (negative electrode) and anode (positive electrode), and
                H2 and O2 are produced at the surface of the cathode and anode.
                The water around the electrodes becomes alkaline and acidic, and
                the oxidation-reduction potential (ORP) of the water also
                changes. The acidic (oxidized) water with a high concentration
                of hydrogen ions is collected from the positive pole, while the
                alkaline (reduced) water is collected from the negative pole.
                Through electrolysis, alkaline water not only gains an excess
                amount of electrons but also the clusters of water seem to be
                reduced in size. This reduced water is dispensed from the main
                faucet, while the oxidized water is dispensed from a separate
                hose.
              </p>
              <p>
                The electrolyzer at the heart of the alkaline Ionized water
                generation process features high-performance pure titanium
                electrodes with a platinum coating that is very resistant to
                deterioration. Catalyzing the electrolysis in water Ionizers is
                absolutely essential. No titanium alloy is used in PNOY's
                Ionizers.
              </p>
            </div>
          </div>
          <div className='lg:w-[48%]'>
            <img src='/PNOY/tech-1.jpg' alt='tech-1' />
          </div>
        </div>
      </section>
      <section className='padding-t relative'>
        <FadeInWhenVisible className='bg-nav-link w-full font-inter relative padding-t padding-x pb-60 max-2xl:max-w-screen-xl flex flex-col items-center gap-5 rounded-3xl'>
          <h2 className='uppercase font-semibold text-2xl z-10'>
            How is Alkaline Ionized Water Generated?
          </h2>
          <div className='text-xs flex flex-col gap-4 justify-center items-center z-10'>
            <p className='text-center'>
              The quality of water we drink determines the quality of our
              health. To keep us healthy, our bodies maintain just the right
              internal conditions such as the concentration of electrolytes,
              body temperature, and pH. Our body contains a lot of water—around
              60% of an adult's body weight is body fluid. The water we drink is
              absorbed by the intestine and circulated throughout the body in
              the form of body fluid such as blood. A total of about 7200L of
              blood per day circulates around the body, carrying oxygen, carbon
              dioxide, nutrients, and metabolic waste. Blood also eliminates
              bacteria and other foreign materials from our body, so it plays a
              very important role in keeping us alive. Body fluid contains
              electrolytes that have a vital role. If electrolyte levels are too
              low or too high, cell and organ functions will decline, which
              could lead to life-threatening conditions. These electrolytes
              include Sodium (Na), Potassium (K), Calcium (Ca), Magnesium (Mg),
              and Chlorides, and they are absorbed by the water we drink, which
              maintains the function of muscles and nerve cells. Therefore, an
              adequate supply of clean and mineral-rich high-quality drinking
              water is the key to a healthy body.
            </p>
            <div className='absolute h-full w-full top-0 left-0 right-0 bottom-0 overflow-hidden'>
              <img
                src='/PNOY/texture_rounded_rec.png'
                alt='texture_rounded_rec'
                className='w-full h-full'
              />
            </div>
          </div>
        </FadeInWhenVisible>
      </section>
      <section className='padding-x -translate-y-40'>
        <img
          src='/PNOY/ionized-water-generation.png'
          alt='ionized-water-generation'
          className='mx-auto'
        />
      </section>
      <section className='padding-x padding-y lg:h-[480px]'>
        <TabComponent />
      </section>
    </LayoutWrapper>
  );
};

export default Technology;
