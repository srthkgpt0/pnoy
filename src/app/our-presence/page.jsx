'use client';
import { ourPresence } from '@/constants';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import {
  GoogleMap,
  Marker,
  useLoadScript,
  useJsApiLoader
} from '@react-google-maps/api';
import React, { useMemo } from 'react';

const OurPresence = () => {
  const containerStyle = {
    width: '100%',
    height: '100%'
  };

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: 'AIzaSyDkDJ_Xln5_nDMabRlxpaPLh2SQSRSumLQ'
  });
  const center = useMemo(
    () => ({
      lat: 20.5937,
      lng: 78.9629
    }),
    []
  );

  return (
    <LayoutWrapper>
      <SecondaryHeader>Our presence</SecondaryHeader>
      <section className='padding-x padding-y w-full flex flex-col lg:flex-row justify-between gap-10 lg:gap-0'>
        <div className='w-full h-[700px] lg:w-4/6 px-5 border border-yellow p-5 mr-5'>
          {isLoaded ? (
            <GoogleMap
              mapContainerStyle={containerStyle}
              center={center}
              zoom={4}>
              {/* Child components, such as markers, info windows, etc. */}
              {ourPresence?.map(({ latitude, longitude }, index) => (
                <Marker
                  key={index}
                  position={{ lat: latitude, lng: longitude }}
                />
              ))}
            </GoogleMap>
          ) : (
            <></>
          )}
        </div>
        <div className='lg:w-1/3 px-5 h-[700px] border border-yellow p-5'>
          <div className='border'>
            <input placeholder='Search' className='w-full h-full' />
          </div>
          <div className='h-[615px] overflow-auto'>
            {ourPresence?.map(({ branchCity, branchAddress }, index) => (
              <div className='bg-card-bg p-3 my-5' key={index}>
                <p className='font-semibold'>{branchCity}</p>
                <p className='text-gray-500'>{branchAddress}</p>
              </div>
            ))}
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default OurPresence;
