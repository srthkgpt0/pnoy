import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const MedicalDisclaimer = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Medical Disclaimer</SecondaryHeader>
      <section className='w-full padding-x padding-y flex justify-between'>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter'>
          <p className='mb-5'>
            The information on this website/mobile app and our youtube channel
            is not intended or implied to be a substitute for professional
            medical advice, diagnosis or treatment. All content, including text,
            video, graphics, images and information, contained on or available
            through this website/mobile app is for general information purposes
            only.
          </p>
          <p className='mb-5'>
            PNOY electronic India Pvt Ltd makes no representation and assumes no
            responsibility for the accuracy of information contained on or
            available through this website/mobile app and such information is
            subject to change without notice. You are encouraged to confirm any
            information obtained from or through this website/mobile app with
            other sources, and review all information regarding any medical
            condition or treatment with your physician.
          </p>
          <p className='mb-5'>
            Never disregard professional medical advice or delay seeking medical
            treatment because of something you have read on or accessed through
            this web site, app or video.
          </p>
          <p className='mb-5'>
            PNOY electronic India Pvt Ltd does not recommend, endorse or make
            any representation about the efficacy, appropriateness or
            suitability of any specific tests, products, procedures, treatments,
            services, opinions, health care providers or other information that
            may be contained on or available through this web site.
          </p>
          <p className='mb-5'>
            Pnoy electronic India Pvt ltd is not responsible nor liable for any
            advice, course of treatment, diagnosis or any other information,
            services or products that you obtain through this web site or mobile
            app .”
          </p>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default MedicalDisclaimer;
