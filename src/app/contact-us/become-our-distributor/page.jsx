'use client';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { useState } from 'react';
import TextInput from '../../../components/TextInput';
import { CitySelect, StateSelect } from 'react-country-state-city';
import 'react-country-state-city/dist/react-country-state-city.css';
import api from '@/utilities/api';

const Distributor = () => {
  const [items, setItems] = useState({});

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Create an instance of FormData
    const formData = new FormData();

    // Append text fields to formData
    Object.keys(items).forEach((key) => {
      if (
        key !== 'state' &&
        key !== 'city' &&
        key !== 'gstDetails' &&
        key !== 'panDetails' &&
        key !== 'aadhaarDetails'
      ) {
        formData.append(key, items[key]);
      }
    });

    // Append selected state and city names, if available
    if (items.state) formData.append('state', items.state.name);
    if (items.city) formData.append('city', items.city.name);

    // Append files to formData
    if (items.gstDetails) formData.append('gstDetails', items.gstDetails);
    if (items.panDetails) formData.append('panDetails', items.panDetails);
    if (items.aadhaarDetails)
      formData.append('aadhaarDetails', items.aadhaarDetails);

    try {
      const response = await api.post('/becomeOurDistributor', formData);
      setItems({});
      // Handle successful response
    } catch (error) {
      console.error('Error submitting form:', error);
      // Handle errors
    }
  };

  const handleTextInput = (e) => {
    const { name, value } = e.target;
    if (name === 'number') {
      const newValue = value.replace(/[^0-9]/g, '');
      setItems({ ...items, [name]: newValue });
    } else setItems({ ...items, [name]: value });
  };
  const onFileChange = (e) => {
    const { name } = e.target;
    setItems({ ...items, [name]: e.target.files[0] });
  };
  const onChangeCheckBox = (e) => {
    setItems({ ...items, [e.target.name]: e.target.checked });
  };
  return (
    <LayoutWrapper>
      <SecondaryHeader>Become our distributor</SecondaryHeader>
      <section className='padding-x padding-y rounded-3xl w-full bg-nav-link margin-y'>
        <div className='p-8 bg-white rounded-3xl'>
          <form>
            <div className='flex flex-col justify-between gap-10'>
              <div className='flex flex-col gap-10'>
                <div className='flex flex-col gap-5'>
                  <h4 className='uppercase text-yellow text-lg'>
                    Basic details
                  </h4>
                  <div className='flex flex-col lg:flex-row gap-4 lg:gap-0 justify-between'>
                    <div className='lg:w-[28%]'>
                      <TextInput
                        id='name'
                        label='Name'
                        items={items}
                        type='text'
                        placeholder='Enter your name'
                        onChange={handleTextInput}
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <TextInput
                        id='email'
                        label='Email Id'
                        items={items}
                        type='email'
                        placeholder='Enter your Email'
                        onChange={handleTextInput}
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <TextInput
                        id='number'
                        label='Mobile No. (Whatsapp)'
                        items={items}
                        type='text'
                        placeholder='Enter your mobile no.'
                        onChange={handleTextInput}
                      />
                    </div>
                  </div>
                </div>
                <div className='flex flex-col gap-5'>
                  <h4 className='uppercase text-yellow text-lg'>
                    Business details
                  </h4>
                  <div className='flex flex-col lg:flex-row justify-between flex-wrap'>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='organizationName'
                        label='Organization Name'
                        onChange={handleTextInput}
                        items={items}
                        type='text'
                        placeholder='Enter your Organization Name'
                      />
                    </div>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='currentBusiness'
                        onChange={handleTextInput}
                        label='Current Business'
                        items={items}
                        type='text'
                        placeholder='Enter your Current Business'
                      />
                    </div>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='ROServicingBusiness'
                        label='Since How Many Years You Are In RO Servicing Business'
                        onChange={handleTextInput}
                        type='number'
                        items={items}
                        placeholder='Type your message'
                      />
                    </div>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='clientDatabase'
                        label='How Much Client Database (Approx Number) You Have?'
                        onChange={handleTextInput}
                        type='text'
                        items={items}
                        placeholder='Type your message'
                      />
                    </div>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='serviceExecutive'
                        label='How Many Service Executive You Have?'
                        onChange={handleTextInput}
                        type='text'
                        items={items}
                        placeholder='Type your message'
                      />
                    </div>
                    <div className='lg:w-[28%] mb-5'>
                      <TextInput
                        id='serviceCalls'
                        label='How Many Service Calls You Receive Each Day?'
                        onChange={handleTextInput}
                        type='text'
                        items={items}
                        placeholder='Type your message'
                      />
                    </div>
                  </div>
                </div>
                <div className='flex flex-col gap-5'>
                  <h4 className='uppercase text-yellow text-lg'>
                    Postal Address
                  </h4>
                  <div className='flex flex-col lg:flex-row justify-between gap-4 lg:gap-0'>
                    <div className='lg:w-[28%]'>
                      <TextInput
                        id='businessAddress'
                        onChange={handleTextInput}
                        label='Business Address'
                        items={items}
                        type='text'
                        placeholder='Type your message'
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='state'
                        className='block text-primary text-xs font-semibold'>
                        State
                      </label>
                      <StateSelect
                        countryid={101}
                        onChange={(e) => {
                          setItems({ ...items, state: e });
                        }}
                        placeHolder='Select State'
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='city'
                        className='block text-primary text-xs font-semibold'>
                        City
                      </label>
                      <CitySelect
                        countryid={101}
                        stateid={items?.state?.id}
                        onChange={(e) => {
                          setItems({ ...items, city: e });
                        }}
                        placeHolder='Select City'
                      />
                    </div>
                  </div>
                  <div>
                    <div className='lg:w-[28%]'>
                      <TextInput
                        id='pinCode'
                        label='Pin Code'
                        onChange={handleTextInput}
                        type='text'
                        items={items}
                        placeholder='Type your pin code'
                      />
                    </div>
                  </div>
                </div>
                <div className='flex flex-col gap-5'>
                  <h4 className='uppercase text-yellow text-lg'>
                    Documents Needed
                  </h4>
                  <div className='flex flex-col lg:flex-row justify-between gap-4 lg:gap-0'>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='passportPhoto'
                        className='block text-primary text-xs font-semibold'>
                        Your Passport Size Photo
                      </label>
                      <input
                        type='file'
                        accept='.pdf, .jpeg, .jpg, .png'
                        id='passportPhoto'
                        name='passportPhoto'
                        onChange={onFileChange}
                        placeholder='Drag or upload'
                        className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full border-[1px] focus:outline-primary outline-gray-300'
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='businessAddress'
                        className='block text-primary text-xs font-semibold'>
                        GST Copy Upload
                      </label>
                      <input
                        type='file'
                        accept='.pdf, .docx, .doc'
                        id='gstDetails'
                        name='gstDetails'
                        onChange={onFileChange}
                        placeholder='Drag or upload'
                        className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full border-[1px] focus:outline-primary outline-gray-300'
                      />
                    </div>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='state'
                        className='block text-primary text-xs font-semibold'>
                        PAN Copy Upload
                      </label>
                      <input
                        type='file'
                        accept='.pdf, .docx, .doc'
                        id='panDetails'
                        name='panDetails'
                        onChange={onFileChange}
                        placeholder='Drag or upload'
                        className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full border-[1px] focus:outline-primary outline-gray-300'
                      />
                    </div>
                  </div>
                  <div>
                    <div className='lg:w-[28%]'>
                      <label
                        htmlFor='city'
                        className='block text-primary text-xs font-semibold'>
                        Aadhaar Copy Upload
                      </label>
                      <input
                        type='file'
                        accept='.pdf, .docx, .doc'
                        id='aadhaarDetails'
                        name='aadhaarDetails'
                        onChange={onFileChange}
                        placeholder='Drag or upload'
                        className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full border-[1px] focus:outline-primary outline-gray-300'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <input
                  type='checkbox'
                  id='t&c'
                  name='termsAndCondition'
                  onChange={onChangeCheckBox}
                  className='focus:border-none border-yellow'
                />
                <label htmlFor='t$c' className='px-3 font-inter text-sm '>
                  I agree with PNOY terms and conditions
                </label>
              </div>
              <button
                className='self-start px-5 py-1 text-sm bg-secondary uppercase text-white rounded-full'
                disabled={
                  items.termsAndCondition ? !items.termsAndCondition : true
                }
                onClick={handleSubmit}>
                Submit
              </button>
            </div>
          </form>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default Distributor;
