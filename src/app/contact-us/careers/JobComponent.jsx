'use client';
import Image from 'next/image';
import { useRouter } from 'next/navigation';

const JobComponent = ({ careerDetail }) => {
  const { push } = useRouter();
  const handleClick = () => {
    push(`/contact-us/careers/${careerDetail.careerId}`);
  };
  return (
    <div className='font-inter bg-nav-link p-8 rounded-3xl gap-3 flex flex-col justify-between w-[30%] mx-3 3xl:mx-5 my-3 3xl:my-5 hover:shadow-md hover:shadow-yellow'>
      <div className='flex flex-col gap-5 justify-between'>
        <div className='relative h-14 w-14 rounded-full overflow-hidden'>
          <Image
            src='/PNOY/testimonial.jpeg'
            alt='career'
            fill
            className='object-cover'
          />
        </div>
        <h2 className='text-primary uppercase text-lg font-semibold'>
          {careerDetail.title}
        </h2>
      </div>
      <div className='flex flex-col gap-3'>
        <p className='text-sm'>Location: {careerDetail.location}</p>
        <div className='mr-auto w-[80%] 3xl:w-[70%] justify-between flex'>
          <button
            className='border-[1px] rounded-full text-secondary border-secondary px-3 py-1 text-xs uppercase'
            onClick={handleClick}>
            Read More
          </button>
          <button
            className='border-[1px] rounded-full text-white border-secondary bg-secondary px-3 py-1 text-xs uppercase'
            onClick={handleClick}>
            Apply Now
          </button>
        </div>
      </div>
    </div>
  );
};

export default JobComponent;
