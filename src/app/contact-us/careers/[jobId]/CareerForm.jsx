'use client';
const CareerForm = () => {
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className='flex flex-col gap-5 font-inter'>
          <div className='flex flex-wrap justify-between'>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='firstName'
                className='block text-primary text-sm font-semibold'>
                First Name
              </label>
              <input
                type='text'
                id='firstName'
                placeholder='Enter your first name'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='lastName'
                className='block text-primary text-sm font-semibold'>
                Last Name
              </label>
              <input
                type='text'
                id='lastName'
                placeholder='Enter your last name'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='email'
                className='block text-primary text-sm font-semibold'>
                Email
              </label>
              <input
                type='email'
                id='email'
                placeholder='Enter your email id'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='number'
                className='block text-primary text-sm font-semibold'>
                Mobile No.
              </label>
              <input
                type='text'
                id='number'
                placeholder='Enter your mobile no.'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='dob'
                className='block text-primary text-sm font-semibold'>
                Date of Birth
              </label>
              <input
                type='date'
                id='dob'
                placeholder='Enter your date of birth'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='position'
                className='block text-primary text-sm font-semibold'>
                Position
              </label>
              <select
                id='position'
                className='border-gray-300 rounded-md px-2 py-2 w-full'>
                <option
                  className='px-5 py-2 '
                  value='business-development-manager'>
                  Hiring are open for Business Development Manager
                </option>
                <option className='px-5 py-2' value='tele-caller'>
                  Hiring are open for Tele-Caller
                </option>
                <option className='px-5 py-2' value='other'>
                  Other
                </option>
              </select>
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='state'
                className='block text-primary text-sm font-semibold'>
                State
              </label>
              <select
                id='state'
                className='border-gray-300 rounded-md px-2 py-3 w-full'>
                <option className='px-5 py-2'>Madhya Pradesh</option>
              </select>
            </div>
            <div className='w-2/5 my-5'>
              <label
                htmlFor='city'
                className='block text-primary text-sm font-semibold'>
                City
              </label>
              <select className='border-gray-300 rounded-md px-2 py-3 w-full'>
                <option className='px-5 py-2'>Indore</option>
              </select>
            </div>
            <div className='w-full my-5'>
              <label
                htmlFor='experience'
                className='block text-primary text-sm font-semibold'>
                Experience (if any)
              </label>
              <input
                type='text'
                id='experience'
                placeholder='Enter your Experience'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
              />
            </div>
            <div className='w-full my-5'>
              <label
                htmlFor='resume'
                className='block text-primary text-sm font-semibold'>
                Upload Resume (.pdf/.docx/.doc) allowed
              </label>
              <input
                type='file'
                accept='.pdf, .docx, .doc'
                id='resume'
                placeholder='Drag or upload'
                className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full border-[1px] focus:outline-primary outline-gray-300'
              />
            </div>
            <button
              type='submit'
              className='self-start bg-secondary rounded-full px-5 py-2 text-white uppercase text-sm my-5'>
              Send Application
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default CareerForm;
