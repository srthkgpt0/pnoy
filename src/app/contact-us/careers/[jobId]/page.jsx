'use client';
import { careers } from '@/constants';
import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import { useParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import CareerSwitcher from './CareerSwitcher';
import CareerForm from './CareerForm';

const Jobs = () => {
  const { jobId } = useParams();
  const [careerDetails, setCareerDetails] = useState({});
  useEffect(() => {
    if (jobId) {
      setCareerDetails(careers.find((c) => c.careerId === jobId));
    }
  }, [jobId]);

  return (
    <LayoutWrapper>
      <SecondaryHeader>Careers</SecondaryHeader>
      <section className='padding-x padding-y w-full'>
        <div className='flex flex-col'>
          <h2 className='text-3xl self-center font-bold text-yellow uppercase mb-16'>
            {careerDetails.title}
          </h2>
          <div className='flex justify-center items-start'>
            <CareerSwitcher details={careerDetails} />
            <div className='flex-1 padding-l'>
              <CareerForm />
            </div>
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default Jobs;
