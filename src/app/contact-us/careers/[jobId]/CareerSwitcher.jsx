import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useCallback, useEffect } from 'react';

const CareerSwitcher = ({ details }) => {
  const { replace, push } = useRouter();
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const search = searchParams.get('selected');
  const createQueryString = useCallback(
    (name, value) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);

      return params.toString();
    },
    [searchParams]
  );

  useEffect(() => {
    if (!searchParams.size) {
      replace(pathname + '?' + createQueryString('selected', 'eligibility'), {
        scroll: false
      });
    }
  }, []);

  const contentSwitcher = (selectedTab) => {
    push(pathname + '?' + createQueryString('selected', selectedTab), {
      scroll: false
    });
  };

  return (
    <div className='bg-card-bg w-1/3 px-10 py-10 rounded-2xl flex flex-col h-full'>
      <div className='flex border border-yellow rounded-full px-2 py-1 w-10/12 3xl:w-3/4 self-center justify-between items-center mb-8'>
        <button
          className={`px-5 py-1 rounded-full font-inter uppercase text-xs ${
            search === 'eligibility'
              ? 'bg-secondary text-white'
              : 'text-primary'
          }`}
          onClick={() => contentSwitcher('eligibility')}>
          Eligibility
        </button>
        <button
          className={`px-5 py-1 rounded-full font-inter uppercase text-xs ${
            search === 'expectations'
              ? 'bg-secondary text-white'
              : 'text-primary'
          }`}
          onClick={() => contentSwitcher('expectations')}>
          Expectations
        </button>
      </div>
      <div className='flex justify-center items-center'>
        <ul className='list-disc font-inter text-sm'>
          {details?.[search]?.map((eligible, index) => (
            <li key={index} className='py-2'>
              {eligible}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default CareerSwitcher;
