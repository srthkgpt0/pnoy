import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import JobComponent from './JobComponent';
import { careers } from '@/constants';

const Careers = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Careers</SecondaryHeader>
      <section className='padding-x padding-y w-full'>
        <div className='flex flex-col'>
          <h2 className='text-3xl self-center font-bold text-yellow uppercase mb-16'>
            Latest job openings
          </h2>
          <div className='flex flex-wrap justify-start'>
            {careers.map((career, index) => (
              <JobComponent careerDetail={career} key={index} />
            ))}
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default Careers;
