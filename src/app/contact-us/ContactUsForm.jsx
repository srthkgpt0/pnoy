'use client';

import TextInput from '@/components/TextInput';
import api from '@/utilities/api';
import { useState } from 'react';
import { CitySelect, StateSelect } from 'react-country-state-city';
import 'react-country-state-city/dist/react-country-state-city.css';

const ContactUsForm = () => {
  const [items, setItems] = useState({});
  const textInputHandler = (e) => {
    const { name, value } = e.target;
    setItems({ ...items, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await api.postFormData('/contactUs', {
        ...items,
        state: items.state.name,
        city: items.city.name
      });
      setItems({});
      // setToaster(true);
    } catch (error) {
      console.error('Error submitting form:', error);
      // Handle errors
    }
  };
  return (
    <form>
      <div className='flex flex-col lg:flex-row flex-wrap justify-between'>
        <div className='lg:w-2/5 my-5'>
          <TextInput
            id='name'
            label='Name'
            placeholder='Enter your name'
            type='text'
            items={items}
            onChange={textInputHandler}
          />
        </div>
        <div className='lg:w-2/5 my-5'>
          <TextInput
            id='email'
            label='Email'
            placeholder='Enter your email id'
            type='email'
            items={items}
            onChange={textInputHandler}
          />
        </div>
        <div className='lg:w-2/5 my-5'>
          <TextInput
            id='number'
            label='Mobile No.'
            placeholder='Enter your mobile no.'
            type='number'
            items={items}
            onChange={textInputHandler}
          />
        </div>
        <div className='lg:w-2/5 my-5'>
          <label
            htmlFor='serviceNeeded'
            className='block text-primary text-xs font-semibold'>
            Service Needed
          </label>
          <select
            type='text'
            id='serviceNeeded'
            name='serviceNeeded'
            placeholder='Enter your email id'
            onChange={textInputHandler}
            className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'>
            <option>--Select--</option>
            <option value='enquiry'>Enquiry</option>
            <option value='requestSupport'>Request Support</option>
            <option value='bookDemo'>Book Demo</option>
            <option value='bulkOrder'>Bulk Order</option>
            <option value='other'>Other</option>
          </select>
        </div>
        <div className='lg:w-2/5 my-5'>
          <label
            htmlFor='state'
            className='block text-primary text-xs font-semibold'>
            State
          </label>
          <StateSelect
            countryid={101}
            onChange={(e) => {
              setItems({ ...items, state: e });
            }}
            placeHolder='Select State'
          />
        </div>
        <div className='lg:w-2/5 my-5'>
          <label
            htmlFor='city'
            className='block text-primary text-xs font-semibold'>
            City
          </label>
          <CitySelect
            countryid={101}
            stateid={items?.state?.id}
            onChange={(e) => {
              setItems({ ...items, city: e });
            }}
            placeHolder='Select City'
          />
        </div>
        <div className='w-full my-5'>
          <TextInput
            id='message'
            label='Your Message'
            placeholder='Enter your message'
            type='text'
            items={items}
            onChange={textInputHandler}
          />
        </div>
        <button
          className='uppercase bg-secondary text-white px-3 py-1 rounded-full mt-5'
          onClick={handleSubmit}>
          Submit
        </button>
      </div>
    </form>
  );
};

export default ContactUsForm;
