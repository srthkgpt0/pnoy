import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';
import Image from 'next/image';
import ContactUsForm from './ContactUsForm';
import Link from 'next/link';

const ReachUs = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Contact Us</SecondaryHeader>
      <section className='padding-x w-full'>
        <div className='flex flex-col-reverse lg:flex-row gap-8 lg:gap-0'>
          <div className='py-10 px-8 bg-yellow lg:w-[40%] 3xl:w-1/3 flex flex-col gap-12 relative'>
            <h2 className='text-3xl self-center font-bold text-primary uppercase'>
              Contact Information
            </h2>
            <div className='flex flex-col gap-10'>
              <div className='flex gap-5'>
                <div className='relative w-6 h-6 mt-2'>
                  <img src='/PNOY/callCareer.svg' alt='call icon' />
                </div>
                <div className='text-white font-inter text-sm leading-6'>
                  <p className='uppercase'>Call us at</p>
                  <p className='uppercase'>
                    +91 9563-26-9563, +91 9563-30-9563
                  </p>
                </div>
              </div>
              <div className='flex gap-5'>
                <div className='relative w-6 h-6 mt-2'>
                  <img
                    src='/PNOY/emailCareer.svg'
                    alt='email icon'
                    className='text-secondary'
                  />
                </div>
                <div className='text-white font-inter text-sm leading-6'>
                  <p className='uppercase'>Email us at</p>
                  <p className='uppercase'>sales@pnoy.in</p>
                </div>
              </div>
              <div className='flex gap-5'>
                <div className='relative w-6 h-6 mt-2'>
                  <img src='/PNOY/locationCareer.svg' alt='location icon' />
                </div>
                <div className='text-white font-inter text-sm leading-6'>
                  <p className='uppercase'>Address</p>
                  <div className='uppercase'>
                    <p>SCO 21/22, Basement 1, Near Genpact,</p>
                    <p>Khushal Nagar, Sector-69, Gurugram-122101,</p>
                    <p>Haryana(BHARAT)</p>
                  </div>
                </div>
              </div>
            </div>
            <Link
              className='flex bg-white lg:padding-x py-5 rounded-xl justify-center items-center gap-5'
              href='https://wa.me/919563269563'
              target='_blank'
              rel='noopener noreferrer'>
              <div className='relative w-8 h-8'>
                <img src='/PNOY/whatsapp-icon.svg' alt='whatsapp icon' />
              </div>
              <p className='uppercase text-whatsapp-green text-sm lg:whitespace-nowrap'>
                click to chat with us
              </p>
            </Link>
            <div className='w-0 h-0 border-t-[20px] lg:border-t-[30px] border-t-transparent border-b-[20px] lg:border-b-[30px] border-b-transparent border-l-[40px] lg:border-l-[60px] border-l-yellow absolute top-0 lg:right-0 right-5 lg:top-24 lg:translate-x-[3rem] -translate-y-[2rem] -rotate-90 lg:rotate-0'></div>
          </div>
          <div className='py-10 flex-1 lg:padding-l'>
            <h2 className='text-3xl self-center font-bold text-yellow uppercase mb-10'>
              Send us a message
            </h2>
            <ContactUsForm />
          </div>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default ReachUs;
