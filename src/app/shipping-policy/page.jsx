import { SecondaryHeader } from '@/utilities';
import LayoutWrapper from '@/utilities/LayoutWrapper';

const ShippingPolicy = () => {
  return (
    <LayoutWrapper>
      <SecondaryHeader>Shipping Policy</SecondaryHeader>
      <section className='w-full padding-x padding-y flex flex-col justify-between'>
        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Introduction
          </h2>
          <p>
            Welcome to PNOY Electronic India Pvt Ltd! This shipping policy
            outlines the terms and conditions governing the delivery of your
            purchases. Kindly read it carefully before finalizing your order.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Shipping Costs and Methods
          </h2>
          <p>
            We provide a range of shipping options to meet your requirements and
            budget. Shipping charges are determined by the weight, dimensions,
            and destination of your order. During checkout, you'll be presented
            with the available options along with their estimated costs.
          </p>
          <ul className='list-disc'>
            <li>
              <strong>Standard Shipping:</strong> Our most economical choice,
              usually delivered within 5-8 business days.
            </li>
            <li>
              <strong>Expedited Shipping:</strong> Ensures quicker delivery,
              typically within 3-5 business days.
            </li>
            <li>
              <strong>Free Shipping:</strong> Enjoy complimentary shipping for
              orders above 10,000.
            </li>
          </ul>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Order Processing Time
          </h2>
          <p>
            Orders are generally processed and dispatched within 2 business
            days. Please note that this timeframe is an approximation and may
            vary based on order volume and product availability.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Delivery Time
          </h2>
          <p>
            Delivery duration is based upon your chosen shipping method and
            location. Estimated delivery time will be displayed during checkout.
            Please note that these are approximations, and actual delivery time
            may vary by factors such as weather conditions and carrier delays.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Tracking Your Order
          </h2>
          <p>
            Upon shipment, you'll receive a confirmation email containing a
            tracking number. Utilize this number to monitor your package's
            progress via the carrier's website.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            International Shipping
          </h2>
          <p>
            We extend international shipping to selected countries. Please be
            aware that international shipping costs may exceed domestic rates,
            and delivery time could be prolonged. During checkout, you'll find
            available shipping options and estimated costs.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Shipping Restrictions
          </h2>
          <p>
            Certain products may have shipping restrictions due to weight,
            dimensions, or hazardous materials content. These limitations will
            be indicated on the product page.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Returns and Exchanges
          </h2>
          <p>
            For details regarding returns or exchanges, kindly refer to our
            separate returns and exchange policy page.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Contact Us
          </h2>
          <p>
            If you have any inquiries regarding our shipping policy, please
            reach out to us at <a href='mailto:sales@pnoy.in'>sales@pnoy.in</a>.
          </p>
        </div>

        <div className='bg-nav-link p-10 w-full flex flex-col justify-between rounded-2xl shadow-md font-inter mb-10'>
          <h2 className='text-xl font-bold text-yellow uppercase mb-10'>
            Updates to this Policy
          </h2>
          <p>
            Periodically, we may revise this shipping policy. Any modifications
            will be promptly posted on this page, so we encourage you to review
            it periodically.
          </p>
        </div>
      </section>
    </LayoutWrapper>
  );
};

export default ShippingPolicy;
