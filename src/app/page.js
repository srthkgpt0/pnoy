import Image from "next/image";
import {
  Banner,
  Benefits,
  ExperienceComponent,
  InfoComponent,
  OverviewComponent,
  Products,
  Testimonial,
} from "../components";
import Link from "next/link";
import BookYourDemo from "./products/[id]/BookYourDemo";

export default function Home() {
  return (
    <main>
      <section className="w-full relative max-md:mx-auto max-md:w-full">
        <Banner />
      </section>
      <section className="flex flex-wrap w-full items-center justify-center max-md:pt-4 pb-10 relative bg-nav-link rounded-b-2xl">
        <div className="flex items-center justify-center max-md:w-full max-md:mx-1 z-10">
          <InfoComponent />
        </div>
        <div className="px-5 py-5 lg:px-0 lg:py-0  text-center flex flex-col font-inter items-center gap-2">
          <p className="px-24 lg:px-0 font-bold text-3xl lg:text-2xl py-2 uppercase">
            PNOY Electronic India Private Limited
          </p>
          <p className="px-16 lg:px-0 text-lg pb-6 max-md:pb-3">
            PNOY Electronic India Pvt. Ltd. is the leading company providing
            products and accessories aimed at improving the quality of life.
          </p>
          <Link href='/about-us' className="transition-all duration-200 antialiased uppercase rounded-full border border-secondary text-sm px-4 py-1 text-secondary max-md:scale-110 z-10 hover:bg-secondary hover:text-white hover:border-black shadow-md">
            Read more
          </Link>
        </div>
        <Image src='/PNOY/texture.png' fill className='object-contain' />
      </section>
      <section className="padding-x padding-y">
        <Products />
      </section>
      <section className="padding-x">
        <ExperienceComponent />
      </section>
      <section className="padding-x padding-y">
        <OverviewComponent />
      </section>
      <section className="padding-x pb-5">
        <BookYourDemo />
      </section>
      <section className="w-full relative rounded-t-2xl overflow-hidden">
        <Benefits />
      </section>
      <section>
        <Testimonial />
      </section>
    </main >
  );
}
