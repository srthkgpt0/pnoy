'use client';
import React from 'react';
import {
  Accordion,
  AccordionHeader,
  AccordionBody
} from '@material-tailwind/react';

function Icon({ open }) {
  return open ? (
    <svg
      className={`w-5 h-5 text-secondary dark:text-white transition-opacity duration-300 font-semibold ${
        open ? 'opacity-100' : 'opacity-0'
      }`}
      aria-hidden='true'
      xmlns='http://www.w3.org/2000/svg'
      fill='none'
      viewBox='0 0 18 2'>
      <path
        stroke='currentColor'
        stroke-linecap='round'
        stroke-linejoin='round'
        stroke-width='2'
        d='M1 1h16'
      />
    </svg>
  ) : (
    <svg
      className={`w-5 h-5 text-secondary dark:text-white transition-opacity duration-300 font-semibold ${
        open ? 'opacity-0' : 'opacity-100'
      }`}
      aria-hidden='true'
      xmlns='http://www.w3.org/2000/svg'
      fill='none'
      viewBox='0 0 18 18'>
      <path
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
        d='M9 1v16M1 9h16'
      />
    </svg>
  );
}

const Header = ({ children }) => (
  <AccordionHeader className='bg-nav-link border-none px-10 rounded-2xl font-inter uppercase text-base'>
    {children}
  </AccordionHeader>
);
const Body = ({ children }) => (
  <AccordionBody className='px-8 bg-nav-link rounded-b-2xl'>
    {children}
  </AccordionBody>
);

function AccordionComponent({ isOpen = false, children }) {
  const [open, setOpen] = React.useState(isOpen);

  const handleOpen = () => setOpen((state) => !state);

  const childrenWithProps = React.Children.map(children, (child) => {
    // Wrap the child component and pass additional props
    return (
      <div
        onClick={handleOpen}
        className={`${
          open
            ? '[&>button]:transition-all [&>button]:duration-300 [&>button]:text-xl [&>button]:rounded-b-none '
            : '[&>button]:transition-all [&>button]:duration-300 [&>button]:text-base'
        }`}>
        {child}
      </div>
    );
  });
  return (
    <Accordion
      className='mb-5 font-inter'
      open={open}
      icon={<Icon open={open} />}>
      {childrenWithProps}
    </Accordion>
  );
}

AccordionComponent.Body = Body;
AccordionComponent.Header = Header;

export default AccordionComponent;
