import Image from 'next/image';

const MottoSingleInfo = ({ imgUrl, header, text }) => {
  return (
    <div className='flex flex-col items-center justify-between w-full lg:h-44 mb-10'>
      <div className='flex flex-col items-center justify-between w-full min-h-full gap-8'>
        <div className='w-full h-24 relative'>
          <Image src={imgUrl} alt='motto' fill className='object-contain' />
        </div>
        <p className='uppercase font-inter font-semibold text-2xl'>{header}</p>
        <div className='relative text-center w-full flex justify-center items-center'>
          <div className='border-secondary border-t-[1px] w-full'></div>
          <div className='w-5 h-5 absolute border-secondary border-[1px] p-3 rounded-full flex justify-center items-center bg-white'>
            <div className='bg-primary p-2 rounded-full'></div>
          </div>
        </div>
      </div>
      <p className='px-5 text-center font-inter text-xs mt-8'>{text}</p>
    </div>
  );
};

export default MottoSingleInfo;
