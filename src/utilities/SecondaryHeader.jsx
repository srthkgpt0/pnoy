const SecondaryHeader = ({ children }) => {
  return (
    <header className='bg-primary rounded-3xl flex justify-center py-6 w-full max-2xl:max-w-screen-xl mt-10'>
      <h1 className='text-3.5xl uppercase text-white font-goBold font-semibold text-center'>
        {children}
      </h1>
    </header>
  );
};

export default SecondaryHeader;
