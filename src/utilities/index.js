import AccordionComponent from "./Accordion";
import MottoSingleInfo from "./MottoSingleInfo";
import ProductDisplay from "./ProductDIsplay";
import SecondaryHeader from "./SecondaryHeader";


export const convertTitleCase = (str) => {
    const convertedText = str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
    return convertedText;
}

export { SecondaryHeader, MottoSingleInfo, ProductDisplay, AccordionComponent };