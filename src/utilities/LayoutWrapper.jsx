const LayoutWrapper = ({ children }) => {
  return (
    <main className='max-w-screen-wide padding-x 3xl:px-0 lg flex items-center flex-col mx-auto'>
      {children}
    </main>
  );
};

export default LayoutWrapper;
