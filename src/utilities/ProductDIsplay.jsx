'use client';
import { motion } from 'framer-motion';
import Image from 'next/image';
import { useRouter } from 'next/navigation';

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1
  }
};

const ProductDisplay = ({ product }) => {
  const router = useRouter();
  return (
    <motion.div
      className='bg-nav-link flex flex-col items-center pb-8 rounded-3xl min-h-[320px] max-w-[18rem] min-w-[285px] my-10 mx-2 justify-between'
      variants={item}>
      <div className='relative w-36 h-36 transform -translate-y-10'>
        <Image
          src={product.productPaths[0].path}
          alt={product.productCode}
          fill
          className='object-contain'
        />
      </div>
      <div className='flex flex-col items-center gap-2 whitespace-nowrap mb-auto'>
        <p className='font-aurinda uppercase text-base text-primary font-semibold'>
          {product.productName}
        </p>
        <p className='text-sm uppercase'>{product.productCode}</p>
        <p className='text-sm uppercase'>{product.productDescription}</p>
      </div>
      <button
        className='rounded-full bg-primary uppercase text-white px-5 py-2 text-xs justify-self-end'
        onClick={() => router.push(`/products/${product.productId}`)}>
        Know more
      </button>
    </motion.div>
  );
};

export default ProductDisplay;
