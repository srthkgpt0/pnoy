import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_BASE_URL, // Set your API base URL
    timeout: 5000, // Timeout in milliseconds
});

const api = {
    postFormData: (url, formData, config) => instance.post(url, formData, config),
    // Add other API calls as needed
};

export default api;