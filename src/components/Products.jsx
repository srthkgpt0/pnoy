'use client';
import { products } from '../constants';
import Media from 'react-media';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import './Products.css';
import { ProductDisplay } from '@/utilities';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 1500 },
    items: 4
  },
  desktop: {
    breakpoint: { max: 1500, min: 1000 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1000, min: 720 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 720, min: 0 },
    items: 1
  }
};
const Products = () => {
  const ButtonGroup = ({ next, previous, ...rest }) => {
    const {
      carouselState: { currentSlide, totalItems, slidesToShow }
    } = rest;
    const firstSlide = currentSlide === 0;
    const lastSlide = currentSlide === totalItems - slidesToShow;
    const activeCss = 'bg-secondary  rounded-full text-white';
    const inActiveCss = 'bg-carousal-white  rounded-full';
    return (
      <div className='absolute top-0 right-0'>
        <button
          className={`${firstSlide ? inActiveCss : activeCss} px-4 py-2 mr-5`}
          onClick={() => previous()}>
          &lt;
        </button>
        <button
          className={`${lastSlide ? inActiveCss : activeCss} px-4 py-2`}
          onClick={() => next()}>
          &gt;
        </button>
      </div>
    );
  };
  return (
    <div className='relative'>
      <div className='flex justify-between'>
        <div className='uppercase font-aurinda font-bold text-2xl'>
          Our Products
        </div>
      </div>
      <>
        <Media query={{ maxWidth: 720 }}>
          {(matches) =>
            matches ? (
              <div className='flex flex-col justify-center items-center pt-10'>
                {products.map((prod, key) => (
                  <ProductDisplay product={prod} key={key} />
                ))}
              </div>
            ) : (
              <div className='pt-10'>
                <Carousel
                  responsive={responsive}
                  ssr
                  infinite={false}
                  containerClass='carousel-container'
                  itemClass='carousel-item'
                  renderButtonGroupOutside
                  arrows={false}
                  customButtonGroup={<ButtonGroup />}>
                  {products.map((prod, key) => (
                    <ProductDisplay product={prod} key={key} />
                  ))}
                </Carousel>
              </div>
            )
          }
        </Media>
      </>
    </div>
  );
};

export default Products;
