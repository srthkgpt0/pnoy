import { features } from '@/constants';
import { FadeInWhenVisible } from '.';

const OverviewComponent = () => {
  return (
    <>
      <div className='flex flex-col items-center pb-10'>
        <p className='uppercase font-inter text-xl py-1'>
          PNOY's Alkaline Water Ionizers
        </p>
        <p className='font-aurinda uppercase text-3.5xl font-bold'>
          A Quick Overview
        </p>
        <p className='py-4'>
          <img src='/PNOY/underline.svg' alt='underline' />
        </p>
      </div>
      <div className='w-full flex flex-col items-center justify-center my-3'>
        <FadeInWhenVisible>
          <div className='flex flex-col lg:flex-row flex-nowrap justify-center items-center gap-4 w-full h-full lg:h-[150px] '>
            {features.map((feat, index) => (
              <div
                key={index}
                className='flex flex-col gap-4 justify-start items-center w-full lg:w-[25%] p-2 h-full py-6 lg:py-0'>
                <div className='flex items-center gap-4 w-full'>
                  <img src={feat.iconPath} alt={feat.heading} />
                  <p className='font-aurinda font-semibold uppercase text-lg lg:text-md'>
                    {feat.heading}
                  </p>
                </div>
                <div className='w-full'>
                  <p className='font-inter text-[13px] leading-5 flex-wrap text-base lg:text-sm'>
                    {feat.subHeading}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </FadeInWhenVisible>
      </div>
    </>
  );
};

export default OverviewComponent;
