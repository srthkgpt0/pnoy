import { MottoSingleInfo } from '@/utilities';

const MottoComponent = () => {
  return (
    <div className='flex flex-col lg:flex-row justify-center items-center max-2xl:max-w-screen-xl m-auto'>
      <MottoSingleInfo
        imgUrl='/PNOY/rocket.svg'
        header='OUR MISSION'
        text='Our mission is to provide 100% satisfaction to our customers in terms of quality and excellent customer service by building long term relationships with our customers.'
      />
      <MottoSingleInfo
        imgUrl='/PNOY/Eye-vision.svg'
        header='OUR Vision'
        text={`Our vision is to nurture India's population by improving their daily lifestyle with the advancement of utmost technologies in Alkaline Water ionizers which provide Microclustred, Antioxidant Hydrogen rich Alkaline Water for their wellbeing.`}
      />
      <MottoSingleInfo
        imgUrl='/PNOY/dart.svg'
        header='OUR Goal'
        text='Our goal at PNOY is to revolutionize drinking water with cutting-edge ionizers and hydrogenating machines, enhancing health and hydration. We strive to lead in water purification technology, ensuring access to revitalizing and pure water for all.'
      />
    </div>
  );
};

export default MottoComponent;
