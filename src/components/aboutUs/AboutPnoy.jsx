const AboutPnoy = () => {
  return (
    <div className='justify-between flex flex-col xl:flex-row mx-auto gap-5'>
      <div className='bg-yellow flex-1 w-full'>
        <img
          src='/PNOY/about-us-01.png'
          alt='about-us-image'
          className='translate-x-3 -translate-y-3 w-full'
        />
      </div>
      <div className='flex flex-col gap-8 flex-1'>
        <h2 className='uppercase font-inter font-semibold text-2xl'>
          PNOY Electronic India Private Limited
        </h2>
        <div className='text-xs flex flex-col gap-4 font-inter'>
          <p>
            "PNOY Electronic India Pvt. Ltd. is the leading company providing
            products and accessories aimed at improving the quality of life. It
            was incorporated under the Companies Act of 2013 as PNOY Electronic
            India Private Limited. PNOY's Alkaline Ionizer offers a range of
            essential health benefits with multiple pH value settings optimized
            to improve everyday activities such as cooking, cleaning, and
            meeting your drinking water needs. With the purpose of promoting
            good health for all, our company strives to be the best guardian of
            your family's well-being.
          </p>
          <p>
            The aim of PNOY Electronic India Pvt. Ltd. is to achieve Zero Water
            Wastage and create value for clients by maintaining high-quality
            standards using advanced technology. We assure you of our excellence
            in providing healthy drinking water solutions for your home. Our
            Ionizers are designed for residential and light commercial use. This
            company is built on years of experience and research in water
            technology. Our primary focus is on providing the best customer
            experience and world-class after-sales services. We believe that
            caring for the environment and caring for people go hand in hand.
            PNOY's corporate policy is "We plant 1 tree for every product sold."
            We take pride in introducing safe and pure drinking water solutions
            for people while contributing to the environment."
          </p>
        </div>
      </div>
    </div>
  );
};

export default AboutPnoy;
