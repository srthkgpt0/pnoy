'use client';
import { useAnimation, useInView, motion } from 'framer-motion';
import { useEffect, useRef } from 'react';

function FadeInWhenVisible({ children, className = '' }) {
  const controls = useAnimation();
  const ref = useRef(null);
  const isInView = useInView(ref);

  useEffect(() => {
    if (isInView) {
      controls.start('visible');
    }
  }, [controls, isInView]);

  return (
    <motion.div
      ref={ref}
      animate={controls}
      initial='hidden'
      transition={{ duration: 1.5 }}
      variants={{
        visible: { opacity: 1, translateY: 0 },
        hidden: { opacity: 0, translateY: 50 }
      }}
      className={className}>
      {children}
    </motion.div>
  );
}

export default FadeInWhenVisible;
