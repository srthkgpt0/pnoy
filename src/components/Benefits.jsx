import Image from 'next/image';

const Benefits = () => {
  return (
    <div className='relative'>
      <div className='lg:absolute block w-full text-center z-10'>
        <h3 className='font-aurinda text-3.5xl text-primary uppercase font-semibold'>
          Benefit of Pnoy's Ionized Water
        </h3>
      </div>
      <Image
        src='/PNOY/monotone-empty-room-authentic-interior-design.png'
        alt='banner-image'
        width='0'
        height='0'
        sizes='100vw'
        className='w-full h-auto relative z-0 object-cover object-center'
      />
    </div>
  );
};

export default Benefits;
