import Link from 'next/link';
import Image from 'next/image';
import NavList from './NavList';

function Navbar() {
  return (
    <nav className='flex h-20 padding-x items-center justify-between shadow-xl bg-card-bg'>
      <Link href='/'>
        <div className='relative w-16 h-16'>
          <Image
            src='/PNOY/Nav-logo.png'
            alt='pnoy-main-logo'
            fill
            className='object-contain'
          />
        </div>
      </Link>
      <NavList />
    </nav>
  );
}

export default Navbar;
