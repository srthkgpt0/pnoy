'use client';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { routesJson } from '@/constants/routes';
import React, { useMemo } from 'react';
import {
  Menu,
  MenuHandler,
  MenuItem,
  MenuList
} from '@material-tailwind/react';
import SideBar from './SideBar';

function MenuData({ data, pathname }) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  return (
    <Menu
      allowHover
      open={isMenuOpen}
      handler={setIsMenuOpen}
      placement='bottom-start'>
      <MenuHandler>
        <Link
          href={data.endPoint}
          className={`px-2 h-full gap-2 flex items-center transition delay-100 hover:bg-nav-link ease-in-out duration-300 text-primary ${
            data.active.includes(pathname)
              ? 'transition ease-in-out delay-100 duration-300 bg-nav-link underline underline-offset-8 decoration-2 decoration-primary'
              : ''
          }`}>
          {data.title}
        </Link>
      </MenuHandler>
      <MenuList className='bg-card-bg p-0 py-5 overflow-hidden'>
        {data.child.map((data, index) => {
          if (data.toBeVisibleInNavBar) {
            return (
              <Link href={data.endPoint} key={index}>
                <MenuItem
                  className={`active:text-primary active:bg-nav-link focus:bg-nav-link focus:text-primary uppercase hover:bg-nav-link text-primary hover:text-yellow
              ${
                data.active.includes(pathname) ? 'text-yellow bg-nav-link' : ''
              }`}>
                  {data.title}
                </MenuItem>
              </Link>
            );
          } else return null;
        })}
      </MenuList>
    </Menu>
  );
}

function MenuDataContactUs({ data, pathname }) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  return (
    <Menu
      allowHover
      open={isMenuOpen}
      handler={setIsMenuOpen}
      placement='bottom-start'>
      <MenuHandler>
        <Link href={data.endPoint}>{data.title}</Link>
      </MenuHandler>
      <MenuList className='bg-card-bg p-0 py-5 overflow-hidden'>
        {data.child.map((data, index) => {
          if (data.toBeVisibleInNavBar) {
            return (
              <Link href={data.endPoint} key={index}>
                <MenuItem
                  className={`active:text-primary active:bg-nav-link focus:bg-nav-link focus:text-primary uppercase hover:bg-nav-link text-primary hover:text-yellow
              ${
                data.active.includes(pathname) ? 'text-yellow bg-nav-link' : ''
              }`}>
                  {data.title}
                </MenuItem>
              </Link>
            );
          } else return null;
        })}
      </MenuList>
    </Menu>
  );
}
function NavList() {
  const findContactUs = useMemo(
    () => routesJson.find((route) => route.title === 'Contact Us'),
    []
  );
  const pathname = usePathname();
  return (
    <>
      <div className='hidden xl:block h-full'>
        <ul className='flex uppercase gap-6 items-center text-sm h-full'>
          {routesJson.map((data, index) => {
            if (data.toBeVisibleInNavBar) {
              if (data.child && data.child.length > 0) {
                return (
                  <li key={data.title} className='h-full'>
                    <MenuData data={data} pathname={pathname} />
                  </li>
                );
              } else {
                return (
                  <li key={data.title} className='h-full last:bg-secondary'>
                    <Link
                      key={index}
                      href={data.endPoint}
                      className={`px-2 h-full flex items-center transition delay-100 hover:bg-nav-link ease-in-out duration-300 text-primary focus-visible:!outline-none ${
                        data.active.includes(pathname)
                          ? 'transition ease-in-out delay-100 duration-300 bg-nav-link underline underline-offset-8 decoration-2 decoration-primary'
                          : ''
                      }`}>
                      {data.title}
                    </Link>
                  </li>
                );
              }
            }
          })}
          <div
            className={`uppercase bg-secondary rounded-full text-sm text-white py-2 px-4 ${
              findContactUs.active.includes(pathname) ? 'bg-yellow' : ''
            } `}>
            <MenuDataContactUs data={findContactUs} pathname={pathname} />
          </div>
        </ul>
      </div>
      <div className='relative xl:hidden'>
        <SideBar />
      </div>
    </>
  );
}

export default NavList;
