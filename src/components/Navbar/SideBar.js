'use client';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React, { useMemo } from "react";
import Image from 'next/image';
import {
    Drawer,
    Accordion,
    AccordionHeader,
    AccordionBody,
} from "@material-tailwind/react";
import { routesJson } from "@/constants/routes";

function Icon() {
    return null;
}
export function MenuAccordion({ data, pathname }) {
    const [open, setOpen] = React.useState(false);

    const handleToggle = () => setOpen((state) => !state);

    return (
        <li className='w-full'>
            <Accordion icon={<Icon open={open} />} open={open}>
                <AccordionHeader onClick={() => handleToggle()} className={`text-card-bg uppercase hover:text-yellow p-2 w-full text-center font-inter border-none font-normal text-base justify-center ml-2 ${data.active.includes(pathname) ? 'text-yellow' : ''}`}>{data.title}</AccordionHeader>
                <AccordionBody className='bg-nav-link flex flex-col w-full justify-center items-center uppercase shadow-inner px-4'>
                    {data.child.map(child => {
                        if (child.toBeVisibleInNavBar) {
                            return <Link href={child.endPoint} className={`hover:text-yellow leading-7 font-inter text-xs whitespace-nowrap ${child.active.includes(pathname) ? 'text-yellow' : ''}`}>
                                {child.title}
                            </Link>
                        }
                    })}
                </AccordionBody>
            </Accordion>
        </li>
    );
}
function SideBar() {
    const [open, setOpen] = React.useState(false);

    const openDrawer = () => setOpen(true);
    const closeDrawer = () => setOpen(false);
    const pathname = usePathname();
    const findContactUs = useMemo(
        () => routesJson.find((route) => route.title === 'Contact Us'),
        []
    );
    return (
        <React.Fragment>
            <div className='h-8 w-8' onClick={openDrawer}>
                <Image
                    src='/PNOY/hamburger-1.svg'
                    alt='hamburger'
                    fill
                    className='object-contain'
                />
            </div>
            <Drawer placement='right' open={open} onClose={closeDrawer} className="bg-primary flex flex-col items-center justify-around w-fit">
                <div className="text-white flex flex-col items-center">
                    <p className="uppercase font-inter font-bold text-4xl">PNOY</p>
                    <p className="uppercase text-xs">FEEL THE DIFFERENCE</p>
                </div>
                <ul className='flex flex-col items-center w-full'>
                    {routesJson.map((data, index) => {
                        if (data.toBeVisibleInNavBar) {
                            if (data.child && data.child.length > 0) {
                                return (
                                    <MenuAccordion data={data} pathname={pathname} />
                                );
                            } else {
                                return (
                                    <li key={data.title} className={`text-card-bg uppercase hover:text-yellow p-2 w-full text-center font-inter ${data.active.includes(pathname)
                                        ? 'text-yellow'
                                        : ''}`}>
                                        <Link
                                            key={index}
                                            href={data.endPoint}
                                        >
                                            {data.title}
                                        </Link>
                                    </li>
                                );
                            }
                        }
                    })}
                    <div className='w-full'>
                        <MenuAccordion data={findContactUs} pathname={pathname} />
                    </div>
                </ul>
                <div className='w-full bg-nav-link text-center flex justify-center p-2'>
                    <div className='relative w-16 h-16'>
                        <Image
                            src='/PNOY/Nav-logo.png'
                            alt='pnoy-main-logo'
                            fill
                            className='object-contain'
                        />
                    </div>
                </div>
            </Drawer>
        </React.Fragment >
    );
}

export default SideBar