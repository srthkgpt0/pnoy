import Image from 'next/image';
import Link from 'next/link';

const Banner = () => {
  return (
    <div className='relative'>
      <Image
        src='/PNOY/homepage_banner.jpg'
        alt='banner-image'
        width='0'
        height='0'
        sizes='100vw'
        className='w-full h-auto relative z-0 object-cover object-center max-md:h-40 max-lg:h-96'
      />
      <p className='absolute left-0 right-0 mx-auto text-center top-3 lg:top-8 uppercase text-primary-color font-goBold font-bold text-md lg:text-2xl xl:text-3xl text-primary max-md:text-sm max-md:top-2 max-md:w-[59%]'>
        Enhancing Your Quality of Life through our Products
      </p>
      <div className='absolute bottom-2 lg:bottom-12 right-0 left-0 leading-8 flex flex-col justify-center items-center'>
        <div className='mb-2'>
          <span className='uppercase text-white font-aurinda font-extrabold text-xs lg:text-lg xl:text-xl'>
            Natural and Healthy Drinking water
          </span>
        </div>
        <Link
          href='/products'
          className='uppercase bg-secondary rounded-full px-2 lg:px-4 py-1 lg:py-2 w-40 lg:w-60 text-text_xs lg:text-sm text-white'>
          Get the Product details
        </Link>
      </div>
    </div>
  );
};

export default Banner;
