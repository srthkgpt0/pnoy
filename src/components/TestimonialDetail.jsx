const TestimonialDetail = ({ testimonial }) => {
  const { imgSrc, content, userName, userOccupation } = testimonial;

  return (
    <div className='flex flex-col items-center'>
      <div
        className={`relative w-[250px] h-[250px] rounded-full overflow-hidden border-primary border-[3px]`}>
        <img
          src={imgSrc}
          alt={userName}
          className='object-cover w-full h-full'
        />
      </div>
      <div className='flex flex-col justify-center items-center pt-5 gap-5 w-3/4 2xl:w-1/2 max-sm:w-full'>
        <div className='flex flex-col items-center'>
          <p className='uppercase text-lg text-primary font-semibold'>
            {userName}
          </p>
          <p className='text-light-text first-letter:uppercase max-sm:text-xs max-sm:mt-2'>
            {userOccupation}
          </p>
        </div>
        <p className='font-inter text-sm text-center max-sm:leading-6'>
          {content}
        </p>
      </div>
    </div>
  );
};

export default TestimonialDetail;
