'use client';
import Image from 'next/image';
import Carousel from 'react-multi-carousel';
import { Certifications, TestimonialDetail } from '.';
import { testimonials } from '@/constants';

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 1
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

const activeCss = 'bg-secondary rounded-full text-white';
const inActiveCss = 'bg-carousal-white rounded-full';

const CustomLeftArrow = ({ onClick, ...rest }) => {
  const {
    onMove,
    carouselState: { currentSlide, deviceType }
  } = rest;
  const firstSlide = currentSlide === 0;
  return (
    <button
      className={`px-4 py-2 absolute top-1/2 left-0 ${
        firstSlide ? inActiveCss : activeCss
      }`}
      onClick={() => onClick()}>
      &lt;
    </button>
  );
};

const CustomRightArrow = ({ onClick, ...rest }) => {
  const {
    onMove,
    carouselState: { currentSlide, deviceType, totalItems }
  } = rest;
  const lastSlide = currentSlide === totalItems;
  return (
    <button
      className={`px-4 py-2 absolute top-1/2 right-0 ${
        lastSlide ? inActiveCss : activeCss
      }`}
      onClick={() => onClick()}>
      &gt;
    </button>
  );
};

const Testimonial = () => (
  <div className='relative h-full bg-nav-link'>
    <div className='padding-x padding-y z-50 relative'>
      <div className=''>
        <div className='px-10 flex flex-col items-center font-aurinda uppercase pb-10'>
          <h1 className='text-3.5xl font-bold max-sm:text-xl'>
            Our Testimonials
          </h1>
          <p className='max-sm:text-sm max-sm:mt-1.5'>
            Happy Clients & Feedbacks
          </p>
        </div>
        <div className='lg:px-10 max-sm:px-0 relative'>
          <Carousel
            ssr
            arrows
            autoPlay
            autoPlaySpeed={5000}
            customLeftArrow={<CustomLeftArrow />}
            customRightArrow={<CustomRightArrow />}
            pauseOnHover={false}
            infinite
            responsive={responsive}>
            {testimonials.map((testimonial, index) => (
              <TestimonialDetail key={index} testimonial={testimonial} />
            ))}
          </Carousel>
        </div>
      </div>
      <Certifications />
    </div>
    <img
      src='/PNOY/testimonial-texture.png'
      alt='texture_rounded_rec'
      className='absolute h-full w-full top-0 left-0 right-0 bottom-0 overflow-hidden z-10'
    />
  </div>
);

export default Testimonial;
