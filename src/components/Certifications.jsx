import { certificates } from '@/constants';
import Image from 'next/image';

const Certifications = () => {
  return (
    <div className='flex justify-center flex-grow'>
      <div className='rounded-full bg-yellow px-16 flex flex-col lg:flex-row justify-between items-center py-32 lg:py-6 gap-16 w-full'>
        <div className='lg:w-2/5 flex flex-col gap-2 py-2'>
          <h2 className='uppercase text-primary text-2xl lg:text-xl font-aurinda font-semibold'>
            certifications
          </h2>
          <p className='text-white text-md lg:text-xs'>
            PNOY is certified medical device company with ISO 13485 ISO 9001,
            ISO 14001, ISO 45001, and CE standards.
          </p>
        </div>
        <div className='flex flex-wrap gap-16 lg:gap-5 justify-center'>
          {certificates.map((cert) => (
            <div
              key={cert}
              className='relative h-[100px] w-[100px] lg:h-[75px] lg:w-[75px]'>
              <Image
                src={cert}
                alt='certs'
                fill
                className='relative object-cover'
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Certifications;
