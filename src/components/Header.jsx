import Image from 'next/image';
import Navbar from './Navbar/Navbar';
import Link from 'next/link';

function Header() {
  return (
    <header>
      <section>
        <div className='bg-yellow p-0 lg:p-2'>
          <div className='hidden relative lg:flex overflow-x-hidden'>
            <div className='animate-marquee whitespace-nowrap flex'>
              {[1, 2, 3, 4].map((_, index) => (
                <div className='flex items-center gap-4 mr-8' key={index}>
                  <div className='h-[50px] w-[50px]'>
                    <img src='/PNOY/fda_approved.png' alt='fda_approved' />
                  </div>
                  <p className='text-primary font-bold text-xl'>
                    UK FDA Approved Certified Medical Devices
                  </p>
                </div>
              ))}
            </div>

            <div className='absolute top-0 animate-marquee2 whitespace-nowrap flex'>
              {[1, 2, 3, 4].map((_, index) => (
                <div className='flex items-center gap-4 mr-8' key={index}>
                  <div className='h-[50px] w-[50px]'>
                    <img src='/PNOY/fda_approved.png' alt='fda_approved' />
                  </div>
                  <p className='text-primary font-bold text-xl'>
                    UK FDA Approved Certified Medical Devices
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
      <section className='hidden bg-primary py-2 md:flex font-inter justify-between padding-x text-white max-lg:hidden shadow-lg'>
        <div className='flex gap-7 items-center'>
          <Link
            href='tel:+919563269563'
            target='_blank'
            rel='noopener noreferrer'
            className='flex gap-1'>
            <Image
              src='/PNOY/callIcon.svg'
              alt='call-icon'
              width={15}
              height={15}
            />
            <p>+91 9563-26-9563</p>
          </Link>
          <Link
            href='mailto:sales@pnoy.in'
            target='_blank'
            rel='noopener noreferrer'
            className='flex gap-2'>
            <Image
              src='/PNOY/Email.svg'
              alt='email-icon'
              width={24}
              height={24}
            />
            <p>sales@pnoy.in</p>
          </Link>
        </div>
        <div className='flex items-center'>
          <div className='flex items-center'>
            {/* <p className='font-semibold mx-5'>
              Choose healthy today, Download the app now
            </p> */}
            <Link
              href='https://play.google.com/store/apps/details?id=com.ionizer.pnoy&hl=en&gl=US'
              target='_blank'
              rel='noreferrer noopener'>
              <img src='/PNOY/playstore.svg' />
            </Link>
          </div>
        </div>
      </section>
      <section>
        <Navbar />
      </section>
    </header>
  );
}

export default Header;
