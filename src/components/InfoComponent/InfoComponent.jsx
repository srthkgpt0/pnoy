import Image from 'next/image';
import { infoDetails } from '@/constants';
import { FadeInWhenVisible } from '..';

const InfoComponent = () => {
  return (
    <div
      className='flex flex-col lg:flex-row justify-center items-center gap-2
                    bg-yellow
                    flex-nowrap 
                    py-6 px-6
                    bg-primary-color
                    rounded-3xl
                    shadow-lg
                    '>
      {infoDetails.map((info) => (
        <FadeInWhenVisible key={info.title}>
          <section
            className='pt-1 pr-2 pb-1 pl-1 last:border-none ml-2 w-[400px] lg:w-full
                    '>
            <div className='h-12 w-12'>
              <Image
                src={info.iconPath}
                alt='Heart'
                width={72}
                height={60}
                style={{
                  height: '100%',
                  width: '100%'
                }}
              />
            </div>
            <div className='text-white mt-4'>
              <p className='text-base uppercase font-aurinda font-bold whitespace-nowrap max-md:text-m'>
                {info.title}
              </p>
              <p className='text-xs flex-wrap mt-2 2xl:text-sm'>{info.info}</p>
            </div>
          </section>
        </FadeInWhenVisible>
      ))}
    </div>
  );
};

export default InfoComponent;
