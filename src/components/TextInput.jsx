const TextInput = ({ id, label, type, placeholder, onChange, items }) => {
  return (
    <>
      <label htmlFor={id} className='block text-primary text-xs font-semibold'>
        {label}
      </label>
      <input
        type={type}
        id={id}
        name={id}
        onChange={onChange}
        value={items?.[id]}
        placeholder={placeholder}
        className='px-2 py-3 rounded-md border-gray-300 placeholder:text-gray-400 placeholder:text-sm w-full'
      />
    </>
  );
};

export default TextInput;
