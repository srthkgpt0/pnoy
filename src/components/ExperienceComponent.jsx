import { Counter } from '.';

const ExperienceComponent = () => {
  return (
    <div className='relative bg-primary flex justify-center gap-6 lg:gap-10 py-10 rounded-full px-10 lg:px-0'>
      <div className='flex flex-col items-center'>
        <Counter className='text-yellow text-3.5xl' from={0} to={7} />
        <p className='text-white uppercase font-aurinda font-semibold text-center'>
          Years of experience
        </p>
      </div>
      <div className='border'></div>
      <div className='flex flex-col items-center'>
        <Counter className='text-yellow text-3.5xl' from={20000} to={25743} />
        <p className='text-white uppercase font-aurinda font-semibold text-center'>
          Happy customers
        </p>
      </div>
      <div className='absolute h-full w-full top-0 left-0 right-0 bottom-0 overflow-hidden'>
        <img src='/PNOY/texture_rounded_rec.png' alt='texture_rounded_rec' />
      </div>
    </div>
  );
};

export default ExperienceComponent;
