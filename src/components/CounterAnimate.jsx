'use client';
import { animate } from 'framer-motion';
import { useEffect, useState } from 'react';

function Counter({ from, to, className }) {
  const [numb, setNumb] = useState(from);

  useEffect(() => {
    const controls = animate(from, to, {
      duration: 2,
      onUpdate(value) {
        setNumb(value.toFixed());
      }
    });
    return () => controls.stop();
  }, [from, to]);

  return <p className={className}>{numb}+</p>;
}

export default Counter;
