'use client';
import {
  Accordion,
  AccordionBody,
  AccordionHeader
} from '@material-tailwind/react';
import { useState } from 'react';

const TabComponent = () => {
  const [platinumSelected, setPlatinumSelected] = useState(true);
  const handleToggle = () => {
    setPlatinumSelected((state) => !state);
  };
  return (
    <div>
      <Accordion
        className='bg-primary rounded-3xl py-5 px-10 gap-5'
        open={!platinumSelected}>
        <AccordionHeader
          className='border-none flex justify-start text-white hover:text-white uppercase font-semibold text-xl font-inter '
          onClick={handleToggle}>
          <div className='w-8 flex justify-center items-center'>
            {platinumSelected ? <p>+</p> : <p>-</p>}
          </div>
          <p>Why Titanium?</p>
        </AccordionHeader>
        <AccordionBody className='text-sm justify-center items-center text-white font-inter'>
          The reason to use Titanium is its utmost safety, strength, hardness,
          and ability to resist erosion. The use of titanium enables a strong,
          robust, erosion-resistant plate with the least mass possible, i.e.,
          thin but very strong. Because of these reasons, titanium is also used
          in bone and joint replacements. Titanium is also used in space
          shuttles. No other metal is better than titanium for use in water
          Ionizer plate construction. Also, one should not select an Ionizer
          that uses a titanium alloy.
        </AccordionBody>
      </Accordion>
      <Accordion
        open={platinumSelected}
        className='bg-yellow rounded-3xl py-5 px-10 gap-5'>
        <AccordionHeader
          className='border-none text-white flex justify-start hover:text-white uppercase font-semibold text-xl font-inter'
          onClick={handleToggle}>
          <div className='w-8 flex justify-center items-center'>
            {!platinumSelected ? <p>+</p> : <p>-</p>}
          </div>
          <p>Why Platinum?</p>
        </AccordionHeader>
        <AccordionBody className='text-sm justify-center items-center text-white font-inter'>
          The reason to use platinum is that every other metal, including gold,
          has been examined for ionizing water efficiency. Platinum exceeds all
          other metals in its ability to catalyze electrolysis. Catalyzing the
          electrolysis in water Ionizers is absolutely essential. Water Ionizers
          "instantly" deliver ionized water to you. There are old-style water
          Ionizers that "brew" The water for about 20 minutes, and without
          platinum help even these don't work well. For the best functionality
          to be carried out in the water ionization process, the water molecule
          needs to be in contact with the platinum. These plates are passed
          through electricity which creates poles.These poles divide the water
          into streams. One is positively charged and the other is negatively
          charged. The water molecules are broken down into ions during the
          water ionization process. Ions are positively or negatively charged
          particles. OH-ions, Which are very basic and beneficial antioxidants,
          are stored within the alkaline stream of water. In the acidic stream,
          H+ ions are concentrated.
        </AccordionBody>
      </Accordion>
    </div>
  );
};

export default TabComponent;
