import { products, quickLinks } from '@/constants';
import { convertTitleCase } from '@/utilities';
import Image from 'next/image';
import Link from 'next/link';
import { FadeInWhenVisible } from '.';

const Footer = () => {
  return (
    <div className='bg-primary py-8 padding-x text-white max-md:py-6 max-md:px-4'>
      <div className='flex flex-col lg:flex-row justify-between'>
        <FadeInWhenVisible className='flex flex-col gap-5 lg:w-1/4 lg:mr-10 max-sm:w-full max-sm:items-start max-sm:p1 max-sm:gap-4 max-sm:px-4'>
          <div className='relative w-64 h-24'>
            <Image
              src='/PNOY/footer-logo.png'
              alt='Pnoy-footer-logo'
              fill
              className='object-cover'
            />
          </div>
          <p className='text-base leading-5 lg:text-sm font-inter'>
            The aim of PNOY Electronic India Pvt. Ltd. is zero water wastage,
            and it targets to create value for its clients by maintaining
            high-quality standards using advanced technology. We promise to
            excel in providing healthy drinking water solutions to your home and
            beloved family.
          </p>
        </FadeInWhenVisible>
        <div className='flex flex-col lg:flex-row lg:gap-0 py-5 lg:py-0 justify-between lg:divide-x-[1px] divide-yellow font-inter'>
          <FadeInWhenVisible className='flex flex-col gap-5 py-5 lg:py-0 items-start flex-1 lg:px-10 whitespace-nowrap max-sm:px-4'>
            <h4 className='uppercase text-xl lg:text-lg'>Quick Links</h4>
            <div className='flex flex-col text-base lg:text-sm leading-7'>
              {quickLinks.map((links) => (
                <Link
                  href={links.linkPath}
                  className='transition-all hover:text-yellow duration-200'>
                  {convertTitleCase(links.linkText)}
                </Link>
              ))}
            </div>
          </FadeInWhenVisible>
          {/* horizontal separator div only for small screen*/}
          <div className='hidden max-sm:block'>
            <div className='h-[1px] bg-yellow w-1/2 my-4 ml-16'></div>
          </div>
          <FadeInWhenVisible className='lg:px-10 py-5 lg:py-0  flex-1 flex flex-col gap-5 whitespace-nowrap max-sm:px-4'>
            <h4 className='uppercase text-xl lg:text-lg'>Products</h4>
            <div className='flex flex-col text-base lg:text-sm leading-7'>
              {products.map((prod) => (
                <Link
                  href={`/products/${prod.productId}`}
                  className='transition-all hover:text-yellow duration-200'>
                  {convertTitleCase(prod.productName)}
                </Link>
              ))}
            </div>
          </FadeInWhenVisible>
          {/* horizontal separator div only for small screen*/}
          <div className='hidden max-sm:block'>
            <div className='h-[1px] bg-yellow w-1/2 my-4 ml-16'></div>
          </div>
          <FadeInWhenVisible className='lg:px-10 py-5 lg:py-0 flex flex-col gap-5 flex-grow max-sm:px-4'>
            <h4 className='uppercase text-xl lg:text-lg'>Contact us</h4>
            <div className='flex flex-col text-base lg:text-sm leading-7 gap-5'>
              <div className='flex gap-3'>
                <div className='relative w-5 h-5'>
                  <Image
                    src='/PNOY/Location Pin.svg'
                    fill
                    alt='Location'
                    className='object-contain'
                  />
                </div>
                <p className='flex flex-wrap'>
                  SCO 21/22, Basement 1, Near Genpact, Khushal Nagar,
                  Sector-69,Gurugram- 122101, Haryana (BHARAT)
                </p>
              </div>
              <div className='flex gap-3'>
                <div className='relative w-4 h-4 max-sm:w-5 max-sm:h-5'>
                  <Image src='/PNOY/callIcon.svg' alt='callIcon' fill />
                </div>
                <p>+91 9563269563</p>
              </div>
              <div className='flex gap-3'>
                <div className='relative w-4 h-4 max-sm:w-5 max-sm:h-5'>
                  <Image src='/PNOY/Email.svg' alt='Email' fill />
                </div>
                <p>sales@pnoy.in</p>
              </div>
              <div className='flex gap-4'>
                <Link
                  className='relative w-10 h-10'
                  href='https://www.facebook.com/PNOY-INDIA-103946561344825'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <Image src='/PNOY/Facebook.svg' alt='Facebook' fill />
                </Link>
                <Link
                  className='relative w-10 h-10'
                  href='https://www.instagram.com/pnoy.india/'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <Image src='/PNOY/Instagram.svg' alt='Instagram' fill />
                </Link>
                <Link
                  className='relative w-10 h-10'
                  href='https://www.linkedin.com/company/pnoyindia'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <Image src='/PNOY/Linkedin.svg' alt='Linkedin' fill />
                </Link>
                <Link
                  className='relative w-10 h-10'
                  href='https://youtube.com/channel/UCnVBywov4NqjuVDdUlPLc1A'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <Image src='/PNOY/Youtube.svg' alt='Youtube' fill />
                </Link>
                <Link
                  className='relative w-10 h-10'
                  href='https://twitter.com/PnoyIndia'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <Image src='/PNOY/twitter-logo.svg' alt='Twitter' fill />
                </Link>
              </div>
            </div>
          </FadeInWhenVisible>
        </div>
      </div>
    </div>
  );
};

export default Footer;
