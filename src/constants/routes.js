export const routesJson = [
    {
        toBeVisibleInNavBar: true,
        endPoint: '/',
        active: ['/'],
        title: 'Home',
        child: []
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '#',
        active: ['/about-us', '/our-presence'],
        title: 'About Us',
        child: [
            {
                endPoint: '/about-us',
                active: ['/about-us'],
                title: 'About Company',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/our-presence',
                active: ['/our-presence'],
                title: 'Our Presence',
                toBeVisibleInNavBar: true
            }
        ]
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '/technology',
        title: 'TECHNOLOGY',
        active: ['/technology'],
        child: []
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '/products',
        title: 'PRODUCTS',
        active: [
            '/products',
            '/products/11-plate',
            '/products/7-plate',
            '/products/5-plate',
            '/products/h2-water-generator',
            '/products/alkaline-water-bottle',
            '/products/6-stage-water-purifier'
        ],
        child: [
            {
                endPoint: '/products/11-plate',
                active: ['/products/11-plate'],
                title: 'Ionized Max',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/7-plate',
                active: ['/products/7-plate'],
                title: 'Ionized Pro',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/5-plate',
                active: ['/products/5-plate'],
                title: 'Ionized Classic',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/h2-water-generator',
                active: ['/products/h2-water-generator'],
                title: 'HydroCure',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/alkaline-water-bottle',
                active: ['/products/alkaline-water-bottle'],
                title: 'Mineral Up',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/6-stage-water-purifier',
                active: ['/products/6-stage-water-purifier'],
                title: 'AlkaPure6',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/products/bamboo-toothbrush',
                active: ['/products/bamboo-toothbrush'],
                title: 'Bamboo Brush',
                toBeVisibleInNavBar: true
            }
        ]
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '#',
        active: [
            '/knowledge-bank/why-alkaline-water',
            '/knowledge-bank/benefits-of-ionized-water'
        ],
        title: 'KNOWLEDGE BANK',
        child: [
            {
                endPoint: '/knowledge-bank/why-alkaline-water',
                active: ['/knowledge-bank/why-alkaline-water'],
                title: 'Water and our bodies',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/knowledge-bank/ph-level',
                active: ['/knowledge-bank/ph-level'],
                title: 'Uses of different ph waters',
                toBeVisibleInNavBar: false
            },
            {
                endPoint: '/knowledge-bank/body-parts',
                active: ['/knowledge-bank/body-parts'],
                title: 'Effects of ionized water on body organs',
                toBeVisibleInNavBar: false
            },
            {
                endPoint: '/knowledge-bank/benefits-of-ionized-water',
                active: ['/knowledge-bank/benefits-of-ionized-water'],
                title: 'Benefits of ionized water',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/knowledge-bank/benefits-of-hydrogen-rich-water',
                active: ['/knowledge-bank/benefits-of-hydrogen-rich-water'],
                title: 'benefits of hydrogen rich antioxidant water',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/knowledge-bank/benefits-of-alkaline-water',
                active: ['/knowledge-bank/benefits-of-alkaline-water'],
                title: 'benefits of alkaline water',
                toBeVisibleInNavBar: true
            }
        ],
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '/faq',
        title: 'FAQ',
        child: [],
        active: ['/faq']
    },
    {
        toBeVisibleInNavBar: true,
        endPoint: '#',
        title: 'RESEARCH',
        active: ['/research/ionizedWaterBooks', '/research/medicalResearch'],
        child: [
            {
                endPoint: '/research/medicalResearch',
                active: ['/research/medicalResearch'],
                title: 'Medical Research',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/research/ionizedWaterBooks',
                active: ['/research/ionizedWaterBooks'],
                title: 'Books on Ionized water',
                toBeVisibleInNavBar: true
            }
        ]
    },
    {
        toBeVisibleInNavBar: false,
        endPoint: '#',
        title: 'Contact Us',
        active: [
            '/contact-us/careers/business-development-manager',
            '/contact-us/careers/tele-caller',
            '/contact-us/become-our-distributor',
            '/contact-us',
            '/contact-us/careers'
        ],
        child: [
            {
                endPoint: '/contact-us/careers',
                active: ['/contact-us/careers'],
                title: 'Careers',
                toBeVisibleInNavBar: false
            },
            // {
            //     endPoint: '/contact-us/careers/business-development-manager',
            //     active: ['/contact-us/careers/business-development-manager'],
            //     title: 'Business Development Manager'
            // },
            // {
            //     endPoint: '/contact-us/careers/tele-caller',
            //     active: ['/contact-us/careers/tele-caller'],
            //     title: 'Tele Caller'
            // },
            {
                endPoint: '/contact-us/become-our-distributor',
                active: ['/contact-us/become-our-distributor'],
                title: 'Become our distributor',
                toBeVisibleInNavBar: true
            },
            {
                endPoint: '/contact-us',
                active: ['/contact-us'],
                title: 'Reach us',
                toBeVisibleInNavBar: true
            }
        ]
    }
];
