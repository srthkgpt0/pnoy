export const infoDetails = [
    {
        iconPath: '/PNOY/Health Based Products.svg',
        title: 'Health Based Products',
        info: 'Our product range helps you achieve better health and boost your immunity.'
    },
    {
        iconPath: '/PNOY/Saving-Enviroment.svg',
        title: 'Saving environment',
        info: 'Our products are designed and developed, always keeping the environment and nature in consideration.'
    },
    {
        iconPath: '/PNOY/People At Center.svg',
        title: 'people at center',
        info: 'Our products are cost-effective so that the maximum number of people can benefit from our devices.'
    },
    {
        iconPath: '/PNOY/Advanced Technology.svg',
        title: 'advanced technology',
        info: 'We are in constant pursuit of upgrading our technology in order to provide technologically advanced products to our clients.'
    }
];

export const products = [
    {
        productPaths: [{ path: '/PNOY/Images/gold_model/11 plate 1.png', type: 'image' }, { path: '/PNOY/Images/gold_model/2.png', type: 'image' }, { path: '/PNOY/Images/gold_model/3.png', type: 'image' }, { path: '/PNOY/Images/gold_model/v.mp4', type: 'video' }],
        productId: '11-plate',
        productName: 'Ionized Max',
        productCode: 'saks89',
        productDescription: '11 plate ionizer',
        productDetails: [
            '11 Platinum-coated titanium plates.',
            'Platinum is coated using triple-layer dip technology.',
            'pH range between 3 - 11.5.',
            'ORP up to -850.',
            'Medically certified device.',
            'Electrodes Auto cleaning feature.',
            'Microcomputer model to efficiently control the electrolysis process.',
            'Zero water wastage.',
            'Dynamic colorful LCD display and operating panel with CDC touch technology.',
            'Displays flow rate, device temperature, pH, and ORP values.',
            'Easy one-touch operation.',
            'Product dimensions: 12cm * 25cm * 33cm.'
        ], isAlkalinePurifier: true
    },
    {
        productPaths: [{ path: '/PNOY/Images/silver_model/7 plate 1.png', type: 'image' }, { path: '/PNOY/Images/silver_model/2.png', type: 'image' }, { path: '/PNOY/Images/silver_model/3.png', type: 'image' }, { path: '/PNOY/Images/silver_model/4.png', type: 'image' }, { path: '/PNOY/Images/silver_model/v.mp4', type: 'video' }],
        productName: 'Ionized Pro',
        productId: '7-plate',
        productCode: 'saks85',
        productDescription: '7 plate ionizer',
        productDetails: [
            "7 Platinum-coated titanium plates.",
            "Platinum is coated using triple-layer dip technology.",
            "pH range between 3.5 - 11.0.",
            "ORP up to -750.",
            "Medically certified device.",
            "Electrodes Auto cleaning feature.",
            "Microcomputer model to efficiently control the electrolysis process.",
            "Zero water wastage.",
            "Dynamic colorful LCD display and operating panel with CDC touch technology.",
            "Displays flow rate, device temperature, pH, and ORP values.",
            "Easy one-touch operation.",
            "Product dimensions: 12cm * 25cm * 33cm."
        ], isAlkalinePurifier: true
    },
    {
        productPaths: [{ path: '/PNOY/Images/bronze_model/5 plate 1.png', type: 'image' }, { path: '/PNOY/Images/bronze_model/2.png', type: 'image' }, { path: '/PNOY/Images/bronze_model/3.png', type: 'image' }, { path: '/PNOY/Images/bronze_model/4.png', type: 'image' }, { path: '/PNOY/Images/bronze_model/v.mp4', type: 'video' }],
        productName: 'Ionized Classic',
        productId: '5-plate',
        productCode: 'saks83',
        productDescription: '5 plate ionizer',
        productDetails: [
            "5 Platinum-coated titanium plates.",
            "Platinum is coated using triple-layer dip technology.",
            "pH range between 4 and 10.50.",
            "ORP up to -650.",
            "Medically certified device.",
            "Electrodes Auto cleaning feature.",
            "Microcomputer model to efficiently control the electrolysis process.",
            "Zero water wastage.",
            "Dynamic colorful LCD display and operating panel with CDC touch technology.",
            "Displays flow rate, device temperature, pH, and ORP values.",
            "Easy one-touch operation.",
            "Product dimensions: 12cm * 25cm * 33cm."
        ], isAlkalinePurifier: true
    },
    {
        productPaths: [{ path: '/PNOY/Images/h2_water_generator/2.png', type: 'image' }, { path: '/PNOY/Images/h2_water_generator/1.png', type: 'image' }, { path: '/PNOY/Images/h2_water_generator/3.png', type: 'image' }, { path: '/PNOY/Images/h2_water_generator/4.png', type: 'image' }, { path: '/PNOY/Images/h2_water_generator/v.mp4', type: 'video' }],
        productName: 'HydroCure',
        productId: 'h2-water-generator',
        productDetails: [
            "Generates hydrogenated and antioxidant water.",
            "The molecular hydrogen concentration is approximately 1600 PPB.",
            "ORP (antioxidant properties) up to -650.",
            "Lithium-ion rechargeable battery lasts for 4-5 hours once fully charged.",
            "Accessories include an attachment that can attach to any packaged water bottle for the convenience of carrying it along.",
            "Accessories include an inhalation attachment that enables you to inhale molecular hydrogen for many health benefits.",
            "Ideal for people who are constantly on the go or for people who have limited mobility due to diseased conditions.",
            "A necessary accessory for the gym or fitness enthusiast.",
            "Comes with a charging cable and adapter.",
            "The functioning of the device can be seen through a transparent bottle.",
            "Product life is more than 10 years.",
            "Capacity: 400 mL.",
            "Warranty: 6 months."
        ], isAlkalinePurifier: false
    },
    {
        productPaths: [{ path: '/PNOY/Images/alkaline_water_bottle/1.png', type: 'image' }, { path: '/PNOY/Images/alkaline_water_bottle/2.png', type: 'image' }, { path: '/PNOY/Images/alkaline_water_bottle/3.png', type: 'image' }, { path: '/PNOY/Images/alkaline_water_bottle/4.png', type: 'image' }, { path: '/PNOY/Images/alkaline_water_bottle/v.mp4', type: 'video' }],
        productName: 'Mineral Up',
        productId: 'alkaline-water-bottle',
        productCode: 'saks49',
        productDescription: 'Alkaline Water Bottle',
        productDetails: [
            'Material - Food grade 304 SS',
            'Packaging - Premium card board',
            'Colour - Black',
            'Capacity - 300 ml',
            'Cartridge Life - 800 L / 1 Year',
            'pH range - pH 8 to pH 9',
            'Product dimension - H 20 cm',
        ],
        productBenefits: [
            'Various unique minerals such as tourmaline, mainfen, far infrared stone, nano silver powder, etc., are utilised for optimal mineralization.',
            'The use of removable alkaline cartridges guarantees effortless cleaning, eliminating any risk of contamination.',
            'The placement of the alkaline cartridge in the centre of the bottle prevents the water from attaining a pH higher than necessary.',
            'A durable food grade stainless-steel construction with an elegant design ensures extended, high-performance use.'
        ],
        productUsage: [
            'Allow the filled drinking water to sit for 10 minutes to achieve a pH level of 8 or higher, ideal for daily consumption.',
            'To experience optimal benefits, aim to consume a minimum of 3 litres of alkaline water daily.',
            'Avoid leaving the bottle filled for an extended duration.',
            'Consume the water within a window of 15 to 20 minutes after filling it.',
            'Regularly rinse the removable alkaline cartridges under tap water pressure for cleaning.',
            'Do not fill hot or cold water.'
        ], isAlkalinePurifier: false
    },
    {
        productPaths: [{ path: '/PNOY/Images/six_stage_alkaline/1.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/6.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/2.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/3.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/4.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/5.png', type: 'image' }, { path: '/PNOY/Images/six_stage_alkaline/v.mp4', type: 'video' }],
        productName: 'AlkaPure6',
        productId: '6-stage-water-purifier',
        productCode: 'saks65',
        productDescription: '6 stage Water Purifier',
        productDetails: [
            "Produces alkaline water suitable for daily drinking.",
            "Zero water wastage.",
            "No requirement of electricity.",
            "Accessory includes faucet for water dispensing.",
            "Product dimensions: 9*49*35 cm."
        ], isAlkalinePurifier: false
    },
    {
        productPaths: [{ path: '/PNOY/Images/bamboo_brushes/1.png', type: 'image' }, { path: '/PNOY/Images/bamboo_brushes/2.png', type: 'image' }, { path: '/PNOY/Images/bamboo_brushes/3.png', type: 'image' }, { path: '/PNOY/Images/bamboo_brushes/4.png', type: 'image' }, { path: '/PNOY/Images/bamboo_brushes/5.png', type: 'image' }],
        productName: 'Bamboo Brush',
        productId: 'bamboo-toothbrush',
        productDetails: [
            "Pack -1 (Set of 5 toothbrushes)",
            "Keeps gums healthy",
            "Removes tooth stains",
            "Whitens teeth",
            "BPA free bristles",
            "Charcoal coated bristles",
            "100% organic Indian grown bamboo toothbrush",
            "Completely biodegrades in 3 months time after disposal"
        ], isAlkalinePurifier: false
    }
];

export const features = [
    {
        iconPath: '/PNOY/EasyTooperate.svg',
        heading: 'Easy to Operate',
        subHeading:
            'Even with a compact and technically enhanced design, our Ionizers are very simple to operate, clean, and use.'
    },
    {
        iconPath: '/PNOY/SmartDesign.svg',
        heading: 'Smart Design',
        subHeading:
            'Our Ionizers are smartly designed & tested by team of expert engineers, enhancing the efficiency of the Ionizers.'
    },
    {
        iconPath: '/PNOY/LongLife.svg',
        heading: 'Long Life',
        subHeading:
            'Our Ionizers are technologically and qualitatively advanced, thus they have a long life, backed up by our warranty and world-class after-sales services.'
    },
    {
        iconPath: '/PNOY/QuickService.svg',
        heading: 'Quick Service',
        subHeading:
            'Our trained team of service engineers is just a call away, and our growing network of DSAs will be right there in your own city for accurate after-sales services.'
    }
];

export const certificates = [
    '/PNOY/iso-certificate1.png',
    '/PNOY/iso-certificate2.png',
    '/PNOY/iso-certificate3.png',
    '/PNOY/iso-certificate4.png',
    '/PNOY/CEcertified.png',
    '/PNOY/fda_approved.png'
];

export const quickLinks = [
    { linkText: 'About Us', linkPath: '/about-us' },
    { linkText: 'FAQ', linkPath: '/faq' },
    { linkText: 'Technology', linkPath: '/technology' },
    { linkText: 'Disclaimer', linkPath: '/disclaimer' },
    { linkText: 'Refund Policy', linkPath: '/refund-policy' },
    { linkText: 'Privacy Policy', linkPath: '/privacy-policy' },
    { linkText: 'Terms & Conditions', linkPath: '/terms-and-conditions' },
    { linkText: 'Medical Disclaimer', linkPath: '/medical-disclaimer' },
    { linkText: 'Return Policy', linkPath: '/return-policy' },
    { linkText: 'Shipping Policy', linkPath: '/shipping-policy' }
];

export const productDetails = [
    '11 Platinum-coated titanium plates.',
    'Platinum is coated using triple-layer dip technology.',
    'pH range between 3 - 11.5.',
    'ORP up to -850.',
    'Medically certified device.',
    'Electrodes Auto cleaning feature.',
    'Microcomputer model to efficiently control the electrolysis process.',
    'Zero water wastage.',
    'Dynamic colorful LCD display and operating panel with CDC touch technology.',
    'Displays flow rate, device temperature, pH, and ORP values.',
    'Easy one-touch operation.',
    'Product dimensions: 12cm * 25cm * 33cm.'
];

export const kindsOfWater = [
    {
        imgSrc: '/PNOY/cooking.svg',
        title: {
            name: 'cooking',
            color: '#634C9D'
        },
        subHeading: 'ALkaline 4 PH',
        ph: '10.5 - 12.0'
    },
    {
        imgSrc: '/PNOY/beverage.svg',
        title: {
            name: 'beverage',
            color: '#2D5596'
        },
        subHeading: 'ALkaline 3 PH',
        ph: '9.0 - 10.5'
    },
    {
        imgSrc: '/PNOY/glass.svg',
        title: {
            name: 'daily drinking',
            color: '#1C739D'
        },
        subHeading: 'ALkaline 2 PH',
        ph: '8.5 - 9.0'
    },
    {
        imgSrc: '/PNOY/glass.svg',
        title: {
            name: 'initial drinking',
            color: '#18A454'
        },
        subHeading: 'ALkaline 1 PH',
        ph: '7.5 - 8.1'
    },
    {
        imgSrc: '/PNOY/Medication.svg',
        title: {
            name: 'neutral medication',
            color: '#9ECC47'
        },
        ph: 'purified 7.0'
    },
    {
        imgSrc: '/PNOY/Facial.svg',
        title: {
            name: 'facial acid',
            color: '#ED6B36'
        },
        subHeading: 'Weakly Acidic PH',
        ph: '4.0 - 6.0'
    },
    {
        imgSrc: '/PNOY/Sterilization.svg',
        title: {
            name: 'sterilization acid',
            color: '#C33538'
        },
        subHeading: 'Acidic PH',
        ph: '2.5 - 4.0'
    }
];

export const displayScreens = [
    { imgSrc: '/PNOY/purple-screen.jpg', title: 'Purple', subHeading: 'Cooking' },
    {
        imgSrc: '/PNOY/dark-blue-screen.jpg',
        title: 'Dark Blue',
        subHeading: 'Beverage'
    },
    {
        imgSrc: '/PNOY/blue-screen.jpg',
        title: 'Blue',
        subHeading: 'Daily Drinking'
    },
    {
        imgSrc: '/PNOY/light-blue-screen.jpg',
        title: 'Light Blue',
        subHeading: 'Initial Drinking'
    },
    {
        imgSrc: '/PNOY/green-screen.jpg',
        title: 'Green',
        subHeading: 'Neutral Medication'
    },
    { imgSrc: '/PNOY/yellow-screen.jpg', title: 'Yellow' },
    {
        imgSrc: '/PNOY/red-screen.jpg',
        title: 'Red',
        subHeading: 'Sterilization Acid'
    }
];

export const kindsOfWaterColorVariants = {
    '#634C9D': 'text-[#634C9D]',
    '#2D5596': 'text-[#2D5596]',
    '#1C739D': 'text-[#1C739D]',
    '#18A454': 'text-[#18A454]',
    '#9ECC47': 'text-[#9ECC47]',
    '#ED6B36': 'text-[#ED6B36]',
    '#C33538': 'text-[#C33538]'
};

export const faq = {
    hi: {
        gq: [
            {
                ques: 'कैसे जांचें कि क्या मुझे सभी गुणों वाला आयोनाइज़्ड पानी मिल रहा है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'इसके लिए आप हर दस पंद्रह दिनों में कुकिंग वॉटर बटन से आने वाले पानी में हल्दी घोल कर देखें, अगर यह लाल रंग का हो जाता है तो आप निश्चिंत रहें कि आपके पिनॉय आयनाइज़र का पानी आपको पूरी तरह से सही गुण प्रदान कर रहा है।'
                    }
                ]
            },
            {
                ques: 'हम अल्कलाइन वॉटर कैसे प्राप्त कर सकते हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'अल्कलाइन पानी प्राप्त करने के कई तरीके हैं, घरेलू मैथड्स के अलावा आप अल्कलाइन वॉटर बॉटल, अल्कलाइन वॉटर प्यूरीफायर एवम वॉटर आयनाइजर्स के माध्यम से आसानी से अल्कलाइन पानी प्राप्त कर सकते हैं।'
                    }
                ]
            },
            {
                ques: 'क्या मुझे वॉटर आयोनाइज़र के साथ आरओ मशीन भी लगाना आवश्यक है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'हां, लेकिन केवल तभी जब आपके आपूर्ति किए गए पानी का टीडीएस 200 से अधिक हो। और निश्चित रूप से आरओ मशीन सभी हानिकारक केमिकल्स को फ़िल्टर करती है इसलिए यह आपके वॉटर आयोनाइज़र डिवाइस का एक आदर्श साथी होगा।'
                    }
                ]
            },
            {
                ques: 'वॉटर आयोनाइज़र खरीदते समय हमें किन मापदंडों का ध्यान रखना चाहिए?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'वॉटर आयोनाइज़्चखरीदने से पहले आपको निम्नलिखित बातों का ध्यान रखना चाहिए-'
                    },
                    {
                        format: 'text',
                        content:
                            '1. आयोनाइज़र की वारंटी के दौरान और उसके बाद भी लगातार सही सेवाएं प्राप्त करते रहने के लिए आपको इसे किसी इंपोर्टर या नेटवर्क मार्केटिंग कंपनी से नहीं, बल्कि मैन्युफैक्चरिंग कंपनी से खरीदना चाहिए।'
                    },
                    {
                        format: 'text',
                        content:
                            '2. आपको आयोनाइज़र के मेडिकल डिवाइस सर्टिफिकेट की जांच करनी चाहिए जो कि आईएसओ 13485 है।'
                    },
                    {
                        format: 'text',
                        content:
                            '3. आपको चेक करना चाहिए कि क्या आयोनाइज़र मशीन से आने वाले पानी के पीएच को बढ़ाने के लिए किसी हानिकारक केमिकल का उपयोग तो नहीं किया जा रहा है?'
                    },
                    {
                        format: 'text',
                        content:
                            '4. आपको आयोनाइज़र मशीन में लगने वाले फिल्टर की फिल्ट्रेशन कैपेसिटी और कीमत की जांच करनी चाहिए।'
                    },
                    {
                        format: 'text',
                        content:
                            '5. आपको अपना वॉटर आयोनाइज़र किसी भी नेटवर्क मार्केटिंग कंपनी से नहीं खरीदना चाहिए क्योंकि नेटवर्क मार्केटिंग कंपनियों का आफ्टर सेल्स सर्विस के लिए कोई स्ट्रक्चर नहीं है और तो और इन कंपनीज से खरीदने पर आपको आयोनाइज़र की लगभग दोगुनी कीमत चुकानी पड़ती है।'
                    }
                ]
            },
            {
                ques: 'कैसे जांचें कि मेरे पीने के पानी में सही गुण हैं या नहीं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आपको नियमित रूप से अपने पीने के पानी की पीएच स्तर, ऑक्सीकरण क्षमता, माइक्रो क्लस्टरिंग और टीडीएस की जांच करनी चाहिए, जिसके लिए बाजार में कई उपकरण उपलब्ध हैं। वहीं दूसरी ओर आप उपरोक्त मापदंडों पर अपने पीने के पानी की जांच कराने के लिए हमें कॉल कर सकते हैं। और यह जांच हम आपके लिए फ्री करते हैं।'
                    }
                ]
            },
            {
                ques: 'क्या हमारा उपलब्ध पीने का पानी हमारे शरीर को नुकसान पहुंचा रहा है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'विशेष रूप से शहरी क्षेत्रों में हमें उपलब्ध पीने का पानी दूषित और एसिडिक होता है, और ऑक्सीडेशन का कारक होता है, इसके अलावा अनुचित टीडीएस के पानी को पीने से मनुष्यों में कम उम्र में कई बीमारियाँ हो रही हैं।'
                    }
                ]
            }
        ],
        machine: [
            {
                ques: 'पिनॉय आयोनाइजर मशीन से कितना पानी ले सकते हैं ?',
                ans: [
                    {
                        format: 'text',
                        content: 'लगभग 80-90 लीटर प्रति घंटा |'
                    }
                ]
            },
            {
                ques: 'क्या पिनॉय आयोनाइजर मशीन में पानी का स्टोरेज भी है?',
                ans: [
                    {
                        format: 'text',
                        content: 'नहीं।'
                    }
                ]
            },
            {
                ques: 'आयोनाइजर मशीन की बिजली खपत कितनी है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'ऑपरेटिंग मोड में 110 - 240 वोल्ट और स्टैंडबाय मोड में 3.5 वॉट।'
                    }
                ]
            },
            {
                ques: 'मशीन को सही स्थिति में रखने के लिए रखरखाव क्या है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            '1. सुनिश्चित करें कि फ़िल्टर प्रत्येक 10000 लीटर या 1 वर्ष, जो भी पहले हो, पर बदला जाए।'
                    },
                    {
                        format: 'text',
                        content:
                            '2. लगभग 9 महीने पर आयोनाइजर डिवाइस की डीप क्लीनिंग करवाएं।'
                    }
                ]
            },
            {
                ques: 'पानी के pH के एक स्तर से pH के दूसरे स्तर पर चेंज करने में आयोनाइजर को कितना समय लगता है?',
                ans: [
                    {
                        format: 'text',
                        content: 'लगभग 5 से 7 सेकंड |'
                    }
                ]
            },
            {
                ques: 'हम वॉटर आयोनाइजर को कितनी देर तक लगातार चला सकते हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'वॉटर आयोनाइजर को लगातार 30 मिनट तक चलाया जा सकता है, फिर 5 मिनट के बाद फिर से 30 मिनट तक चलाया जा सकता है,और इसी प्रकार आगे भी।'
                    }
                ]
            },
            {
                ques: 'वॉटर आयोनाइजर डिवाइस के लिए सही इनपुट वॉटर टीडीएस क्या होना चाहिए?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'वॉटर आयोनाइजर को पानी का इनपुट 125 - 150 टीडीएस के बीच का देना चाहिए।'
                    }
                ]
            },
            {
                ques: 'वाटर आयोनाइजर मशीन क्या होती है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'वॉटर आयनाइज़र मशीन प्लैटिनम कोटेड टाइटेनियम प्लेटों से बनी एक मशीन है जो आयोनाइजेशन प्रक्रिया के माध्यम से आपके पीने के पानी में चार महत्वपूर्ण गुण पैदा करता है यानी माइक्रोक्लस्टरिंग, अल्क्लिनिटी, हाइड्रोजनीकरण और एंटीऑक्सीडेंट इस प्रकार आपके पीने के पानी को दुनिया का सबसे स्वास्थ वर्धक पेयजल बनाता है।'
                    }
                ]
            },
            {
                ques: 'हमें वाॅटर आयनाइज़र मशीन की आवश्यकता क्यों है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आज के समय में बढ़ते प्रदूषण, रेडिएशन, कीटनाशकों, हमारे भोजन की आदतों, तनावपूर्ण वर्क प्रोफ़ाइल और यहां तक ​​कि हमारे पीने के पानी के कारण हमारे लिए एसिडिक, डिहाइड्रेशन और ऑक्सीडेटिव तनाव पैदा हो रहा है जो मानव शरीर में 90% जीवनशैली रोगों का मूल कारण है। और PNOY वॉटर आयोनाइजर आयनाइजेशन की प्रक्रिया से हमारे पीने के पानी को दुनिया का सबसे स्वास्थ्यवर्धक पेयजल बनाता है और हमें इन तनावों को बेअसर करने और 400 से अधिक बीमारियों से बचाने में मदद करता है।'
                    }
                ]
            },
            {
                ques: 'आयोनाइजर मशीन में पॉवर कंजप्शन कितना होता है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइजर मशीन को चलाते समय 110 से 240 वोल्ट्स की आवश्यकता होती है, जबकि स्टैंडबाय मोड में 3.5 वॉट पॉवर कंज्यूम होती है।'
                    }
                ]
            }
        ],
        ionizedWater: [
            {
                ques: 'हाइड्रोजन वॉटर क्या है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'वॉटर आयोनाइजर डिवाइस का लाइव पानी मॉलिक्युलर हाइड्रोजन से भरपूर होता है, जिसमें शक्तिशाली एंटी-इंफ्लेमेटरी गुण होते हैं, जिससे मस्तिष्क की कार्यक्षमता में सुधार होता है, दर्द से आराम मिलता है और त्वरित ऊर्जा वृद्धि होती है।'
                    }
                ]
            },
            {
                ques: 'एंटीऑक्सीडेंट पानी क्या है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'एंटी-ऑक्सीडेंट पानी एंटीऑक्सीडेंट गुणों से भरपूर पानी है जो आपके शरीर और इंटरनल अंगों को फ्री रेडिकल्स से होने वाले नुकसान को न्यूट्रलाइज कर सुरक्षा प्रदान करता है।'
                    }
                ]
            },
            {
                ques: 'अल्कलाइन वॉटर क्या है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'अल्कलाइन पानी वह पानी है जिसकी पीएच वैल्यू 7.5 या उससे अधिक होती है।'
                    }
                ]
            },
            {
                ques: 'माइक्रो क्लस्टर वॉटर क्या है ?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'माइक्रो क्लस्टर्ड पानी वह पानी है जो 5 गुना बेहतर सेलुलर हाइड्रेशन सुनिश्चित करने के लिए पानी के अणुओं के छोटे क्लस्टर के रूप में होता है। माइक्रो क्लस्टरिंग आयोनाइज़्ड पानी के चार महत्वपूर्ण गुणों में से एक है।'
                    }
                ]
            },
            {
                ques: 'आयोनाइजर से मिलने वाले पानी में क्या क्या प्रॉपर्टीज होती हैं ?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइज़्ड पानी में चार महत्वपूर्ण गुण शामिल हैं जो अल्क्लिन, माइक्रो क्लस्टरिंग, हाइड्रोजन और एंटीऑक्सीडेंट हैं। ये गुण आयोनाइज़्ड पानी को दुनिया का सबसे स्वास्थ्यवर्धक पीने का पानी बनाते हैं।'
                    }
                ]
            },
            {
                ques: 'आयोनाइज़्ड पानी लंबे समय तक पीते रहने के बाद क्या बाहर का पानी पीने से मुझे नुकसान होगा ?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'बिल्कुल नहीं, क्योंकि आयोनाइज़्ड पानी पीने से आपके शरीर और इंटरनल अंगों की कार्यक्षमता में सुधार होता है, इसलिए आपका शरीर बाहरी पानी के हानिकारक प्रभाव को बेअसर करने में और भी अधिक सक्षम होगा। हालांकि केवल स्वाद भिन्न हो सकता है।'
                    }
                ]
            },
            {
                ques: 'मुझे प्रतिदिन कितना आयोनाइज़्ड पानी पीना चाहिए?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'किसी को अपने शरीर के वजन का कम से कम 5% पानी पीना चाहिए। उदाहरण के लिए यदि आपका वजन 80 किलोग्राम है तो आपको प्रतिदिन 4 लीटर आयोनाइज़्ड पानी अवश्य पीना चाहिए।'
                    }
                ]
            },
            {
                ques: 'क्या आयोनाइज़्ड पानी पीने के बारे में कोई सावधानी है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइज़्ड पानी पीना आम तौर पर हर किसी के लिए सुरक्षित है। लेकिन इसे भोजन से आधा घंटा पहले और भोजन करने के बाद एक घंटे तक नहीं पीना चाहिए।'
                    }
                ]
            },
            {
                ques: 'हम आयोनाइज़्ड पानी को कितने समय तक संग्रहित कर सकते हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइज़्ड पानी को 24 घंटे तक संग्रहीत किया जा सकता है लेकिन अधिकतम लाभ प्राप्त करने के लिए जितना संभव हो सके लाइव पानी को तुरंत पीने की सलाह दी जाती है।'
                    }
                ]
            },
            {
                ques: 'क्या बच्चों के लिए आयोनाइज़्ड पानी पीना सुरक्षित है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'हालांकि ऐसी कोई रिपोर्ट नहीं है जो बताती हो कि आयोनाइज़्ड पानी पीना बच्चों के लिए असुरक्षित है, फिर भी 6 साल से कम उम्र के बच्चों को आयोनाइज़्ड पानी देने से बचें।'
                    }
                ]
            },
            {
                ques: 'क्या आयोनाइज़्ड पानी पीने के कोई दुष्प्रभाव हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'नहीं, बिल्कुल नहीं, सबसे पहले, आयनीकरण प्रक्रिया आपके पीने के पानी से हानिकारक एसिडिक पदार्थ को अलग करती है। दूसरी बात यह कि यह आपके पीने के पानी में कुछ भी नहीं मिलाता है, बल्कि पानी के आंतरिक गुणों को बढ़ाता है। और अंत में आयोनाइज़्ड पानी आपके इंटरनल ओरगंस जैसे लिवर, किडनी, हार्ट आदि को मजबूत बनाता है जिससे उनकी अधिक कुशलता से काम करने की क्षमता बढ़ती है और आप स्वस्थ रह पाते हैं।'
                    }
                ]
            },
            {
                ques: 'क्या आयोनाइज़्ड वॉटर और केंगेन वॉटर अलग-अलग हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'नहीं, दोनो एक ही हैं, केंगेन वॉटर वॉटर आयोनाइजर बनाने वाली कंपनियों में से एक का ब्रांड नाम है।'
                    }
                ]
            },
            {
                ques: 'मुझे आयोनाइज़्ड पानी पीने के लाभ कितनी जल्दी अनुभव होने लगेंगे?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइज़्ड पानी पीने के पहले दिन से ही आपके शरीर की मरम्मत शुरू हो जाएगी और आयोनाइज पानी पीने के 15 दिनों के भीतर आपकी ऊर्जा में वृद्धि, पाचन में सुधार, एसिडिटी नहीं होने और कब्ज में राहत के रूप में स्पष्ट लाभ दिखाई देंगे।'
                    }
                ]
            },
            {
                ques: 'क्या आयोनाइज़्ड पानी पीने के क्या फायदे हैं?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'आयोनाइज़्ड पानी 5 गुना बेहतर हाइड्रेशन और डिटॉक्सिफिकेशन सुनिश्चित करता है, आपके इंटरनल ओरगंस जैसे कि लिवर, किडनी आदि के अतिरिक्त काम के बोझ को कम करता है, आपके इंटर्नल ओरगेंस को ऑक्सीडेशन से होने वाले नुकसान से बचाता है और इन्फ्लेमेशन को ठीक करता है।'
                    }
                ]
            },
            {
                ques: 'क्या आयोनाइज़्ड पानी और दुनिया भर के स्वास्थ प्रदायक जल स्रोतों के पानी के गुणों में कोई समानता है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'हां, स्वास्थ प्रदायक पानी के स्त्रोतों और आयोनाइज़्ड पानी के गुणों में कई समानताएं हैं, जो शोधकर्ताओं द्वारा सहस्त्रधारा, भीमकुंड, गंगोत्री आदि के पानी की तुलना आयोनाइज्ड पानी से करने पर पाई गईं।'
                    }
                ]
            },
            {
                ques: 'क्या एक स्वस्थ व्यक्ति को आयोनाइज़्ड पानी पीने की जरूरत है?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'बाह्य रूप से हम स्वस्थ दिख सकते हैं लेकिन आंतरिक रूप से बढ़ते रेडिएशन, प्रदूषण, कीटनाशकों, केमिकल्स जैसे कारणों के कारण डिहाइड्रेशन, एसिडिक और ऑक्सीडेटिव स्ट्रेस का सामना कर रहे हैं। जिससे कई जीवनशैली संबंधी बीमारियों जैसे मधुमेह, जोड़ों की समस्याएं, हृदय रोग आदि का खतरा बढ़ रहा है। और आयोनाइज़्ड पानी पीने से ये तनाव बेअसर हो जाता है और हमें इन बीमारियों से बचाता है और हमें सामान्य और स्वस्थ जीवन जीने में मदद करता है।'
                    }
                ]
            }
        ]
    },
    en: {
        gq: [
            {
                ques: 'Is our drinking water causing harm to us?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Especially in urban areas, our available drinking water is contaminated and acidic in nature. This contamination leads to oxidation, and the consumption of water with inappropriate TDS (Total Dissolved Solids) levels is causing many diseases at an early age in humans.'
                    }
                ]
            },
            {
                ques: 'How can I check if my drinking water has the right properties?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "You must check your drinking water for its pH level, oxidation potential, micro clustering, and TDS regularly. There are many instruments available in the market for this purpose. Additionally, you can call us to get your drinking water checked for the above parameters, and it's absolutely free of cost."
                    }
                ]
            },
            {
                ques: 'How can I check if I am getting Ionized water with all the desired properties?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Every fortnight, you should conduct a curcumin test. Dissolve curcumin in cooking water, and if it turns red, you can be assured that your ionizer is delivering the desired properties perfectly.'
                    }
                ]
            }
        ],
        machine: [
            {
                ques: 'What are the benefits of drinking Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Ionized water ensures 5 times better hydration and detoxification, reduces the extra workload of your internal organs, prevents oxidative damage to your internal organs, and reduces inflammation. Moreover, drinking ionized water helps in weight loss, has anti-aging properties, improves brain function, reduces stress, offers a quick energy boost, improves bone health, digestion, and many other health benefits.'
                    }
                ]
            },
            {
                ques: 'What is a water Ionizer?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'A water Ionizer is a device made of platinum-coated titanium plates which, through the ionization process, generates important properties in your drinking water, such as micro clustering, alkalinity, hydrogenation, and antioxidants. This makes your drinking water the healthiest in the world.'
                    }
                ]
            },
            {
                ques: 'Why do we need water Ionizers?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "In today's time, due to increased pollution, radiation, pesticides, our food habits, stressful work profiles, and even our drinking water, acidic conditions, dehydration, and oxidative stress are caused, which are the root causes of 90% of lifestyle diseases in the human body. PNOY's water ionizer, through the process of ionization, makes our drinking water the healthiest option and helps neutralize these stressors, preventing over 400 diseases."
                    }
                ]
            },
            {
                ques: 'What parameters should we consider while selecting a water Ionizer?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "There are a few things you should consider before finalizing a water ionizer: You should buy it from an OEM company rather than an importer in order to receive consistent services during and after the product's warranty period. You should check if the seller has medical device certification, such as ISO 13485. You should verify whether the ionizer device uses any harmful chemicals for pH enhancement. You should check the filtration capacity and price of ionizers with built-in filters. You should be cautious about purchasing through any network marketing company, as they tend to have to double the price with no structured sales or service team."
                    }
                ]
            },
            {
                ques: 'Do I need the RO machine along with an Ionizer?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Yes, but only when your supplied water TDS is more than 200. RO (reverse osmosis) machines filter out all the harmful chemicals, so it would be an ideal companion for your water Ionizer device.'
                    }
                ]
            },
            {
                ques: 'What should be the right input water TDS for the Ionizer device?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'The water input to the Ionizer should be between 125-150 TDS.'
                    }
                ]
            },
            {
                ques: 'For how long can we run the water Ionizer continuously?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'The Ionizer can be operated continuously for 30 minutes, then it should be put on hold for 5 minutes before running again for another 30 minutes, and so on.'
                    }
                ]
            },
            {
                ques: 'How long does it take to shift from one pH level to another pH level?',
                ans: [
                    {
                        format: 'text',
                        content: 'It takes about 5 to 7 seconds.'
                    }
                ]
            },
            {
                ques: 'What is the maintenance required to keep the machine in Perfect condition?',
                ans: [
                    {
                        format: 'text',
                        content:
                            '1. Ensure the filter is changed every 10,000 L or one year, whichever comes earlier. 2. Deep clean the machine approximately every 8 months.'
                    }
                ]
            },
            {
                ques: 'What is the power consumption?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'In operating mode, it requires 110 - 240 volts, and in standby mode, it consumes 3.5 watts.'
                    }
                ]
            },
            {
                ques: 'Does this machine store water?',
                ans: [
                    {
                        format: 'text',
                        content: 'No, it does not.'
                    }
                ]
            },
            {
                ques: 'What is the water dispensing capacity of the machine?',
                ans: [
                    {
                        format: 'text',
                        content: 'The machine can dispense about 80 - 90 L per hour.'
                    }
                ]
            },
            {
                ques: 'What is the most important thing in order to get the best outcome from my Ionizer device?',
                ans: [
                    {
                        format: 'text',
                        content: "Keep a strict check on your RO water's output TDS."
                    }
                ]
            }
        ],
        ionizedWater: [
            {
                ques: 'How soon will I begin experiencing the benefits of drinking Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Drinking Ionized water will start repairing your body from day one itself, and visible benefits such as increased energy, improved digestion, no acidity, and relief from constipation will be noticeable within 15 days of consuming Ionized water.'
                    }
                ]
            },
            {
                ques: 'Is there any similarity in the properties of Ionized water and the healthy water sources around the world?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "Yes, there are many similarities in the water properties that were found by researchers when comparing the properties of Ionized water with the water from Sahastradhara, Bhimkund, Gangotri, etc. That is why Ionized water is also called medicinal water, healing water, and the world's healthiest drinking water."
                    }
                ]
            },
            {
                ques: 'Is Ionized water and Kangen water different?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'No, they are not different, as Kangen water is the brand name of one of the many companies dealing with Ionized water.'
                    }
                ]
            },
            {
                ques: 'Are there any side effects of drinking Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "No, absolutely not. Firstly, the Ionization process separates the harmful acidic components from your drinking water. Secondly, it doesn't add anything to your drinking water but just enhances its intrinsic properties. Finally, Ionized water strengthens your internal organs, thus increasing their capacity to work more efficiently and keeping you healthy."
                    }
                ]
            },
            {
                ques: 'Is it safe for children to drink Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Although there are no reports suggesting that drinking Ionized water is unsafe for kids, it is still advisable to avoid giving Ionized water to children under the age of 6 years.'
                    }
                ]
            },
            {
                ques: 'How long can we store Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Ionized water can be stored for up to 24 hours, but it is recommended to drink freshly Ionized water as much as possible in order to maximize its benefits.'
                    }
                ]
            },
            {
                ques: 'How much Ionized water should I drink every day ?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "One should drink at least 5% of their body weight water for example if you weigh 80 KG then you should drink 4 litres of ionised water everyday. Which is why Ionised water is also called Medicinal Water, Healing Water and World's Healthiest drinking water."
                    }
                ]
            },
            {
                ques: 'Are there any cautions about drinking Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Drinking Ionized water is generally safe for everyone. However, one should avoid drinking it before and after a meal.'
                    }
                ]
            },
            {
                ques: 'Should an otherwise healthy person care about drinking ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Externally, we may appear healthy, but internally we are facing dehydration, acidic, and oxidative stresses due to increased radiation, pollution, pesticides, chemical exposure, and many others. These factors increase the risk of many lifestyle diseases such as diabetes, joint problems, cardiac diseases, and so on. Drinking ionized water neutralizes these stresses, prevents us from these diseases, and helps us lead a normal and healthy life.'
                    }
                ]
            },
            {
                ques: 'Will drinking outdoor water harm me after I get used to drinking Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Absolutely NO, drinking Ionized water improves the efficiency of your body and internal organs, so your body would be even more capable of neutralizing the harmful effects of outdoor water. Though only the taste may differ.'
                    }
                ]
            },
            {
                ques: 'What is Ionized water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            "IONIZED WATER includes four important properties: alkalinity, micro clustering, hydrogen, and antioxidants. These properties make ionized water the world's healthiest drinking water."
                    }
                ]
            },
            {
                ques: 'What is Micro-Clustered Water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'MICRO-CLUSTERED WATER is water that is in the form of smaller clusters of water molecules in order to ensure 5 times better cellular hydration. Micro-clustering is one of the four important properties of ionized water.'
                    }
                ]
            },
            {
                ques: 'What is Alkaline water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Alkaline water is water that has a pH value greater than 7.5 or above.'
                    }
                ]
            },
            {
                ques: 'What is antioxidant water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Antioxidant water is nothing but water full of antioxidants, which help neutralize free radical damage to your body and internal organs. Antioxidant water works great as it offers anti-aging properties and prevents the damage caused to your internal organs by free radicals.'
                    }
                ]
            },
            {
                ques: 'What is Hydrogen water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'Live water from a water Ionizer device is full of molecular hydrogen, which has powerful anti-inflammatory properties. This improves brain function, reduces pain, and offers a quick energy boost.'
                    }
                ]
            },
            {
                ques: 'How can we get Alkaline Water?',
                ans: [
                    {
                        format: 'text',
                        content:
                            'There are many ways to get alkaline water besides home processes. You can get alkaline water through alkaline water bottles, alkaline water purifiers, and, of course, alkalinity is one of the four inclusive properties of Ionized water.'
                    }
                ]
            }
        ]
    }
};

export const testimonials = [
    {
        imgSrc: '/PNOY/testimonial.jpeg',
        content:
            "11 Plate Gold Model Ionizer user, I am an 80-year-old who had difficulty walking due to joint pain.However, after only 4 months of drinking Pnoy's lonized Water, I can now walk comfortably, even without a stick, for shorter distances.",
        userName: 'Mr. gopal sahai shrivastav',
        userOccupation: 'Retired Govt. Employee And Practicing Astrologer'
    },
    {
        imgSrc: '/PNOY/testimonial-2.jpeg',
        content:
            "My family has been drinking Pnoy's Ionized Water since 2022. We have experienced an improvement in our overall energy levels and relief from acidity, as no one in our family has had any other major health issues. I had this device installed because, as a doctor, I understand the importance of prevention, and this Ionizer device definitely serves that purpose.",
        userName: 'Dr. Ravindra Bansal',
        userOccupation: 'ENT Surgeon'
    },
    {
        imgSrc: '/PNOY/testimonial-3.jpeg',
        content:
            "11-plate gold model user, We have been using the Pnoy water Ionizer for 3 to 4 years and are extremely happy with the results and services of the company.Indigestion and constipation were major health concerns we had before we started drinking Pnoy Ionized water.It's a great device. Thanks, Pnoy.",
        userName: 'Mr. Bishan Singh',
        userOccupation: 'Retired Railway Engineer'
    },
    {
        imgSrc: '/PNOY/testimonial-4.jpeg',
        content:
            "11-plate gold model user, Three years ago, the Pnoy water Ionizer came to my rescue as I had severe problems with migraines and hair loss. After drinking Ionized water for six months, I started experiencing relief in both areas. The migraines almost disappeared within a year, and I now have minimal hair loss. I would definitely recommend it to everyone.",
        userName: 'Mrs. Vinita Singh',
        userOccupation: 'Homemaker'
    },

    {
        imgSrc: '/PNOY/testimonial-4.jpg',
        content:
            "Pnoy's Hydrogen Generator Device User As a fitness coach for many years, I understand the importance of antioxidants and anti-inflammatory supplements. And it becomes effortless with Pnoy's hydrogen generator device, which can be conveniently carried with you wherever you go. I strongly recommend it to all fitness enthusiasts as it would be a great addition to your fitness journey.",
        userName: 'Mr. Anoop Gupta',
        userOccupation: 'Fitness Coach - Talwalkars'
    },

    {
        imgSrc: '/PNOY/testimonial-5.jpg',
        content:
            "7-plate silver model Ionizer user I bought this Pnoy Ionizer machine 5 years ago because my 80-year-old mother had been suffering from chronic acidity for the last 35 years, and no medicines or treatments were able to provide her with lasting relief. At that time, I heard about the benefits of Pnoy's Ionized water and purchased the machine. The benefits my mother received from drinking Ionized water were far better than what I had heard. Within six months of drinking Ionized water, my mother experienced complete relief from her chronic acidity. I would also like to give Pnoy Water Ionizer's team additional points for their excellent after-sales service every time in the last 5 years.",
        userName: 'Mr. Sudesh Jhawar',
        userOccupation: 'Business Owner'
    },

    {
        imgSrc: '/PNOY/testimonial-6.png',
        content:
            "Being a cardiovascular surgeon, I have always understood the importance of maintaining the right cholesterol profile. Back in 2017, I came across Pnoy Ioniser and had it installed at my home. Since then, my family and I have experienced immense benefits from drinking Ionized water, especially in terms of maintaining the right body weight and lipid profile. After personally testing it, I have recommended Pnoy's water Ionizer to nearly 50-60 patients of mine who were suffering from various cardiac ailments. They have purchased the Pnoy water Ionizer and are responding extremely well to my treatment, along with experiencing significant benefits from drinking Ionized water. I strongly recommend drinking Pnoy's Ionized water not only to those who are already diseased for faster recovery but also to otherwise healthy individuals to maintain their overall health.",
        userName: 'Dr. Ajay Yadav',
        userOccupation: 'Cardiovascular and Thoracic surgeon - Sir Gangaram hospital'
    },

    {
        imgSrc: '/PNOY/testimonial-7.jpg',
        content: "7 Plate Silver Model User: When I chose to install Pnoy Ionizer devices, I must say it turned out to be a good decision. One significant benefit I experienced is that my doses of anti-hypertensive medicine were reduced by half within four months of drinking Ionized water. Now, after six months, my blood pressure remains normal even without medication.  I must say that drinking Ionized water is a great addition to my daily life. Thanks, Pnoy.",
        userName: 'Mr. Sanjeev Talwar',
        userOccupation: 'Hotelier and Contractor'
    }
];

export const format = {
    TEXT: 'text',
    IMAGE: 'image',
    HEADING: 'heading',
    VIDEO: 'video',
    BADGE: 'badge',
    TABLE: 'table'
};
// export const medicalResearch = [
//     {
//         title: 'Benefits of Ionised Hydrogen water',
//         content: [
//             {
//                 format: 'image',
//                 imgSrc: '/PNOY/accordian.png',
//                 alt: 'rain-water'
//             },
//             {
//                 format: 'text',
//                 content:
//                     'There are hundreds and thousands of articles and testimonials, which show the healing benefits of ionised  hydrogen water.Free-Molecular hydrogen is a unique molecular combination of two hydrogen atoms, bonded together, but not to any other elements.Ionised Hydrogenated water is known to  protect and heal the kidneys, heart, liver, stomach, intestines, skin, brain, lungs, eyes and pretty much every part of the body.'
//             },
//             {
//                 format: 'heading',
//                 content:
//                     'The best way to consume free-Molecular hydrogen is through ionised water.'
//             },
//             {
//                 format: 'text',
//                 content:
//                     'The most widely used and possibly the most therapeutic form of free-molecular hydrogen is in ionised water. Ionised water is infused with free molecular  hydrogen that is produced by a water ioniser machine, which splits the water in two streams, alkaline and acidic. The alkaline water is packed with free hydrogen and so is therapeutic for that reason as well as the fact of being an alkaline pH.'
//             },
//             {
//                 format: 'heading',
//                 content:
//                     "Molecular hydrogen's ability to scavenge free radicals anywhere in the body"
//             },
//             {
//                 format: 'text',
//                 content:
//                     'Molecular Hydrogen is known by many prominent researchers world-wide for its ability to truly penetrate through cell membranes (even the blood-brain barrier) and scavenge free radicals.'
//             },
//             {
//                 format: 'text',
//                 content:
//                     'One reason Hydrogen is not like any other antioxidant is due to its tiny size. Hydrogen is also lipid soluble, which is important, because lipid solubility is necessary for entering into cells through the cell membranes. So, Hydrogen (unlike any other antioxidant) can get into every living part of the body. All other antioxidants are incapable of this feat. Another thing that makes it different from all other free-radical scavengers is that it simply evaporates or turns into water when it\'s "used up", so it creates no danger of becoming a free- radical itself as other antioxidants do.'
//             },
//             {
//                 format: 'heading',
//                 content: 'Free molecular hydrogen - safest antioxidant available'
//             },
//             {
//                 format: 'text',
//                 content:
//                     "As we all know, the combination of H2 and Oxygen makes water, which is at the source of all life forms. Oxygen also brings life, but unfortunately complications arise when there is too much oxygen within the body and there is not enough balance of hydrogen. Oxygen can actually become a dangerous free radical within the body when it has lost its electron balance. When it's in this state it's called reactive Oxygen species which causes severe damage to cells,This condition is called Oxidative Stress, and is thought to be at the root of almost all disease."
//             },
//             {
//                 format: 'text',
//                 content:
//                     'a tiny little molecular hydrogen can calm it down completely; neutralise it; turn it into harmless water and make it useful for the body. This is done with no harmful waste or byproducts produced.'
//             },
//             {
//                 format: 'text',
//                 content:
//                     "At the same time molecular hydrogen doesn't attack good radicals in the body.It will never act as a poison to the body, and any excess of it will simply evaporate harmlessly and combine with oxygen in the air to hydrate or moisturise it."
//             },
//             {
//                 format: 'text',
//                 content:
//                     "Other types of antioxidants have been found to become free-radicals, themselves, once they have donated their electron to neutralise another molecule. This doesn't happen with molecular hydrogen.As a matter of fact molecular hydrogen is able to refresh the antioxidant abilities of various other antioxidants to keep them from becoming free- radicals.After much testing and scrutinising by research scientists worldwide, no negative effects whatsoever have been found to be caused by molecular hydrogen."
//             }
//         ]
//     },
//     {
//         title: 'Fighting free-radicals and heart disease',
//         content: [
//             {
//                 format: 'image',
//                 imgSrc: '/PNOY/HEART DISEASE-01.png',
//                 alt: 'HEART DISEASE'
//             },
//             {
//                 format: 'text',
//                 content:
//                     'Dr. Shigeo Ohta (of Japan) specifically devoted many years of research on the free radical benefits of ionised water and hydrogen water. Free-hydrogen (molecular hydrogen) was shown to effectively neutralise free-radicals, without any of the negative side-effects often seen with other antioxidants. This is important. Fighting free-radicals while battling (or preventing) cancer, heart disease or 170 other diseases is a huge benefit that alkaline ionised water and hydrogen water both offer. In this video Dr. Ohta, is being interviewed by the CEO of the Molecular Hydrogen Foundation. They talk about the latest research into Molecular Hydrogen and the benefits of hydrogen water and hydrogen gas.'
//             },
//             {
//                 format: 'video',
//                 videoId: '1Up2i35MLdw'
//             },
//             {
//                 format: 'text',
//                 content:
//                     'Here is another video where Tyler LeBaron of the Molecular Hydrogen Foundation explains this research in more detail, using charts and peer reviewed reports to clarify his statements.'
//             },
//             {
//                 format: 'video',
//                 videoId: '5a6MIISq2nw'
//             },
//             {
//                 format: 'heading',
//                 content:
//                     'Here is a partial list of diseases hydrogen antioxidants (molecular hydrogen) have been shown to help.'
//             },
//             {
//                 format: 'badge',
//                 content: [
//                     'Alexander disease',
//                     'Allergies',
//                     "Alzheimer's disease and other dementias",
//                     'Angina',
//                     'Asthma',
//                     'Atherosclerosis',
//                     "Athlete's foot",
//                     'Atopic dermatitis',
//                     'Bed sores',
//                     'Bone Loss',
//                     'Cancer (various types)',
//                     'Cardio renal symptoms',
//                     'Cataracts',
//                     'Cerebral haemorrhage',
//                     "Chemo's negative side effects",
//                     "Crohn's disease",
//                     'Chronic fatigue syndrome',
//                     'Cirrhosis',
//                     'Collagen disease',
//                     'Colon diseases',
//                     'Cystic fibrosis',
//                     'Dehydration',
//                     'Diabetes mellitus',
//                     'Diabetic necrosis',
//                     'Frontotemporal lobar degeneration',
//                     'Glaucoma',
//                     'Gout',
//                     'Heart attacks',
//                     'Heart disease or coronary artery disease',
//                     'HGC disorders',
//                     'High blood pressure',
//                     "Huntington's disease",
//                     'Hypersensitive disorders',
//                     'Hypertension',
//                     'Inflammatory disorders',
//                     'Ischemic brain injuries',
//                     "Kawasaki's disease",
//                     'Kidney disease',
//                     'Liver disease',
//                     'Lupus',
//                     'Malaise',
//                     'Metabolic syndrome',
//                     'Muscle fatigue of athletes'
//                 ]
//             }
//         ]
//     },
//     {
//         title:
//             'Research Articles on the Positive Effects of Ionised Hydrogen Water / Inhalation On the Heart',
//         content: [
//             {
//                 format: 'image',
//                 imgSrc: '/PNOY/POSITIVE EFFECTS ON THE HEART-01.png',
//                 alt: 'POSITIVE EFFECTS ON THE HEART'
//             },
//             {
//                 format: 'table',
//                 content: [
//                     {
//                         year: 'Spanning over 20 years worth of treating patients with ionised water',
//                         subject: 'Blood Pressure',
//                         description: [
//                             {
//                                 format: 'text',
//                                 content:
//                                     'Use of Ionized water in hypochlorhydria or achlorhydria Prof. Kuninaka Hironage, Head of Kuninaka Hospital.. ',
//                                 linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28321000/'
//                             },
//                             {
//                                 format: 'text',
//                                 content:
//                                     "The consumption of alkaline antioxidant water for a period of 2 to 3 months, I have observed the blood pressure slowly drop, due to the water's solvent ability, which dissolves the cholesterol in the blood vessels ."
//                             }
//                         ]
//                     },
//                     {
//                         year: '2017',
//                         subject: 'Myocardial Infarction',
//                         description: [
//                             {
//                                 format: 'text',
//                                 content:
//                                     'The Effects of Hydrogen Gas Inhalation on Adverse Left Ventricular Remodelling After Percutaneous Coronary Intervention for ST-Elevated Myocardial Infarction..',
//                                 linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28321000/'
//                             },
//                             {
//                                 format: 'text',
//                                 content:
//                                     '- First Pilot Study in Humans. [Human study shows inhaling hydrogen is safe and improves the markers of hypertension.]'
//                             }
//                         ]
//                     },
//                     {
//                         year: '2019',
//                         subject: 'Hypertension',
//                         description: [
//                             {
//                                 format: 'text',
//                                 content:
//                                     'Hydrogen gas improves left ventricular hypertrophy in Dahl rats of salt-sensitive hypertension..',
//                                 linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29902079/'
//                             },
//                             {
//                                 format: 'text',
//                                 content:
//                                     '[Animal study shows hydrogen reduces hypertension and left ventricular hypertrophy.]'
//                             }
//                         ]
//                     }
//                 ]
//             }
//         ]
//     }
// ];

export const scientificResearchOnIonizedWater = [
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/batmenghalid.jpg',
            alt: 'Your Body\'s Many Cries for Water'
        },
        name: 'F. Batmenghalid',
        title: 'Your Body\'s Many Cries for Water - You Are Not Sick, You Are Thirsty',
        text: [
            'A groundbreaking medical revelation showing how proper hydration with water can prevent and treat a range of illnesses and contribute to overall health and longevity.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/reid.jpg',
            alt: 'The Tao of Detox'
        },
        name: 'Daniel Reid',
        title: 'The Tao of Detox',
        text: [
            'This offers a comprehensive approach to detoxifying the body, combining ancient eastern wisdom and modern western science, to combat environmental pollutants and enhance overall health, energy, and longevity.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/tietze.jpg',
            alt: 'Youthing'
        },
        name: 'Harald Tietze',
        title: 'Youthing',
        text: [
            'By drinking alkaline water, the ageing process can be reversed and functions of the organs can be revived in order to live a long and healthy life.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/laucevicius.jpg',
            alt: 'Ionized Water - Life Without Diseases'
        },
        name: 'Telesforas Laucevicius',
        title: 'Ionized Water - Life Without Diseases',
        text: [
            'Explores the lesser-known but scientifically backed benefits of ionized water, used widely in Japan and South Korea for health and disease prevention.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/shaktiviya.jpg',
            alt: 'Breatharianism, Ionized Water and Pranic Nourishment'
        },
        name: 'Yogi Shaktiviya',
        title: 'Breatharianism, Ionized Water and Pranic Nourishment',
        text: [
            'Discusses the concept of living on prana and how ionized water can aid in achieving a healthier lifestyle through super nutrition and energy absorption.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/swaroop.jpg',
            alt: 'The Most Comprehensive Compilation of Research Papers on Alkaline Ionized Water'
        },
        name: 'Puneet Swaroop',
        title: 'The Most Comprehensive Compilation of Research Papers on Alkaline Ionized Water',
        text: [
            'A thorough collection of research and insights on the health benefits and commercial aspects of alkaline and hydrogen water products.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/yves.jpg',
            alt: 'The Ion Miracle: The Effects of Negative Ions on Physical and Mental Well Being'
        },
        name: 'Jean Yves',
        title: 'The Ion Miracle: The Effects of Negative Ions on Physical and Mental Well Being',
        text: [
            'Highlights the significant health benefits of negative ions in various environments, including their therapeutic effects on physical and mental health.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/johnson.jpg',
            alt: 'Healing Waters: The Powerful Health Benefits of Ionized Water'
        },
        name: 'Ben Johnson',
        title: 'Healing Waters: The Powerful Health Benefits of Ionized Water',
        text: [
            'Emphasizes the crucial role of oxygen-rich ionized water in maintaining health and combating chronic diseases.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/malik.jpg',
            alt: 'Healing Water: Facts about Ionized Water'
        },
        name: 'Sudesh Malik',
        title: 'Healing Water: Facts about Ionized Water',
        text: [
            'Presents extensive research on the myriad health benefits of Ionized Water, from improving immunity to holistic well-being.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/gordon.jpg',
            alt: 'Alkaline Water and the Effects of Drinking It on the Body'
        },
        name: 'Gordon',
        title: 'Alkaline Water and the Effects of Drinking It on the Body',
        text: [
            'Discusses how Alkaline Water can restore the body\'s pH balance and improve overall health, contrasting with the drawbacks of Reverse Osmosis water.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/young.jpg',
            alt: 'The pH Miracle - Reclaim Your Health'
        },
        name: 'Robert O. Young',
        title: 'The pH Miracle - Reclaim Your Health',
        text: [
            'Uncovers the critical importance of blood pH balance for health and weight loss, controllable through diet and alkaline water.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/kopco.jpg',
            alt: 'Benefits of Alkaline Water'
        },
        name: 'Peter Kopco',
        title: 'Benefits of Alkaline Water',
        text: [
            'Details the health advantages of Alkaline Water, including anti-aging effects, weight loss, and pH balance restoration.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/pickford.jpg',
            alt: 'Powerful Health Benefits of Alkaline Water'
        },
        name: 'Craig Pickford',
        title: 'Powerful Health Benefits of Alkaline Water',
        text: [
            'Outlines the numerous health benefits of alkaline water, from detoxification and hydration to aiding in athletic recovery.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/sherry.jpg',
            alt: 'Detoxify Or Die'
        },
        name: 'Dr Sherry Rogers',
        title: 'MD, Author : Detoxify or Die',
        text: [
            'Toxicity in the form of acidic waste is the primary cause of degenerative disease. ALKALINE WATER clears acidic waste and prevents degenerative diseases.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/theodore.jpg',
            alt: 'Alkalize Or Die'
        },
        name: 'Dr Theodore Baroody',
        title: 'Author : Alkalize or Die',
        text: [
            'The IONIZED ALKALINE WATER benefits everyone in health problems which I have seen in thousands of my patients.'
        ]
    },
    {
        image: {
            imgSrc: '/PNOY/scientificResearch/harald.jpg',
            alt: 'Youthing'
        },
        name: 'Harald Tietze',
        title: 'Author : Youthing',
        text: [
            'By drinking alkaline water, the ageing process can be reversed and functions of the organs can be revived in order to live a long and healthy life.'
        ]
    },

];

export const medicalResearch = [
    {
        title: 'Benefits of Ionised Hydrogen water',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/Ionised Hydrogen water.png',
                alt: 'rain-water'
            },
            {
                format: 'text',
                content:
                    'There are hundreds and thousands of articles and testimonials, which show the healing benefits of ionised  hydrogen water.Free-Molecular hydrogen is a unique molecular combination of two hydrogen atoms, bonded together, but not to any other elements.Ionised Hydrogenated water is known to  protect and heal the kidneys, heart, liver, stomach, intestines, skin, brain, lungs, eyes and pretty much every part of the body.'
            },
            {
                format: 'heading',
                content:
                    'The best way to consume free-Molecular hydrogen is through ionised water.'
            },
            {
                format: 'text',
                content:
                    'The most widely used and possibly the most therapeutic form of free-molecular hydrogen is in ionised water. Ionised water is infused with free molecular  hydrogen that is produced by a water ioniser machine, which splits the water in two streams, alkaline and acidic. The alkaline water is packed with free hydrogen and so is therapeutic for that reason as well as the fact of being an alkaline pH.'
            },
            {
                format: 'heading',
                content:
                    "Molecular hydrogen's ability to scavenge free radicals anywhere in the body"
            },
            {
                format: 'text',
                content:
                    'Molecular Hydrogen is known by many prominent researchers world-wide for its ability to truly penetrate through cell membranes (even the blood-brain barrier) and scavenge free radicals.'
            },
            {
                format: 'text',
                content:
                    'One reason Hydrogen is not like any other antioxidant is due to its tiny size. Hydrogen is also lipid soluble, which is important, because lipid solubility is necessary for entering into cells through the cell membranes. So, Hydrogen (unlike any other antioxidant) can get into every living part of the body. All other antioxidants are incapable of this feat. Another thing that makes it different from all other free-radical scavengers is that it simply evaporates or turns into water when it\'s "used up", so it creates no danger of becoming a free- radical itself as other antioxidants do.'
            },
            {
                format: 'heading',
                content: 'Free molecular hydrogen - safest antioxidant available'
            },
            {
                format: 'text',
                content:
                    "As we all know, the combination of H2 and Oxygen makes water, which is at the source of all life forms. Oxygen also brings life, but unfortunately complications arise when there is too much oxygen within the body and there is not enough balance of hydrogen. Oxygen can actually become a dangerous free radical within the body when it has lost its electron balance. When it's in this state it's called reactive Oxygen species which causes severe damage to cells,This condition is called Oxidative Stress, and is thought to be at the root of almost all disease."
            },
            {
                format: 'text',
                content:
                    'a tiny little molecular hydrogen can calm it down completely; neutralise it; turn it into harmless water and make it useful for the body. This is done with no harmful waste or byproducts produced.'
            },
            {
                format: 'text',
                content:
                    "At the same time molecular hydrogen doesn't attack good radicals in the body.It will never act as a poison to the body, and any excess of it will simply evaporate harmlessly and combine with oxygen in the air to hydrate or moisturise it."
            },
            {
                format: 'text',
                content:
                    "Other types of antioxidants have been found to become free-radicals, themselves, once they have donated their electron to neutralise another molecule. This doesn't happen with molecular hydrogen.As a matter of fact molecular hydrogen is able to refresh the antioxidant abilities of various other antioxidants to keep them from becoming free- radicals.After much testing and scrutinising by research scientists worldwide, no negative effects whatsoever have been found to be caused by molecular hydrogen."
            }
        ]
    },
    {
        title: 'Fighting free-radicals and heart disease',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/Fighting free-radicals and heart disease.png',
                alt: 'HEART DISEASE'
            },
            {
                format: 'text',
                content:
                    'Dr. Shigeo Ohta (of Japan) specifically devoted many years of research on the free radical benefits of ionised water and hydrogen water. Free-hydrogen (molecular hydrogen) was shown to effectively neutralise free-radicals, without any of the negative side-effects often seen with other antioxidants. This is important. Fighting free-radicals while battling (or preventing) cancer, heart disease or 170 other diseases is a huge benefit that alkaline ionised water and hydrogen water both offer. In this video Dr. Ohta, is being interviewed by the CEO of the Molecular Hydrogen Foundation. They talk about the latest research into Molecular Hydrogen and the benefits of hydrogen water and hydrogen gas.'
            },
            {
                format: 'video',
                videoId: '1Up2i35MLdw'
            },
            {
                format: 'text',
                content:
                    'Here is another video where Tyler LeBaron of the Molecular Hydrogen Foundation explains this research in more detail, using charts and peer reviewed reports to clarify his statements.'
            },
            {
                format: 'video',
                videoId: '5a6MIISq2nw'
            },
            {
                format: 'heading',
                content:
                    'Here is a partial list of diseases hydrogen antioxidants (molecular hydrogen) have been shown to help.'
            },
            {
                format: 'badge',
                content: [
                    'Alexander disease',
                    'Allergies',
                    "Alzheimer's disease and other dementias",
                    'Angina',
                    'Asthma',
                    'Atherosclerosis',
                    "Athlete's foot",
                    'Atopic dermatitis',
                    'Bed sores',
                    'Bone Loss',
                    'Cancer (various types)',
                    'Cardio renal symptoms',
                    'Cataracts',
                    'Cerebral haemorrhage',
                    "Chemo's negative side effects",
                    "Crohn's disease",
                    'Chronic fatigue syndrome',
                    'Cirrhosis',
                    'Collagen disease',
                    'Colon diseases',
                    'Cystic fibrosis',
                    'Dehydration',
                    'Diabetes mellitus',
                    'Diabetic necrosis',
                    'Frontotemporal lobar degeneration',
                    'Glaucoma',
                    'Gout',
                    'Heart attacks',
                    'Heart disease or coronary artery disease',
                    'HGC disorders',
                    'High blood pressure',
                    "Huntington's disease",
                    'Hypersensitive disorders',
                    'Hypertension',
                    'Inflammatory disorders',
                    'Ischemic brain injuries',
                    "Kawasaki's disease",
                    'Kidney disease',
                    'Liver disease',
                    'Lupus',
                    'Malaise',
                    'Metabolic syndrome',
                    'Muscle fatigue of athletes'
                ]
            }
        ]
    },
    {
        title: 'Hydrogen Research on Cancer',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/Hydrogen Research on Cancer.png',
                alt: 'CANCER CELLS AND WATER'
            },
            {
                format: 'text',
                content:
                    'The articles in this section show that hydrogenated ionised water inhibits cancer progression and improves the prognosis of cancer patients.'
            },
            {
                format: 'text',
                content:
                    'Alkaline ionised water has been proven to help alkalize the body, which is essential in cancer prevention. It helps us to maintain the proper pH balance within cells and organs and so is protective of our cells and a preventative measure against cancer.'
            },
            {
                format: 'text',
                content:
                    'Hydrogen water research has also shown many cancer prevention and healing benefits for patients being treated with cancer as well.'
            },
            {
                format: 'text',
                content:
                    "This short, 2-minute video, summarises what researchers have worked years to learn about keeping a healthy, disease-free colon, as well as other organs and cells. Colorectal cancer is one of the most dangerous, common and horrible-to-live-with cancers. Don't wait to be diagnosed with colorectal cancer. It's very common in the US to get this disease, and it can be prevented."
            },
            {
                format: 'video',
                videoId: 'SCEN-6Q4Hyk'
            }
        ]
    },
    {
        title: 'Hydrogen Research on Diabetes',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/Hydrogen Research on Diabetes.png',
                alt: 'CANCER CELLS AND WATER'
            },
            {
                format: 'text',
                content:
                    'These studies show that hydrogen-water helps improve blood glucose control,'
            },
            {
                format: 'text',
                content:
                    'The following video presentation is by Mr. Darnell of the "Diabetes Support Services" organisation. In this video you can get a history of water ioniser research and how using ionised water has helped his group members. The history of water ionisers all started by people noticing there were pockets of "healing waters" around the world, and then scientists tested those waters and developed equipment (water ionisers) to simulate the healing waters. Mr. Darnell learned about these waters and this equipment, water ionisers, in a roundabout way, and then after fully studying the subject and equipment, he made recommendations to his group to use ionised water with the group\'s diabetic members. He reports that ionised water has been very effective in helping the hundreds of diabetic patients in his organisation live better lives, free of many of the complications associated with diabetes. This next section (after the video) is all about the interesting history of these machines.'
            },
            {
                format: 'video',
                videoId: 'P1ThW-tq308'
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Ionised Hydrogen Water / Inhalation On the Heart',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE HEART-01.png',
                alt: 'POSITIVE EFFECTS ON THE HEART'
            },
            {
                format: 'table',
                content: [
                    {
                        year: 'Spanning over 20 years worth of treating patients with ionised water',
                        subject: 'Blood Pressure',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Use of Ionized water in hypochlorhydria or achlorhydria Prof. Kuninaka Hironage, Head of Kuninaka Hospital.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28321000/'
                            },
                            {
                                format: 'text',
                                content:
                                    "The consumption of alkaline antioxidant water for a period of 2 to 3 months, I have observed the blood pressure slowly drop, due to the water's solvent ability, which dissolves the cholesterol in the blood vessels ."
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Myocardial Infarction',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'The Effects of Hydrogen Gas Inhalation on Adverse Left Ventricular Remodelling After Percutaneous Coronary Intervention for ST-Elevated Myocardial Infarction..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28321000/'
                            },
                            {
                                format: 'text',
                                content:
                                    '- First Pilot Study in Humans. [Human study shows inhaling hydrogen is safe and improves the markers of hypertension.]'
                            }
                        ]
                    },
                    {
                        year: '2019',
                        subject: 'Hypertension',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen gas improves left ventricular hypertrophy in Dahl rats of salt-sensitive hypertension..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29902079/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen reduces hypertension and left ventricular hypertrophy.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogenated Ionized Water on Cancer',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE CANCER-01.png',
                alt: 'POSITIVE EFFECTS ON CANCER'
            },
            {
                format: 'text',
                content: '# 2 Leading cause of death is Cancer'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2019',
                        subject: 'Colorectal cancer',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen gas restores exhausted CD8+ T cells in patients with advanced colorectal cancer.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/30542740/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Human study shows hydrogen reverses imbalances caused by chemotherapy and improved prognosis of stage IV cancer patients.]'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Colorectal Cancer [LIVER]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective effect of hydrogen-rich water on liver function of colorectal cancer patients treated with mFOLFOX6 chemotherapy.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29142752/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[A controlled, randomised, single-blind human clinical study shows that hydrogen water alleviates liver injury due to chemotherapy.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Lung Cancer',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen gas inhibits lung cancer progression through targeting SMC3.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29852353/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen inhibits lung cancer progression.]'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Colon Cancer Inhibition',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-water enhances 5-fluorouracil-induced inhibition of colon cancer.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/25870767/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen improves the survival rate of mice with colon cancer and has anticancer functions, particularly in combination with 5-fluorouracil.]'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: "Mitochondria's huge Influence on Cancer",
                        description: [
                            {
                                format: 'text',
                                content: 'Mitochondria and Cancer, Past, Present and Future..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3581248/'
                            },
                            {
                                format: 'text',
                                content:
                                    "[A summary of the research on mitochondria's influence on cancer."
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Alleviates Cancer Drug Side-Effects',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen alleviates nephrotoxicity induced by an anti-cancer drug cisplatin without compromising anti-tumor activity in mice.. ',
                                linkToText:
                                    'https://link.springer.com/article/10.1007/s00280-008-0924-2#page-1'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen helps protect against cisplatin-induced toxicity showing it has potential for improving the quality of life of patients undergoing chemotherapy.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Ionized Hydrogen Water on Chronic Lower Respiratory Disease',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE CHRONIC LOWER RESPIRATORY-01.png',
                alt: 'POSITIVE EFFECTS ON CHRONIC LOWER RESPIRATORY DISEASE'
            },
            {
                format: 'text',
                content:
                    '# 3 Leading cause of Death is Chronic lower respiratory disease'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2017',
                        subject: 'Anti-Inflammatory [RHINITIS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effect of hydrogen-rich saline on the CD4(+) CD25(+) Foxp3(+) Treg cells of allergic rhinitis guinea pigs model.. ',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/28728239'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen reduces allergic inflammation.]'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Anti-Inflammatory [Lung Injury from O2 THERAPY]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen alleviates hyperoxic acute lung injury related endoplasmic reticulum stress in rats through upregulation of SIRT1.. ',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen+alleviates+hyperoxic+acute+lung+injury+related+endoplasmic+reticulum+stress+in+rats+through+upregulation+of+SIRT1'
                            },
                            {
                                format: 'text',
                                content:
                                    '[A controlled, randomised, single-blind human clinical study shows that hydrogen water alleviates liver injury due to chemotherapy.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'Anti-Inflammatory [EXERCISE]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water inhibits mitochondrial oxidative stress and inflammation in the skeletal muscle after eccentric exercise.. ',
                                linkToText:
                                    'http://wprim.whocc.org.cn/admin/article/articleDetail?WPRIMID=468363&articleId=468363&locale=zh_CN'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows [Animal study shows hydrogen reduces inflammation after exercise.]'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Immune System',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'The immunological effects of electrolyzed reduced water on the Echinostoma hortense infection in C57BL/6 mice.. ',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19252295'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water assists immune responsiveness.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogen Water and Ionized Water on Stroke Victims',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE Stroke Victims-01.png',
                alt: 'POSITIVE EFFECTS ON STROKE VICTIMS'
            },
            {
                format: 'text',
                content: '# 4 Leading cause of death is stroke'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2017',
                        subject: 'Cerebral Infarction (Acute)',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen Gas Inhalation Treatment in Acute Cerebral Infarction: A Randomised Controlled Clinical Study on Safety and Neuroprotection..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28669654/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[A randomised controlled human clinical study of patients with acute cerebral infarctions showed no harmful effects and better recovery rates.]'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Neuroblastoma',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen protects against oxidative stress-induced SH-SY5Y neuroblastoma cell death through the process of mitohormesis..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/28467497/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[A human cultured neuroblastoma, pre-treated with hydrogen, was protected against H2O2 induced cell death.]'
                            }
                        ]
                    },
                    {
                        year: '2019',
                        subject: 'Glioblastoma',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen suppresses glioblastoma growth via inducing the glioma stem-like cell differentiation..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/31113492/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows reduction in common brain tumours by inhaling hydrogen.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Traumatic Brain Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water attenuates oxidative stress in rats with traumatic brain injury via Nrf2 pathway..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29907217/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen-rich water has a neuroprotective effect on the brain.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Mitochondria',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Inhalation of Hydrogen of Different Concentrations Ameliorates Spinal Cord Injury in Mice by Protecting Spinal Cord Neurons from Apoptosis, Oxidative Injury and Mitochondrial Structure Damages.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29763919/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows the neuroprotective effect of hydrogen on the neurons in the spinal cord. Hydrogen was also seen to help the neurons and their mitochondria to heal after damage.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'After stroke',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen exerts neuroprotective effects on OGD/R damaged neurons in rat hippocampal by protecting mitochondrial function via regulating mitophagy mediated by PINK1/Parkin signalling pathway.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29958907/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study showing the neuroprotective effect of hydrogen.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Stroke',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen inhalation improves mouse neurological outcomes after cerebral ischemia/reperfusion independent of anti-necroptosis.. ',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29770189/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study showing that hydrogen helps improve recovery after stroke.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Severe Brain Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Neuroprotective effects of hydrogen inhalation in an experimental rat intracerebral haemorrhage model..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/30016724/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study showing that hydrogen has a neuroprotective effect on the brain and helps prevent or reduce the effects of brain injury.]'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Brain injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effect of hydrogen-rich water on the angiogenesis in lesion boundary brain tissue of traumatic brain..',
                                linkToText: 'https://e-century.us/web/journal.php?journal=ijcep'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen-rich water promotes angiogenesis and improves nerve function.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'Neuroprotective',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen has a neuroprotective effect via activation of Nrf-2/HO-1 pathway in ischemia reperfusion rat..',
                                linkToText: 'https://e-century.us/web/journal.php?journal=ijcem'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows that hydrogen has a neuroprotective effect against stroke.]'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Brain damage',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water attenuates brain damage and inflammation after traumatic brain injury in rats..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/26826009/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen reduces the effects of brain damage and prevents inflammation.]'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Brain infarction',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Inhalation of water electrolysis-derived hydrogen ameliorates cerebral ischemia-reperfusion injury in rats - A possible new hydrogen resource for clinical use..',
                                linkToText:
                                    'https://pubmed.ncbi.nlm.nih.gov/?term=.%2C+Inhalation+of+water+electrolysis-derived+hydrogen+ameliorates+cerebral+ischemia-reperfusion+injury+in+rats+-+A+possible+new+hydrogen+resource+for+clinical+use'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen has a neuroprotective effect against cerebral damage.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'Stroke',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen improves neurological function through attenuation of blood-brain barrier disruption in spontaneously hypertensive stroke-prone rats..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4411925/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water can improve neurological function after a stroke.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'Protects Against Stroke',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water protects against ischemic brain injury in rats by regulating calcium buffering proteins..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/25920370/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water helps to reduce brain infarct volume and improve neurological function following an induced brain injury.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            "Research Articles on the Positive Effects of Ionized Hydrogen Water / inhalation on Alzheimer's , Multiple sclerosis & Parkinson's",
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE Alzheimer\'s-01.png',
                alt: "POSITIVE EFFECTS ON ALZHEIMER'S, MULTIPLE SCLEROSIS & PARKINSON'S"
            },
            {
                format: 'text',
                content: "# 5 Leading Cause of death is Alzheimer's Disase"
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2013',
                        subject: "Parkinson's",
                        description: [
                            {
                                format: 'text',
                                content:
                                    "Pilot study of H2 therapy in Parkinson's disease: A randomised double‐blind placebo‐controlled trial..",
                                linkToText:
                                    'https://movementdisorders.onlinelibrary.wiley.com/doi/abs/10.1002/mds.25375?deniedAccessCustomisedMessage=&userIsAuthenticated=false'
                            },
                            {
                                format: 'text',
                                content:
                                    "[A human controlled trial shows that drinking Hydrogen water was safe, and well tolerated. The patients who drank it showed significant improvement, while the control group who didn't drink the hydrogen water worsened.]"
                            }
                        ]
                    },
                    {
                        year: '2019',
                        subject: 'Vascular Dementia',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'FoxO1-mediated autophagy plays an important role in the neuroprotective effects of hydrogen in a rat model of vascular dementia..',
                                linkToText: 'https://pubmed.ncbi.nlm.nih.gov/29885845/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows drinking hydrogen water inhibits neuron loss and alleviates memory impairments due to vascular dementia.]'
                            }
                        ]
                    },
                    {
                        year: '2019',
                        subject: 'Glioblastoma',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen suppresses glioblastoma growth via inducing the glioma stem-like cell differentiation.n..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/31113492'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows reduction in common brain tumors by inhaling hydrogen.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Mitochondria',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Inhalation of Hydrogen of Different Concentrations Ameliorates Spinal Cord Injury in Mice by Protecting Spinal Cord Neurons from Apoptosis, Oxidative Injury and Mitochondrial Structure Damages..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/29763919'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows the neuroprotective effect of hydrogen on the neurons in the spinal cord. Hydrogen was also seen to help the neurons and their mitochondria to heal after damage.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Mild Cognitive Impairment',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of Molecular Hydrogen Assessed by an Animal Model and a Randomised Clinical Study on Mild Cognitive Impairment.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Effects+of+Molecular+Hydrogen+Assessed+by+an+Animal+Model+and+a+Randomized+Clinical+Study+on+Mild+Cognitive+Impairment'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study showing that hydrogen water helps to suppress dementia and other oxidative stress.]'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Cognitive Impairment',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water improves cognitive impairment gender-dependently in APP/PS1 mice without affecting Aβ clearance..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen-rich+water+improves+cognitive+impairment+gender-dependently+in+APP%2FPS1+mice+without+affecting+Abeta+clearance'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study showing that hydrogen significantly helps cognitive behavior and reduces oxidative stress and inflammation in the brain. This study noted that there was an even more profound effect in the female brains than male.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'Multiple Sclerosis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water improves neurological functional recovery in experimental autoimmune encephalomyelitis mice..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen-rich+water+improves+neurological+functional+recovery+in+experimental+autoimmune+encephalomyelitis+mice'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water helps to both delay the onset of and reduce the severity of induced multiple sclerosis.]'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: "Alzheimer's",
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water attenuates amyloid β-induced cytotoxicity through upregulation of Sirt1-FoxO3a by stimulation of AMP-activated protein kinase in SK-N-MC cells..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen-rich+water+attenuates+amyloid+beta-induced+cytotoxicity+through+upregulation+of+Sirt1-FoxO3a+by+stimulation+of+AMP-activated+protein+kinase+in+SK-N-MC+cells'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water reduces reactive oxygen-induced cell death and stimulates healing.]'
                            }
                        ]
                    },
                    {
                        year: '2014',
                        subject: 'Age-related Neurological Diseases',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Electrochemically Reduced Water Protects Neural Cells from Oxidative Damage..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4212634/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen water protects cells from oxidative stresses.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of ionized Hydrogen Water on Diabetes',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE Diabetes-01.png',
                alt: 'POSITIVE EFFECTS ON DIABETES'
            },
            {
                format: 'text',
                content: '# 6 Leading Cause of death is Diabetes'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2008',
                        subject: 'Diabetes [GLUCOSE TOLERANCE]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Supplementation of hydrogen-rich water improves lipid and glucose metabolism in patients with type 2 diabetes or impaired glucose tolerance..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19083400/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[A randomized double-blind human study shows hydrogen helped decrease oxidative stress and insulin resistance and improved glucose metabolism.]'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: 'Diabetes [GLUCOSE UPTAKE]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen Improves Glycemic Control in Type1 Diabetic Animal Model by Promoting Glucose Uptake into Skeletal Muscle..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3542317/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen improves glucose control.]'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Diabetes [Pt NANOPARTICLES]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Anti-diabetes effect of water containing hydrogen molecule and Pt nanoparticles..',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3284969/'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows both hydrogen water and the nano platinum in ionized water improve glucose signaling pathways.]'
                            }
                        ]
                    },
                    {
                        year: '2006',
                        subject: 'Diabetes [ANTI-DIABETIC EFFECTS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Anti-diabetic effects of electrolyzed reduced water in streptozotocin-induced and genetic diabetic mice..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/16945392'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen improves blood glucose control.]'
                            }
                        ]
                    },
                    {
                        year: '2006',
                        subject: 'Diabetes [BETA CELL PROTECTION]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Preservative effect of electrolyzed reduced water on pancreatic beta-cell mass in diabetic db/db mice..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/17268057'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen improves blood glucose tolerance, increases blood insulin and reduces blood glucose.]'
                            }
                        ]
                    },
                    {
                        year: '2005',
                        subject: 'Diabetes [BETA CELL PROTECTION]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective mechanism of reduced water against alloxan-induced pancreatic beta-cell damage: Scavenging effect against reactive oxygen species..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19003114'
                            },
                            {
                                format: 'text',
                                content:
                                    '[Animal study shows hydrogen enhances glucose-sensitivity, glucose response of beta-cells and protects pancreatic beta-cells from alloxan-induced cell damage.]'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogen Water and Ionized Water on the Kidneys',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE kidney-01.png',
                alt: 'POSITIVE EFFECTS ON DIABETES'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2018',
                        subject: 'Dialysis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Novel hemodialysis (HD) treatment employing molecular hydrogen (H2)-enriched dialysis solution improves prognosis of chronic dialysis patients: A prospective observational study.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5762770/'
                            },
                            {
                                format: 'text',
                                content:
                                    "This is a human study spanning 3 years and shows that adding hydrogen to the dialysis solution benefits dialysis patients' prognosis."
                            }
                        ]
                    },
                    {
                        year: '2003',
                        subject: 'Kidney Disease',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Reduced hemodialysis-induced oxidative stress in end-stage renal disease patients by electrolyzed reduced water.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/12846769'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows that ionized water is effective in reducing oxidative stress and improving overall health of patients with kidney disease. This is important because hemodialysis increases oxidative stress and being able to combat that would naturally improve the patient prognosis.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Toxicity',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Nephroprotective effect of electrolyzed reduced water against cisplatin-induced kidney toxicity and oxidative damage in mice.',
                                linkToText:
                                    'https://www.sciencedirect.com/science/article/pii/S1726490117302617'
                            },
                            {
                                format: 'text',
                                content:
                                    "Animal study shows ionized water helps to protect the kidneys against damage when 'Cisplatin' (a chemotherapy drug) is given."
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Renal Injury & Fibrosis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen Rich Water Attenuates Renal Injury and Fibrosis by Regulation Transforming Growth Factor.',
                                linkToText:
                                    'https://www.jstage.jst.go.jp/article/bpb/40/5/40_b16-00832/_article/-char/ja/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen water helps to inhibit the development of fibrosis in kidneys.'
                            }
                        ]
                    },
                    {
                        year: '2010',
                        subject: 'Renal Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Intake of water with high levels of dissolved hydrogen (H2) suppresses ischemia-induced cardio-renal injury in Dahl salt-sensitive rats.',
                                linkToText:
                                    'https://academic.oup.com/ndt/article/26/7/2112/1895116'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen water helped healing after a kidney injury.'
                            }
                        ]
                    },
                    {
                        year: '2014',
                        subject: 'Renal Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Consumption of hydrogen-rich water alleviates renal injury in spontaneous hypertensive rats.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/24652103'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen water helps to alleviate renal injury.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Ionized Hydrogen Water on the Liver',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE liver-01.png',
                alt: 'POSITIVE EFFECTS ON LIVER'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2017',
                        subject: 'Chemo: Protects Liver',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective effect of hydrogen-rich water on liver function of colorectal cancer patients treated with mFOLFOX6 chemotherapy.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=.%2C+Protective+effect+of+hydrogen-rich+water+on+liver+function+of+colorectal+cancer+patients+treated+with+mFOLFOX6+chemotherapy'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows that H2 helps to maintain microcirculation and the health of the mitochondria in patients being treated with chemotherapy.'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Radiotherapy for liver tumours',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of drinking hydrogen-rich water on the quality of life of patients treated with radiotherapy for liver tumours.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/22146004'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows that drinking hydrogen-rich water reduces the stress markers connected with radiation therapy.'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: 'Chronic Hepatitis B',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effect of Hydrogen‐Rich Water on Oxidative Stress, Liver Function, and Viral Load in Patients with Chronic Hepatitis B.',
                                linkToText:
                                    'https://ascpt.onlinelibrary.wiley.com/doi/full/10.1111/cts.12076'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study showing that hydrogen-rich water helps to alleviate oxidative stress in patients with chronic Hepatitis B.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Liver Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Post-reperfusion hydrogen gas treatment ameliorates ischemia reperfusion injury in rat livers from donors after cardiac death: a preliminary study.',
                                linkToText:
                                    'https://link.springer.com/article/10.1007/s00595-018-1693-0'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows H2 to be effective in maintaining microcirculation and mitochondrial function in liver cells and suppressing cellular death in the liver after cardiac death.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Renal Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of molecular hydrogen-dissolved alkaline electrolyzed water on intestinal environment in mice.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5937304/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows alkaline ionized water assists the intestinal environment and has beneficial roles for health in terms of cholesterol metabolism and liver protection.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Liver Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Inhalation of high concentrations of hydrogen ameliorates liver ischemia/reperfusion injury through A2A receptor mediated PI3K-Akt pathway.',
                                linkToText:
                                    'https://www.sciencedirect.com/science/article/abs/pii/S0006295217300655'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows that a high concentration of hydrogen improves liver function and may protect the liver against I/R injury, which is the injury when blood supply returns to tissue after a period of absence.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Protect Against SepsisLiver Injury',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Pre-administration of Hydrogen-Rich Water Protects Against Lipopolysaccharide-Induced Sepsis and Attenuates Liver Injury.',
                                linkToText:
                                    'https://journals.lww.com/shockjournal/Fulltext/2017/07000/Preadministration_of_Hydrogen_Rich_Water_Protects.12.aspx'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows drinking hydrogen water prolongs life and reduces cell death and oxidative stress in mice with sepsis (a life-threatening response to infection).'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Liver',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hepatoprotective effect of electrolyzed reduced water against carbon tetrachloride-induced liver damage in mice.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19477216'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows ionized water protects against liver damage.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Ionized Hydrogen Water on Cholesterol',
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE cholestrol-01.png',
                alt: 'POSITIVE EFFECTS ON CHOLESTEROL'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2013',
                        subject: 'Cholesterol level Balancing',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water decreases serum LDL-cholesterol levels and improves HDL function in patients with potential metabolic syndrome.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3679390/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows drinking ionised water helps cholesterol levels.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Liver & Cholesterol',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of molecular hydrogen-dissolved alkaline electrolyzed water on intestinal environment in mice.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5937304/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows drinking ionised water helps intestinal fermentation.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Ionized Hydrogen Water on the Intestines & Stomach',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE stomach-01.png',
                alt: 'POSITIVE EFFECTS ON STOMACH'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2019',
                        subject: 'Gut Flora',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of the long-term consumption of hydrogen-rich water on the antioxidant activity and the gut flora in female juvenile soccer players from Suzhou, China.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6352569/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows hydrogen-rich water was healthy for gut flora.'
                            }
                        ]
                    },
                    {
                        year: '2019',
                        subject: 'Gastrointestinal Symptoms',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Daily ingestion of alkaline electrolyzed water containing hydrogen influences human health, including gastrointestinal symptoms.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6352572/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows alkaline ionised water is effective in relieving abdominal complaints. Subjects drinking ionised water also had better rest at night and woke in the morning with more energy.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Irritable Bowel Syndrome with Diarrhoea',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of Alkaline-Reduced Drinking Water on Irritable Bowel Syndrome with Diarrhoea: A Randomised Double-Blind, Placebo-Controlled Pilot Study.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5925025/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows alkaline ionised water was helpful in improving the symptoms of diarrhoea and irritable bowel syndrome.'
                            }
                        ]
                    },
                    {
                        year: '2005',
                        subject: 'Intestinal Health',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Selective stimulation of the growth of anaerobic microflora in the human intestinal tract by electrolyzed reduced water.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/15617863'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows that alkaline ionised water assists to properly balance the gut microflora.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Protects-Good Bacteria',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Intestinal Microbiota Ecological Response to Oral Administrations of Hydrogen-Rich Water and Lactulose in Female Piglets Fed a Fusarium Toxin-Contaminated Diet.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6024725/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows that hydrogen-water has shown a protective effect on the balance of intestinal microbiota.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Intestinal Environment',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of molecular hydrogen-dissolved alkaline electrolyzed water on intestinal environment in mice.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5937304/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows alkaline ionised water has benefit to intestinal microbes.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'GI Toxicity',
                        description: [
                            {
                                format: 'text',
                                content:
                                    "Hydrogen-water ameliorates radiation-induced gastrointestinal toxicity via MyD88's effects on the gut microbiota.",
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5799803/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows that drinking hydrogen-rich water improved the prognosis of mice undergoing radiation and was protective of the intestines.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Inflammatory Bowel Disease',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water protects against inflammatory bowel disease in mice by inhibiting endoplasmic reticulum stress and promoting heme oxygenase-1 expression.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5330822/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen-rich water protects against inflammatory bowel disease.'
                            }
                        ]
                    },
                    {
                        year: '1996',
                        subject: 'Intestinal Health',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Physiological effects of alkaline ionised water: Effects on metabolites produced by intestinal fermentation.',
                                linkToText:
                                    'http://www.ionza.co.nz/content/Intestinal-fermentation-Alkaline_Water.pdf'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows alkaline ionised water was healthy for the intestines.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogen Water and Ionized Water on the Skin',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE skin-01.png',
                alt: 'POSITIVE EFFECTS ON SKIN'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2018',
                        subject: 'Psoriasis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Positive effects of hydrogen-water bathing in patients of psoriasis and parapsoriasis en plaques.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5966409/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows hydrogen water bathing is beneficial to psoriasis.'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: 'Skin Ulcers',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen water intake via tube-feeding for patients with pressure ulcer and its reconstructive effects on normal human skin cells in vitro.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3843550/'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows intake of hydrogen water helps the reconstruction of normal skin cells.'
                            }
                        ]
                    },
                    {
                        year: '2012',
                        subject: 'Skin Diseases',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen(H2) treatment for acute erythematous skin diseases. A report of 4 patients with safety data and a non-controlled feasibility study with H2 concentration measurement on two volunteers.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/22607973'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows hydrogen helped with the prognosis of inflammation of the skin.'
                            }
                        ]
                    },
                    {
                        year: '2012',
                        subject: 'Wrinkles',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich electrolyzed warm water represses wrinkle formation against UVA ray together with type-I collagen production and oxidative-stress diminishment in fibroblasts and cell-injury prevention in keratinocytes.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/22070900'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human study which shows hydrogen water bathing helps to repress UVA-induced wrinkles.'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: 'Dermatitis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'The Drinking Effect of Hydrogen Water on Atopic Dermatitis Induced by Dermatophagoides farinae Allergen in NC/Nga Mice.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/24348704'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen water has a positive effect on atopic dermatitis.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogen Water on Inflammation',
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE inflammation-01.png',
                alt: 'POSITIVE EFFECTS ON INFLAMMATION'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2014',
                        subject: 'Sports-Related Soft Tissue Injuries',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effectiveness of oral and topical hydrogen for sports-related soft tissue injuries.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/25295663'
                            },
                            {
                                format: 'text',
                                content:
                                    'Human study of professional athletes shows faster recovery times when injured for those who were given hydrogen supplements daily during the season.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'anti-inflammatory [RHINITIS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effect of hydrogen-rich saline on the CD4(+) CD25(+) Foxp3(+) Treg cells of allergic rhinitis guinea pigs model.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/28728239'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen reduces allergic inflammation.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'anti-inflammatory [PANCREATITIS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen Treatment Protects Mice Against Chronic Pancreatitis by Restoring Regulatory T Cells Loss.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/29237160'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helped to prevent and reduce pancreatitis.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'anti-inflammatory [Lung Injury from O2 THERAPY]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen alleviates hyperoxic acute lung injury related endoplasmic reticulum stress in rats through upregulation of SIRT1.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen+alleviates+hyperoxic+acute+lung+injury+related+endoplasmic+reticulum+stress+in+rats+through+upregulation+of+SIRT1'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows that hydrogen has a protective effect against oxygen-induced lung injury and reduces edema.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'anti-inflammatory [PANCREATITIS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective Effects of Hydrogen Gas on Experimental Acute Pancreatitis.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4845997/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen has a protective effect against oxidative damage and reduces inflammation.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'anti-inflammatory [ARTHRITIS]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen decelerates rheumatoid arthritis progression through inhibition of oxidative stress.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Molecular+hydrogen+decelerates+rheumatoid+arthritis+progression+through+inhibition+of+oxidative+stress'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen slows down the progression of rheumatoid arthritis.'
                            }
                        ]
                    },
                    {
                        year: '2015',
                        subject: 'anti-inflammatory [EXERCISE]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen-rich water inhibits mitochondrial oxidative stress and inflammation in the skeletal muscle after eccentric exercise.',
                                linkToText:
                                    'http://wprim.whocc.org.cn/admin/article/articleDetail?WPRIMID=468363&amp;articleId=468363&amp;locale=zh_CN'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen reduces inflammation after exercise.'
                            }
                        ]
                    },
                    {
                        year: '2001',
                        subject: 'anti-inflammatory [LIVER]',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Anti-inflammatory properties of molecular hydrogen: investigation on parasite-induced liver inflammation.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/11510417'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen has an anti-inflammatory effect.'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'aImmune System',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'The immunological effects of electrolyzed reduced water on the Echinostoma hortense infection in C57BL/6 mice.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19252295'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen water assists immune responsiveness.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles on the Positive Effects of Hydrogen Water on Free-Radicals',
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE free radicals-01.png',
                alt: 'POSITIVE EFFECTS ON FREE RADICALS'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2012',
                        subject: 'Platelets',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen may inhibit collagen-induced platelet aggregation: an ex vivo and in vivo study.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/22687834'
                            },
                            {
                                format: 'text',
                                content:
                                    'This is a human and animal study which shows hydrogen may inhibit collagen-induced platelet aggregation.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Antioxidant',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of Hydrogenated Water on Intracellular Biomarkers for Antioxidants, Glucose Uptake, Insulin Signalling and SIRT 1 and telomerase Activity.',
                                linkToText: 'http://pubs.sciepub.com/ajfn/4/6/4/'
                            },
                            {
                                format: 'text',
                                content:
                                    'In vitro study shows that commercial hydrogen water is effective in antioxidant properties.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Bone-Health',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective effects of molecular hydrogen on steroid-induced osteonecrosis in rabbits via reducing oxidative stress and apoptosis.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Protective+effects+of+molecular+hydrogen+on+steroid-induced+osteonecrosis+in+rabbits+via+reducing+oxidative+stress+and+apoptosis'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen prevents steroid-induced osteonecrosis.'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Eyes - Cornea',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen and N-acetyl-L-cysteine rescue oxidative stress-induced angiogenesis in a mouse corneal alkali-burn model.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/20847117'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helps prevent blindness caused by alkali burn.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Obesity',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Preventive Effects of Drinking Hydrogen-Rich Water on Gingival Oxidative Stress and Alveolar Bone Resorption in Rats Fed a High-Fat Diet.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Preventive+Effects+of+Drinking+Hydrogen-Rich+Water+on+Gingival+Oxidative+Stress+and+Alveolar+Bone+Resorption+in+Rats+Fed+a+High-Fat+Diet'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen-rich water suppressed body weight gain and reduced oxidative stress.'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Obesity & Diabetes',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen improves obesity and diabetes by inducing hepatic FGF21 and stimulating energy metabolism in db/db mice.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/21293445'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen stimulated energy metabolism, improving obesity, diabetes, and metabolic syndrome.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title:
            'Research Articles Related to the Positive Effects of Ionized Hydrogen Water on Aging',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE Aging-01.png',
                alt: 'POSITIVE EFFECTS ON AGING'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2018',
                        subject: 'Reduction of Senescence-Associated Cells',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen alleviates cellular senescence via regulation of ROS/p53/p21 pathway in bone marrow-derived mesenchymal stem cells in vivo.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/30119179'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen is a protective anti-aging treatment.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Anti-Aging & Cell Death',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen Treatment Protects against Cell Death and Senescence Induced by Oxidative Damage.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/27780950'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen protects against aging and cell death.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Longevity',
                        description: [
                            {
                                format: 'text',
                                content: 'Alkaline Water and Longevity.',
                                linkToText:
                                    'https://www.hindawi.com/journals/ecam/2016/3084126/'
                            },
                            {
                                format: 'text',
                                content:
                                    '3-year survival study shows alkaline ionized water improves strength and longevity of mice.'
                            }
                        ]
                    },
                    {
                        year: '2010',
                        subject: 'Anti-Aging; Bones',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen gas treatment prolongs replicative lifespan of bone marrow multipotential stromal cells in vitro while preserving differentiation and paracrine potentials.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/20570654'
                            },
                            {
                                format: 'text',
                                content:
                                    'In vitro study shows hydrogen prolongs the lifespan of cells.'
                            }
                        ]
                    },
                    {
                        year: '2013',
                        subject: 'Increased Lifespan',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Electrolyzed-reduced water increases resistance to oxidative stress, fertility, and lifespan via insulin/IGF-1-like signal in C. elegans.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/23959012'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helps to resist oxidative stress and aging.'
                            }
                        ]
                    },
                    {
                        year: '1997',
                        subject: 'Protects DNA',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Electrolyzed-reduced water scavenges active oxygen species and protects DNA from oxidative damage.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/9169001'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helps to protect DNA and scavenges harmful radicals.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title: 'Research articles on Ionized hydrogen water on eyes',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE water on eyes-01.png',
                alt: 'POSITIVE EFFECTS ON WATER ON EYES'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2019',
                        subject: 'Retinal Vein Occlusion',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protective effects of hydrogen gas in a rat model of branch retinal vein occlusion via decreasing VEGF-α expression.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/31096936'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helps alleviate retinal edema and improve its function.'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Cornea',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen and N-acetyl-L-cysteine rescue oxidative stress-induced angiogenesis in a mouse corneal alkali-burn model.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen+and+N-acetyl-L-cysteine+rescue+oxidative+stress-induced+angiogenesis+in+a+mouse+corneal+alkali-burn+model.'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen helps prevent blindness from alkali burn.'
                            }
                        ]
                    },
                    {
                        year: '2010',
                        subject: 'Retina',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Protection of the retina by rapid diffusion of hydrogen: administration of hydrogen-loaded eye drops in retinal ischemia-reperfusion injury. Oharazawa H1, Igarashi T, Yokota T..',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19834032'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen-loaded eye drops are highly useful and neuroprotective.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title: 'Research articles on many other pathologies',
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE pathologies-01.png',
                alt: 'POSITIVE EFFECTS ON PATHOLOGIES'
            },
            {
                format: 'table',
                content: [
                    {
                        year: '2011',
                        subject: 'Radiotherapy',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effects of drinking hydrogen-rich water on the quality of life of patients treated with radiotherapy for liver tumors.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/22146004'
                            },
                            {
                                format: 'text',
                                content:
                                    'A randomized placebo-controlled human study shows therapeutic benefits for drinking hydrogen-rich water before, during, and after radiation treatments.'
                            }
                        ]
                    },
                    {
                        year: '2011',
                        subject: 'Weight Loss',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'The effect of daily consumption of 2 liters of electrolyzed water for 2 months on body composition and several physiological parameters in four obese subjects: a preliminary report.',
                                linkToText:
                                    'https://www.thefreelibrary.com/The+effect+of+daily+consumption+of+2+liters+of+electrolyzed+water+for...-a0269433201'
                            },
                            {
                                format: 'text',
                                content:
                                    'This was a human trial to see if alkaline ionized water would influence the ability of four obese patients to lose weight. The average weight loss reported during the two-month trial was 12 pounds. No other variable was introduced. No toxins were in the water, yet increased toxins were in the urine test, which indicates the body was able to detox several of the tested heavy metals.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Blood Viscosity',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Effect of electrolyzed high-pH alkaline water on blood viscosity in healthy adults.',
                                linkToText:
                                    'https://jissn.biomedcentral.com/articles/10.1186/s12970-016-0153-8'
                            },
                            {
                                format: 'text',
                                content:
                                    'Human study shows high-pH water drunk after exercise to be more hydrating. The blood viscosity was tested and shown to be better in the group who drank alkaline ionized water vs. acceptable standard purified water.'
                            }
                        ]
                    },
                    {
                        year: '2018',
                        subject: 'Pregnancy: Protective Benefits Spread to Newborn',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Administration of molecular hydrogen during pregnancy improves behavioral abnormalities of offspring in a maternal immune activation model.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Administration+of+molecular+hydrogen+during+pregnancy+improves+behavioral+abnormalities+of+offspring+in+a+maternal+immune+activation+model'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows that giving H2 to the mother is protective for the newborn.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'Hemorrhagic Shock',
                        description: [
                            {
                                format: 'text',
                                content:
                                    "Hydrogen gas inhalation inhibits progression to the 'irreversible' stage of shock after severe hemorrhage in rats.",
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Hydrogen+gas+inhalation+inhibits+progression+to+the+%22irreversible%22+stage+of+shock+after+severe+hemorrhage+in+rats.'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows treatment with H2 inhalation stabilizes the blood pressure until curative treatment can be performed.'
                            }
                        ]
                    },
                    {
                        year: '2014',
                        subject: 'Hearing Loss (noise-induced)',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Inhaled hydrogen gas therapy for prevention of noise-induced hearing loss through reducing reactive oxygen species.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/25196919'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows H2 inhalation is protective against noise-induced hearing loss.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Sepsis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular Hydrogen Therapy Ameliorates Organ Damage Induced by Sepsis.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4931094/'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows molecular hydrogen improves chances of survival.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Motor Deficits',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen alleviates motor deficits and muscle degeneration in mdx mice.',
                                linkToText:
                                    'https://www.tandfonline.com/doi/full/10.1080/13510002.2015.1135580'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows hydrogen improves prognosis for muscular dystrophy.'
                            }
                        ]
                    },
                    {
                        year: '2016',
                        subject: 'Bladder',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Preventive Effect of Hydrogen Water on the Development of Detrusor Overactivity in a Rat Model of Bladder Outlet Obstruction.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/26518110'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows H2 is protective against bladder dysfunction.'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Hearing: Auditory Hair Cells',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Hydrogen protects auditory hair cells from free radicals.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19339905'
                            },
                            {
                                format: 'text',
                                content:
                                    'In vitro study shows hydrogen to be protective against cell death of auditory hair cells.'
                            }
                        ]
                    },
                    {
                        year: '2014',
                        subject: 'Prevention Medicine',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Molecular hydrogen as a preventive and therapeutic medical gas: initiation, development and potential of hydrogen medicine.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/24769081'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Metabolic Acidosis',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Ionized alkaline water: new strategy for management of metabolic acidosis in experimental animals.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19527469'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows alkaline ionized water can manage metabolic acidosis safely.'
                            }
                        ]
                    },
                    {
                        year: '2009',
                        subject: 'Hangovers',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Electrolyzed-reduced water inhibits acute ethanol-induced hangovers in Sprague-Dawley rats.',
                                linkToText: 'https://www.ncbi.nlm.nih.gov/pubmed/19887722'
                            },
                            {
                                format: 'text',
                                content:
                                    'Animal study shows drinking alkaline ionized water has a detox effect from ethanol.'
                            }
                        ]
                    },
                    {
                        year: '2017',
                        subject: 'ERW Superior to other H2 Water',
                        description: [
                            {
                                format: 'text',
                                content:
                                    'Electrochemically reduced water exerts superior reactive oxygen species scavenging activity in HT1080 cells than the equivalent level of hydrogen-dissolved water.',
                                linkToText:
                                    'https://www.ncbi.nlm.nih.gov/pubmed/?term=Electrochemically+reduced+water+exerts+superior+reactive+oxygen+species+scavenging+activity+in+HT1080+cells+than+the+equivalent+level+of+hydrogen-dissolved+water.'
                            },
                            {
                                format: 'text',
                                content:
                                    'In vitro study shows that alkaline ionized water has an additional antioxidative factor, likely to be platinum nanoparticles, which makes it superior to other hydrogen applications for scavenging radicals.'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title: 'Ionized Hydrogen water in pregnancy',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE pregnancy-01.png',
                alt: 'PREGNANCY'
            },
            {
                format: 'text',
                content:
                    'Use of Ionized water for gynaecological conditions Prof. Watanabe Ifao, Watanabe Hospital experience in his 30 years of clinical practice that "Ionised alkaline antioxidant water improves body constituents and ensures effective healing to many illnesses. The uses of antioxidant water in gynaecological patients have proved to be very effective. The main reason for its effectiveness is that this water can neutralise toxins.'
            },
            {
                format: 'text',
                content:
                    'When given antioxidant water to pre-eclamptic toxaemia cases, the results are most significant. During my long years of servicing the pre-eclamptic toxaemia cases, I found that the women with pre-eclamptic toxaemia who consumed antioxidant water tend to deliver healthier babies with stronger muscles. A survey report carried out on babies in this group showed intelligence above average.'
            }
        ]
    },
    {
        title: 'Ionized Water Used for Cure of Atopic Dermatitis: Hospital Report',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE Dermatitis-01.png',
                alt: 'DERMATITIS'
            },
            {
                format: 'text',
                content:
                    'In another Japanese study, an Atopic Dermatitis patient became cured using ionised water. Atopic dermatitis is an illness accompanied by intense itching. Its cause is unknown. It has been found that this almost-incurable-illness can be cured using ionised water.'
            },
            {
                format: 'text',
                content:
                    'This report is from Akashi Hospital in Tokyo, Japan. It shows how Dr. Shinkai has cured thousands of patients with atopic dermatitis using both alkaline and acidic ionised water.'
            },
            {
                format: 'video',
                videoId: 'ktc9FoXmLWQ'
            }
        ]
    },
    {
        title:
            "Ionized Water Used for The Cure of Athlete's Foot, Bed Sores & More",
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE athletes foot-01.png',
                alt: "ATHLETE'S FOOT"
            },
            {
                format: 'text',
                content:
                    'This report shows several more serious medical problems being cured with alkaline and acidic ionized water.'
            },
            {
                format: 'video',
                videoId: 'vdZPqTykGf8'
            }
        ]
    },
    {
        title: 'Mice Experiments With Ionized Water',
        content: [
            {
                format: 'image',
                imgSrc: '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE mice-01.png',
                alt: 'MICE EXPERIMENTS'
            },
            {
                format: 'text',
                content:
                    'These experiments with mice show the positive effects found by water ionizer research in Japan and Korea. The video shows impressive results using ionized water with diabetes, cancer, high blood pressure, and obesity in mice and rats. The tests, shown in this video, were done in laboratory-controlled conditions and show without a doubt that alkaline ionized water has made significant improvements in multiple health-related conditions, i.e. diabetes, cholesterol, cancer, and more.'
            },
            {
                format: 'video',
                videoId: '5igYdtZxPW4'
            }
        ]
    },
    {
        title: 'Chicken Experiments With Ionized Water',
        content: [
            {
                format: 'image',
                imgSrc:
                    '/PNOY/medicalResearch/POSITIVE EFFECTS ON THE chicken-01.png',
                alt: 'CHICKEN EXPERIMENTS'
            },
            {
                format: 'text',
                content:
                    'These experiments with chickens are pretty impressive too. The tests, shown in this video, were done in controlled, scientific conditions and show without a doubt that alkaline ionized water has made significant improvements in the overall health and immune systems of chickens.'
            },
            {
                format: 'video',
                videoId: 'eIohs5qvY6M'
            }
        ]
    },
    {
        title: 'Scientific Research With Ionized Water',
        isScientificResearch: true,
        content: [
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/warburg.jpg',
                    alt: 'ottoWarburg'
                },
                name: 'Dr. Otto Warburg',
                title: '1931 Nobel Prize winner for cancer discovery',
                text: [
                    'The root cause of CANCER is an ACIDIC state leading to OXYGEN DEFICIENCY. CANCER cells cannot survive in high levels of oxygen, as found in an ALKALINE state.',
                    '“No Disease including Cancer can survive in an alkaline environment.”'
                ]
            },
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/kuwata.jpg',
                    alt: 'doctor'
                },
                name: 'Prof. Kuwata Keijiroo',
                title: 'Doctor of Medicine',
                text: [
                    'Along with medicines, the group consuming IONISED WATER showed superior control of BLOOD SUGAR pre /post prandial in comparison to the group who were consuming normal water along with the medicines.'
                ]
            },

            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/testSuji.avif',
                    alt: 'Stomach Problems'
                },
                name: 'Dr. Testsuji Hokudou',
                title: 'Director of Gastroenterology, National Ohkura Hospital',
                text: [
                    'ALKALINE IONISED WATER proved to be 80.5% effective against constipation with safety verified in numerous trials conducted by me in various hospitals.'
                ]
            },
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/kuninaka.avif',
                    alt: 'Blood Sample'
                },
                name: 'Prof. Dr Kuninaka Hironage',
                title: 'Head, Kuninaka Hospital',
                text: [
                    'Consumption of ALKALINE ANTIOXIDANT WATER for 2 to 3 months helps lowering the blood pressure and reduces high cholesterol levels.'
                ]
            },
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/yoshinori.jpg',
                    alt: 'Kyoto University'
                },
                name: 'Dr. Yoshinori Itokawa',
                title: 'Dean, Kyoto University Graduate School of Medicine',
                text: [
                    'Alkaline Ionized water helps promote Bone health and as it is full of alkaline miners which are necessary for bone health.'
                ]
            },
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/kogure.jpg',
                    alt: 'Kogure Clinic of Juntendo Hospital'
                },
                name: 'Prof. Dr Kogure Keizou',
                title: 'Kogure Clinic of Juntendo Hospital',
                text: [
                    'IONISED WATER enhances digestion and absorption of minerals by restoring the right pH balance of the body and helps retain right bacterial flora in the digestive system.'
                ]
            },
            {
                image: {
                    imgSrc: '/PNOY/scientificResearch/kuwata.jpg',
                    alt: 'Doctor'
                },
                name: 'Prof. Kuwata Keijiroo',
                title: 'Doctor of Medicine',
                text: [
                    'In cases of Kidney dialysis/diseases, patients who do not respond to drugs should drink ALKALINE IONIC WATER for at least 6 to 12 months to reduce uric acid in the blood.'
                ]
            }
        ]
    }
];

export const benefitsOfIonizedWater = [
    {
        imgSrc: '/PNOY/hydration.png',
        title: 'Hydration',
        text: 'Provides cellular-level hydration that is Five times better than Tap/RO water.'
    },
    {
        imgSrc: '/PNOY/detoxification.png',
        title: 'Detoxification',
        text: 'Experience 5 times better detoxification, which cleanses your system and keeps your kidneys, liver, and colon healthy.'
    },
    {
        imgSrc: '/PNOY/ph-balance.png',
        title: 'pH balance',
        text: 'Achieve a balanced body pH, reducing the risk of obesity, high cholesterol, high blood pressure, high blood sugar, kidney stones, metabolic acidosis, and cancers.'
    },
    {
        imgSrc: '/PNOY/weight-loss.png',
        title: 'Weight Loss',
        text: 'Ionized water removes acidic pockets in the body, which are major sites for fat deposition. This helps reduce weight and prevent weight-related health problems.'
    },
    {
        imgSrc: '/PNOY/anti-ageing.png',
        title: 'Anti-ageing',
        text: 'With its powerful antioxidant properties, Ionized water prevents wrinkles, and greying of hair, and keeps your internal organs healthy.'
    },
    {
        imgSrc: '/PNOY/heart-health.png',
        title: 'Heart Health',
        text: 'Improve heart health and blood pressure as Ionized water lowers cholesterol and enhances heart function.'
    },
    {
        imgSrc: '/PNOY/liver-health.png',
        title: 'Liver Health',
        text: 'Enhance liver health as Ionized water supports nearly 700 internal functions of the body.'
    },
    {
        imgSrc: '/PNOY/bone-health.png',
        title: 'Bone Health',
        text: 'Ionized water is rich in alkaline minerals like calcium and magnesium, ensuring superior bone health.'
    },
    {
        imgSrc: '/PNOY/Digestive-health.png',
        title: 'Digestive Health',
        text: 'Regular intake of Ionized water improves digestion, treats heartburn, and relieves constipation.'
    },
    {
        imgSrc: '/PNOY/Sports.png',
        title: 'Sports Performance',
        text: 'The properties of Ionized water enhance athletic performance, quick recovery after exercise, reduce fatigue and cramps, and provide a quick energy boost.'
    }
];

export const careers = [
    {
        careerId: 'business-development-manager',
        title: 'Business development Manager',
        location: 'Indore/Goa',
        eligibility: [
            'Research and identify new business opportunities - including new markets, growth areas, trends, customers, partnerships, products and services - or new ways of reaching existing markets.',
            'Seek out the appropriate contact in an organisation.',
            'Generate leads and call prospective customers.',
            'Meet with customers/clients face to face or over the phone.',
            'Foster and develop relationships with customers/clients.',
            'Understand the needs of your customers and be able to respond effectively with a plan of how to meet these.',
            'Think strategically - seeing the bigger picture and setting aims and objectives in order to develop and improve the business.',
            'Work strategically - carrying out necessary planning in order to implement operational changes.',
            'Strong verbal and written communications skills with a focus on needs analysis, positioning, business justification, and closing techniques.',
            'Would be responsible for managing end to end sales and achieve targets / Quotas.'
        ],
        expectations: [
            'MBA, BBA, or any specialization in sales and marketing of digital products.',
            '2+ years proven track record in enterprise business sales or related market.',
            'Strong proficiency in Microsoft Word, Excel, PowerPoint.',
            'Proven working experience as a business development manager, Sales or a relevant role.',
            'Good communication skills.',
            'Computer skills- Good at Excel and Presentation.',
            'Should be open for Travelling.'
        ]
    },
    {
        careerId: 'tele-caller',
        title: 'Tele Caller',
        location: 'Indore/Goa',
        eligibility: [
            'Checking inquiries of Customers and Follow up them.',
            'Studying the details of each offering and remaining abreast of updates to these offerings.',
            "Obtaining and updating lists of individuals' contact details.",
            'Communicating verbal acceptances of offers to our sales team for closing.',
            'Identifying customers’ needs, clarify information, research every issue and providing solutions.',
            'Informs clients by explaining procedures; answering questions; providing information.',
            'Maintains communication equipment by reporting problems.',
            'Ensure you follow the customer service script provided by the company for uniformity.',
            'Write and submit timely reports on performance, targets and customer queries.',
            'Keep records of all conversations in our call center database in a comprehensible way.',
            'Route calls to appropriate resources.'
        ],
        expectations: [
            'University/college degree is any Stream.',
            'Previous customer service experience, especially in a particular industry, preferred.',
            'Good Communication skills in Hindi & English.',
            'Good Listening Skills',
            'Customer/Client focus and Service.',
            'Ability to multi-task, set priorities and manage time effectively',
            'Proficient in relevant computer applications',
            'Ability to handle stressful situations appropriately',
            'Customer-oriented attitude with professionalism.'
        ]
    }
];

export const ourPresence = [
    { "branchCity": "PNOY Siddipet", "branchAddress": "Integrate market, opp bawani nagar, Siddipet, Telangana - 502102", "latitude": 18.1019043, "longitude": 78.85207700000001 },

    { "branchCity": "PNOY Bhopal", "branchAddress": "F 8 - 9B, Triveni heights, bersiya road, infront of promise hospital, Bhopal, Madhya Pradesh - 462038", "latitude": 23.29289, "longitude": 77.40376 },

    { "branchCity": "PNOY Ichalkaranji", "branchAddress": "Ambis complex near Radha Krishna Talkies Circle opposite Royal Plaza Mayur Highschool road Ichalkaranji, Kolhapur, Maharashtra - 416115", "latitude": 74.46186093602789, "longitude": 16.69985519217339 },

    { "branchCity": "PNOY Valsad", "branchAddress": "F - 5 krishna complex, madan wad, Near orchid hospital, Valsad, Gujarat - 396001", "latitude": 72.93281969546807, "longitude": 20.615354224620365 },

    { "branchCity": "PNOY sikar", "branchAddress": "Nahar techno Engineer, Ghorana, Piprali Road, sikar, Jaipur, Rajasthan - 332001", "latitude": 75.17147929567753, "longitude": 27.62809205874632 },

    { "branchCity": "PNOY Tirupati", "branchAddress": "6 - 535, Irla nagar, Tirupati, Andhra Pradesh - 517501", "latitude": 13.62803, "longitude": 79.42754 },

    { "branchCity": "PNOY Raipur", "branchAddress": "Gali no.7, Infront of laxman saloon, near durga mandir, Raipur, Chhattisgarh - 492006", "latitude": 21.23941, "longitude": 81.66418 },

    { "branchCity": "PNOY Puri", "branchAddress": "Daraji Pokhri square, police line chhak, Puri, Orissa - 752001", "latitude": 19.81173, "longitude": 85.83057 },

    { "branchCity": "PNOY jaipur", "branchAddress": "Plot No.89, ratan sagar, ISKON Road, Vijay Path, Mansarovar, Jaipur, Rajasthan - 302020", "latitude": 26.83781, "longitude": 75.75923 },

    { "branchCity": "PNOY Guwahati", "branchAddress": "C/O dhaijya kakoti, ghoramara, North Guwahati, Amingan, doul gobinda, Road near ghoramara, LP school, Guwahati, Assam - 781031", "latitude": 26.20537, "longitude": 91.73820 },

    { "branchCity": "PNOY Faridabad", "branchAddress": "House no.9, gali no 1, surya vihar part 2, sector 91, Faridabad, Haryana - 121013", "latitude": 28.4023443, "longitude": 77.1542621 },

    { "branchCity": "PNOY Gurugram", "branchAddress": "SCO 21/22, KHUSHAL NAGAR, NEAR GENPACT, SECTOR 69, Gurugram, Haryana - 122101", "latitude": 28.39953, "longitude": 77.03991 },

    { "branchCity": "PNOY Gwalior", "branchAddress": "101-102, kartik plaza, jinsi road no.1, Gwalior, Madhya Pradesh - 474001", "latitude": 26.20416, "longitude": 78.16179 },

    { "branchCity": "PNOY Lalbag Mumbai", "branchAddress": "A 202, datta krupa CHS, Dr Ambedkar road, opp voltas house, lalbag, Mumbai, Maharashtra - 400012", "latitude": 18.98852, "longitude": 72.83456 },

    { "branchCity": "PNOY Rafiq nagar Mumbai", "branchAddress": "Nr sai baba mandir, ahilya bai holkar marg, rafiq nagar, Mumbai, Maharashtra - 400043", "latitude": 19.03669, "longitude": 72.86807 },

    { "branchCity": "PNOY Jodhpur", "branchAddress": "Makan No. 7, jagdamba colony, mata ka than, Jodhpur, Rajasthan - 342304", "latitude": 26.32469, "longitude": 73.07527 },

    { "branchCity": "PNOY Udaipur", "branchAddress": "11 mama bhanja market, Near ICICI bank, Opp roadways bus stand, udaipole, Udaipur, Rajasthan - 313001", "latitude": 24.5763799, "longitude": 73.697893 },

    { "branchCity": "PNOY Bengaluru", "branchAddress": "168, CBS layout, Kasavanhalli main road, Opp Union Bank, Bengaluru Urban, Karnataka - 560035", "latitude": 12.9093438, "longitude": 77.6748206 },

    { "branchCity": "PNOY Vijaywada", "branchAddress": "Sitarampuram signal, opp costal bank, Eluru road, Vijaywada, Andhra Pradesh - 520002", "latitude": 16.5157342, "longitude": 80.6364389 },

    { "branchCity": "PNOY Narasaraopet", "branchAddress": "Behind apple dental clinic, arundelpet, Narasaraopet, Andhra Pradesh - 522601", "latitude": 16.23013, "longitude": 80.04010 },

    { "branchCity": "PNOY Chennai", "branchAddress": "Old No. 216, New No. 244, Paper mills road, Chennai, Tamilnadu - 600011", "latitude": 13.1149221, "longitude": 80.2302422 },

    { "branchCity": "PNOY Pune", "branchAddress": "Shop No.1, Ramganga Niwas, Wagholi, Pune, Maharashtra - 412207", "latitude": 18.57905, "longitude": 73.97256 },

    { "branchCity": "PNOY Bhavnagar", "branchAddress": "Opp BOB, shop no.2, Yogeshwar complex, Virani circle, kaliyabad, Bhavnagar, Gujarat - 364001", "latitude": 21.7396135, "longitude": 72.1411537 },

    { "branchCity": "PNOY Jalgaon", "branchAddress": "Shop No. 2, sachi apartment, mahabal road, Jalgaon, Maharashtra - 425001", "latitude": 20.9859725, "longitude": 75.5515721 },

    { "branchCity": "PNOY Gorakhpur", "branchAddress": "Raiganj road, Alahdadpur, Gorakhpur, Uttar Pradesh - 273001", "latitude": 26.7424989, "longitude": 83.3639319 },

    { "branchCity": "PNOY Bareilly", "branchAddress": "229 Zakati street, Bareilly, Uttar Pradesh - 243003", "latitude": 28.3676266, "longitude": 79.4086002 },

    { "branchCity": "PNOY Guntur", "branchAddress": "Arundelpet 12/1, shop no. 2 and 3, Fertilisers kalyan mandapam, Guntur, Andhra Pradesh - 522002", "latitude": 16.3049706, "longitude": 80.4303602 },

    { "branchCity": "PNOY Vadodara", "branchAddress": "GF-11, Nilkanth varni flat, bhardan nagar, near hegdewar garden, Vadodara, Gujarat - 390024", "latitude": 22.3389904, "longitude": 73.1844007 },

    { "branchCity": "PNOY Indore", "branchAddress": "352 A vidur nagar, CAT road, Indore, Madhya Pradesh - 452009", "latitude": 22.7076167, "longitude": 75.7732324 },

    { "branchCity": "PNOY Chinchwad", "branchAddress": "Flat No. 11, A-8 kunal estate society, kakade park, chinchwad, Pune, Maharashtra - 411033", "latitude": 18.6236909, "longitude": 73.7843608 },

    { "branchCity": "PNOY Bhagalpur", "branchAddress": "Gurhatta chowk, bounsi road, besides bank of india, Bhagalpur, Bihar - 812002", "latitude": 25.235166, "longitude": 86.9719322 },

    { "branchCity": "PNOY Nashik", "branchAddress": "B - 101 -102, vraj life space commercial complex, opposite RTO office, Peth road, Nashik, Maharashtra - 422004", "latitude": 20.0325011, "longitude": 73.7926091 },

    { "branchCity": "PNOY New Delhi", "branchAddress": "F-2, Lado Sarai, Near Kali Mata Mandir, Mehrauli, New Delhi, New Delhi - 110030", "latitude": 28.5233064, "longitude": 77.1749922 },

    { "branchCity": "PNOY Hyderabad", "branchAddress": "Meghal Ka Nala, Inner Ring Road, Hyderabad, Telangana - 500006", "latitude": 17.3666445, "longitude": 78.4260306 },

    { "branchCity": "PNOY Surat", "branchAddress": "G-14 Alishan Enclave, Hazira Road, Opp. Star Bazar, Adajan, Surat, Gujarat - 395009", "latitude": 21.1889411, "longitude": 72.7930682 },

    { "branchCity": "PNOY Agra", "branchAddress": "24/206 Bagh Ram Sahai, Loha Mandi, Agra, Uttar Pradesh - 282002", "latitude": 27.1949094, "longitude": 77.955105 },

    { "branchCity": "PNOY Morena", "branchAddress": "In Front of Dr. S. K. sharma, M.S. Road, Morena, Madhya Pradesh - 476001", "latitude": 26.4897594, "longitude": 77.9815369 },

    { "branchCity": "PNOY Valsad", "branchAddress": "F-5 krishna Complex, Madan Wad, Near Orchid Hospital, Valsad, Gujarat - 396001", "latitude": 20.6152287, "longitude": 72.9302448 },

    { "branchCity": "PNOY Nagpur", "branchAddress": "1133/A Hingana T Point, Takli seem, Hingana Road, Nagpur, Maharashtra - 440016", "latitude": 21.0945626, "longitude": 78.9552486 },

    { "branchCity": "PNOY Jabalpur", "branchAddress": "Shop No. 17, Suvidha Market, Opp. Kalmains Office Gupteshar Mandir, Shashtri Nagar, Jabalpur, Madhya Pradesh - 482001", "latitude": 23.1457188, "longitude": 79.9154577 },

    { "branchCity": "PNOY Kochi", "branchAddress": "11/307A - Sammoham Madam, Ferry Road South Chitoor, Kochi, Kerala - 682027", "latitude": 10.03798, "longitude": 76.27274 }
]
